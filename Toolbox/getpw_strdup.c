/*
 * =====================================================================================
 *
 *       Filename:  getpw_strdup.c
 *
 *    Description:  Read password from stdin and not show the characters.
 *
 *        Version:  1.0
 *        Created:  23. okt. 2024 kl. 18.46 +0200
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

/* This code uses the strndup() function. You need to compile the code with a recent
   compiler and -std=c23. If you get errors, then you can try to use the flags:
   -std=gnu11 or std=c11 -D_GNU_SOURCE
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

int getch() {
    int ch;
    // https://man7.org/linux/man-pages/man3/termios.3.html
    struct termios t_old, t_new;

    // https://man7.org/linux/man-pages/man3/termios.3.html
    // tcgetattr() gets the parameters associated with the object referred
    //   by fd and stores them in the termios structure referenced by
    //   termios_p
    tcgetattr(STDIN_FILENO, &t_old);
    
    // copy old to new to have a base for setting c_lflags
    t_new = t_old;

    // https://man7.org/linux/man-pages/man3/termios.3.html
    //
    // ICANON Enable canonical mode (described below).
    //   * Input is made available line by line (max 4096 chars).
    //   * In noncanonical mode input is available immediately.
    //
    // ECHO   Echo input characters.
    t_new.c_lflag &= ~(ICANON | ECHO);
    
    // sets the attributes
    // TCSANOW: the change occurs immediately.
    tcsetattr(STDIN_FILENO, TCSANOW, &t_new);

    ch = getchar();

    // reset stored attributes
    tcsetattr(STDIN_FILENO, TCSANOW, &t_old);

    return( ch );
}

const char *getpass() {
    int show_asterisk = true;

    /* See https://en.cppreference.com/w/cpp/language/ascii for codes */
    const char DEL = 127;
    const char NEWLINE = '\n';

    unsigned char ch = 0;
    char password[100];
    char *password_copy = 0x0;
    unsigned int password_index = 0;
    size_t password_length = 0;

    /* Clear password buffer */
    memset( password, 0x0, 100 );
    printf("Password: ");

    while ( (ch = getch()) != NEWLINE ) {
        if (ch == DEL) {
            password_length = strlen(password);
            if ( password_length != 0 ) {
                if (show_asterisk) {
                    printf( "\b \b" ); /* backspace: \b */
                }
                password[password_length-1] = 0x0;
                password_index--;
            }
            else {
                password_index = 0;
            }
        }
        else {
            if ( password_index < 99 ) {
                password[password_index] = ch;
                password_index++;
                if ( show_asterisk ) {
                    printf("*");
                }
            }
        }
    }
    printf("\n");
    password_copy = strndup( password, 99 );
    return( password_copy );
}

/*----------------------------------------------------------------------
//  M A I N
//---------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    int rc = 0;
    char *mypass;

    mypass = (char *)getpass();
    if ( strlen(mypass) == 0 ) {
        printf("No password provided.\n");
        rc = 1;
    }
    else {
        printf("Password entered: %s\n", mypass );
    }
    free( (void *)mypass );

    return(rc);
}
