// =====================================================================================
//
//       Filename:  getpw.cpp
//
//    Description:  Read password from stdin and not show the characters.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 21.44 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <iostream>
#include <string>

#include <stdio.h>

#ifdef __unix__
#include <sysexits.h>   // Usually not availble on non-UNIX systems
#else
#define EX_OK		0	/* successful termination */
#define EX_DATAERR	65	/* data format error */
#endif

#include <termios.h>
#include <unistd.h>

extern "C" auto getch() -> int {
    int ch;
    // https://man7.org/linux/man-pages/man3/termios.3.html
    struct termios t_old, t_new;

    // https://man7.org/linux/man-pages/man3/termios.3.html
    // tcgetattr() gets the parameters associated with the object referred
    //   by fd and stores them in the termios structure referenced by
    //   termios_p
    tcgetattr(STDIN_FILENO, &t_old);
    
    // copy old to new to have a base for setting c_lflags
    t_new = t_old;

    // https://man7.org/linux/man-pages/man3/termios.3.html
    //
    // ICANON Enable canonical mode (described below).
    //   * Input is made available line by line (max 4096 chars).
    //   * In noncanonical mode input is available immediately.
    //
    // ECHO   Echo input characters.
    t_new.c_lflag &= ~(ICANON | ECHO);
    
    // sets the attributes
    // TCSANOW: the change occurs immediately.
    tcsetattr(STDIN_FILENO, TCSANOW, &t_new);

    ch = getchar();

    // reset stored attributes
    tcsetattr(STDIN_FILENO, TCSANOW, &t_old);

    return( ch );
}

auto getpass() -> std::string {
    bool show_asterisk = true;

    // See https://en.cppreference.com/w/cpp/language/ascii for codes
    const char DEL = 127;
    const char NEWLINE = '\n';

    unsigned char ch = 0;
    std::string password;

    std::cout << "Password: ";

    while ( (ch = getch()) != NEWLINE ) {
        if (ch == DEL) {
            if ( password.length() != 0 ) {
                if (show_asterisk) {
                    std::cout << "\b \b"; // backspace: \b
                }
                password.resize(password.length() - 1);
            }
        }
        else {
            password.push_back( ch );
            if ( show_asterisk ) {
                std::cout << "*";
            }
        }
    }
    std::cout << "\n";
    return( password );
}

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK; // See sysexits.h for error codes
    std::string mypass;

    mypass = getpass();
    if ( mypass.empty() ) {
        std::cout << "No password provided.\n";
        rc = EX_DATAERR;
    }
    else {
        std::cout << "Password entered: " << mypass << "\n";
    }

    return(rc);
}
