# Move Semantics

Foo foo = std::move(bar); // construction, invokes move constructor
foo = std::move(other);   // assignment, invokes move assignment operator
