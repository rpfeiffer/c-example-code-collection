// =====================================================================================
//
//       Filename:  building.cpp
//
//    Description:  Building class implementation.
//
//        Version:  1.0
//        Created:  22. okt. 2024 kl. 00.01 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <algorithm>
#include <iostream>
#include <memory>

#define FMT_HEADER_ONLY
#include <fmt/core.h>
#include <fmt/color.h>
#include <fmt/format.h>

#include "building.h"

//----------------------------------------------------------------------
// Constructor functions
//----------------------------------------------------------------------

building::building( const int floor_min, const int floor_max, const int ground_floor ) {
    if ( floor_min < floor_max ) {
        m_floor_min = floor_min;
        m_floor_max = floor_max;
        m_floors = ( floor_max - floor_min ) + 1;
    }
    m_ground_floor = std::clamp( m_ground_floor, floor_min, floor_max );
    populateFloors();
    // This constructors leaves the creation of elevators to the member function.
    n_elevators = 0;
}

building::building( const int floors, const int floor_min, const int floor_max, const int ground_floor ) {
    if ( floor_min < floor_max ) {
        m_floor_min = floor_min;
        m_floor_max = floor_max;
        m_floors = ( floor_max - floor_min ) + 1;
    }
    m_ground_floor = std::clamp( m_ground_floor, floor_min, floor_max );
    populateFloors();
    // This constructors leaves the creation of elevators to the member function.
    n_elevators = 0;
}

building::building( const int floors, const int floor_min, const int floor_max, const int ground_floor, const int elevators ) {
    if ( floor_min < floor_max ) {
        m_floor_min = floor_min;
        m_floor_max = floor_max;
        m_floors = ( floor_max - floor_min ) + 1;
    }
    m_ground_floor = std::clamp( m_ground_floor, floor_min, floor_max );
    populateFloors();
    if ( elevators > 1 ) {
        n_elevators = elevators;
    }
}

auto building::populateFloors() -> void {
    for( int f = m_floor_min; f <= m_floor_max; f++ ) {
        m_building_floors[f] = std::make_unique<class floor>(f);
    }
}

auto building::populateElevators() -> void {
    for( int e = 1; e <= n_elevators; e++ ) {
        m_elevators.at(e) = std::make_unique<class elevator>( m_floor_min, m_floor_max );
    }
}

//----------------------------------------------------------------------
// Destructor function
//----------------------------------------------------------------------

building::~building() {
    m_building_floors.clear();
    m_elevators.clear();
    n_elevators = 0;
}

//----------------------------------------------------------------------
// Add elevator(s) and persons
//----------------------------------------------------------------------

auto building::addElevator() -> bool {
    return( addElevator( m_floor_min, m_floor_max ) );
}

auto building::addElevator( const int floor_min, const int floor_max ) -> bool {
    bool rc = true;

    m_elevators.push_back( std::make_unique<class elevator>( floor_min, floor_max ) );

    return(rc);
}

auto building::addMaxElevators() -> bool {
    bool rc = true;

    for( unsigned int e=1; e <= m_floors; e++ ) {
        rc = rc and addElevator();
    }
    n_elevators = m_building_floors.size();

    return(rc);
}

auto building::addRandomPersons( unsigned int max_persons ) -> unsigned int {
    unsigned int count = 0;
    std::random_device rdev;
    std::uniform_int_distribution<unsigned int> p( 1, max_persons );
    std::uniform_int_distribution<int> p_floor( m_floor_min, m_floor_max );

    for( int f=m_floor_min; f <= m_floor_max; f++ ) {
        unsigned int persons = p(rdev);
        for( unsigned int k=1; k <= persons; k++ ) {
            m_building_floors.at(f)->addPerson( std::make_unique<class person>( p_floor(rdev) ) );
            count++;
        }
    }

    return( count );
}

//----------------------------------------------------------------------
// Check if floors are empty (no waiting persons)
//----------------------------------------------------------------------

auto building::floorsEmpty() -> bool {
    bool empty = true;

    for( auto const& [key,floor] : m_building_floors ) {
        if ( floor->isEmpty() ) {
            empty = false;
            break;
        }
    }

    return(empty);
}

//----------------------------------------------------------------------
// Query defective elevators
//----------------------------------------------------------------------

auto building::getDefectiveElevators() -> int {
    return( n_elevators_defect );
}

auto building::getElevators() -> int {
    return( m_floors );
}

//----------------------------------------------------------------------
// Get random existing floor
//----------------------------------------------------------------------

auto building::getRandomFloor() -> int {
    std::random_device rdev;
    std::uniform_int_distribution<int> p_floor( m_floor_min, m_floor_max );

    int floor = p_floor(rdev);

    return(floor);
}

auto building::getRandomFloor( int not_this_floor ) -> int {
    std::random_device rdev;
    std::uniform_int_distribution<int> p_floor( m_floor_min, m_floor_max );
    int floor;

    do {
        floor = p_floor(rdev);
    } while ( floor != not_this_floor );

    return(floor);
}

//----------------------------------------------------------------------
// Handle specific floor and all floors
//----------------------------------------------------------------------

auto building::handleFloor( std::unique_ptr<class floor>& floor ) -> bool {
    bool error = false;
    int c_floor = floor->getFloor();
    double max_weight = 0.0;
    std::unique_ptr<class person> p(nullptr);

    // Check if there are any elevators
    for( auto e = m_elevators.begin(); e != m_elevators.end(); e++ ) {
        //fmt::print( "handleFloor() - for() loop.\n" );
        if ( (*e)->getCurrentFloor() == c_floor ) {
            //fmt::print( "Elevator is in floor {}.\n", c_floor ); 
            // Remove passengers who want to exit
            while ( (p = (*e)->getPassengerArrived( c_floor )) != nullptr ) {
                if ( p->checkExit( c_floor, getRandomFloor() ) ) {
                    p.reset();
                }
                else {
                    bool success = (*e)->passengerBoard( std::move(p) );
                    if ( not success ) {
                        if ( p != nullptr ) {
                            floor->addPerson( std::move(p) );
                        }
                    }
                }
                //fmt::print( "handleFloor() - while() loop.\n" );
            }
            // We have an elevator; now get next person(s) available waiting on floor
            p = nullptr;
            bool do_continue = true;
            do {
                max_weight = (*e)->getLoadCapacity();
                p = floor->getPerson( max_weight );
                if ( p != nullptr ) {
                    bool status = (*e)->passengerBoard( std::move(p) );
                    p = nullptr;
                    if ( not status ) {
                        error = true;
                        break;
                    }
                    max_weight = (*e)->getLoadCapacity();
                    do_continue = ( not floor->isEmpty() ) and floor->isPersonAvailable(max_weight);
                }
                else {
                    do_continue = false;
                }
                // fmt::print( "handleFloor() - do() loop.\n" );
                //fmt::print( "Floor = {} | max_weight = {}\n", c_floor, max_weight );
            } while ( do_continue );
        }
        else {
            //fmt::print( "Elevator not in floor {}.\n", c_floor ); 
        }
    }

    if ( error ) {
        std::clog << "ERROR: Error during handleFloor() function." << std::endl;
    }

    return(error);
}

auto building::handleFloors() -> bool {
    bool status = true;

    for( int floor = m_floor_min; floor <= m_floor_max; floor++ ) {
        //fmt::print( "Floor #{}\n", floor );
        if ( not handleFloor( m_building_floors.at(floor) ) ) {
            status = false;
        }
        //fmt::print( "handleFloors() - for() loop.\n" );
    }

    return(status);
}

//----------------------------------------------------------------------
// Mvoe all elevators
//----------------------------------------------------------------------

auto building::move_elevators() -> int {
    int f = 0;

    for( auto ei = m_elevators.begin(); ei != m_elevators.end(); ei++ ) {
        f = f + (*ei)->elevator_move();
    }

    return(f);
}

//----------------------------------------------------------------------
// Show elevator status
//----------------------------------------------------------------------

auto building::showElevators() -> void {
    unsigned int width = n_elevators * 5 + 1;
    std::string bar;

    for( unsigned int i=1; i <= width; i++ ) {
        bar.append("═");
    }

    // First line
    fmt::print( "╔{}╗\n", bar );
    // Header
    fmt::print( "║ " );
    for( unsigned int i=1; i <= n_elevators; i++ ) {
        fmt::print( "{:>04} ", i );
    }
    fmt::print( "║\n" );
    // Floor lines
    int floor = m_floor_max;
    for( unsigned int f=1; f <= m_floors; f++, floor-- ) {
        fmt::print( "║ " );
        for( unsigned int i=0; i < n_elevators; i++ ) {
            if ( floor == m_elevators.at(i)->getCurrentFloor() ) {
                fmt::print( "▄▅▅▄ " );
            }
            else {
                fmt::print( "▁▁▁▁ " );
            }
        }
        fmt::print( "║\n" );
    }
    // Last line
    fmt::print( "╚{}╝\n\n", bar );
}

//----------------------------------------------------------------------
// Show persons in elevators
//----------------------------------------------------------------------

auto building::showPersonsInElevators() -> void {
    unsigned int elevator = 1;
    for( auto ei = m_elevators.begin(); ei != m_elevators.end(); ei++ ) {
        fmt::print( "Elevator {:>4} has {} passengers.\n", elevator, (*ei)->getPersons() );
        elevator++;
    }
    fmt::print("\n");
}

//----------------------------------------------------------------------
// Show waiting persons
//----------------------------------------------------------------------

auto building::showPersons() -> void {
    int floor = m_floor_max;

    for( int f = m_floor_max; f >= m_floor_min; f--, floor-- ) {
        fmt::print( "Floor {:>-4} has {} waiting persons.\n", floor, m_building_floors.at(f)->getPersons() );
    }
    fmt::print("\n");
}
