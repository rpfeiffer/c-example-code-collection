// =====================================================================================
//
//       Filename:  floor.cpp
//
//    Description:  Implementation of floor class.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 23.38 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <random>
#include <utility>
#include "floor.h"
#include "person.h"

//----------------------------------------------------------------------
// Constructor function
//----------------------------------------------------------------------

floor::floor( int floor_number ) {
    m_floor = floor_number;
}

//----------------------------------------------------------------------
// Move and swap semantics
//----------------------------------------------------------------------

// Move constructor
floor::floor( floor&& other ) noexcept {
    m_floor = other.m_floor;
    m_persons_waiting = other.m_persons_waiting;
}

// Move assigment
class floor& floor::operator=( class floor&& other ) noexcept {
    if ( this != &other ) {
        this->m_floor = std::move( other.m_floor );
        this->m_persons_waiting = std::move( other.m_persons_waiting );
        this->m_persons = std::move( other.m_persons );
    }
    return( *this );
}

// Swap operation
void floor::swap( floor& f ) noexcept {
    std::swap( this->m_floor, f.m_floor );
    std::swap( this->m_persons_waiting, f.m_persons_waiting );
    std::swap( this->m_persons, f.m_persons );
}

//----------------------------------------------------------------------
// Destructor function
//----------------------------------------------------------------------

floor::~floor() {
    m_persons.clear();
}

//----------------------------------------------------------------------
// Add person object / objects
//----------------------------------------------------------------------

auto floor::addPerson( std::unique_ptr<class person> person ) -> void {
    m_persons.push_back( std::move(person) );
    m_persons_waiting++;
}

//----------------------------------------------------------------------
// Get floor number
//----------------------------------------------------------------------

auto floor::getFloor() -> int {
    return( m_floor );
}

//----------------------------------------------------------------------
// Check number of waiting persons, get person to enter elevator
//----------------------------------------------------------------------

auto floor::getPerson( double max_weight ) -> std::unique_ptr<class person> {
    std::unique_ptr<class person> p(nullptr);

    if ( not m_persons.empty() ) {
        auto p_wait = m_persons.begin();
        while ( p_wait != m_persons.end() ) {
            if ( (*p_wait)->getWeight() <= max_weight ) {
                p = std::move( *p_wait );
                m_persons.erase(p_wait);
                break;
            }
            else {
                p_wait++;
            }
        }
    }

    return(p);
}

auto floor::getPersons() -> int {
    if ( m_persons_waiting >= 0 ) {
        return( m_persons_waiting );
    }
    else {
        // There are no negative persons.
        return( 0 );
    }
}

auto floor::isEmpty() -> bool {
    return( m_persons_waiting == 0 ? true : false );
}

auto floor::isPersonAvailable( double max_weight ) -> bool {
    bool person_available = false;

    if ( ( not m_persons.empty() ) and ( max_weight > 0.0 ) ) {
        auto p_wait = m_persons.begin();
        while ( p_wait != m_persons.end() ) {
            if ( (*p_wait)->getWeight() <= max_weight ) {
                person_available = true;
                break;
            }
            p_wait++;
        }
    }

    return( person_available );
}
