// =====================================================================================
//
//       Filename:  elevator.cpp
//
//    Description:  Implementation of elevator class.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 23.06 +0200
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <algorithm>
#include <cmath>
#include <cstdlib>

#include <unistd.h>

#include "elevator.h"
#include "floor.h"

//----------------------------------------------------------------------
// Constructor functions
//----------------------------------------------------------------------

elevator::elevator( const int floor_min, const int floor_max ) {
    if ( floor_min < floor_max ) {
        m_floor_min = floor_min;
        m_floor_max = floor_max;
    }
}

elevator::elevator( const int floor_min, const int floor_max, const double weight_max ) {
    if ( floor_min < floor_max ) {
        m_floor_min = floor_min;
        m_floor_max = floor_max;
    }
    if ( weight_max > 0.0 ) {
        m_weight_max = weight_max;
    }
    else {
        m_weight_max = fabs( weight_max );
    }
}

auto elevator::initialise() -> void {
    std::normal_distribution<double>::param_type newParams( m_in_out_delay, m_in_out_delay * 0.35 );
    p.param( newParams );
}

//----------------------------------------------------------------------
// Move assignment operator and constructor
//----------------------------------------------------------------------

elevator::elevator( elevator&& other ) noexcept
    : m_floor_cur( other.m_floor_cur ),
      m_floor_min( other.m_floor_min ),
      m_floor_max( other.m_floor_max ),
      m_speed( other.m_speed ),
      m_accel( other.m_accel ),
      m_decel( other.m_decel ),
      m_in_out_delay( other.m_in_out_delay ),
      m_direction( other.m_direction ),
      m_weight_max( other.m_weight_max ),
      m_weight_cur( other.m_weight_cur ),
      m_defect( other.m_defect ),
      m_defect_probability( other.m_defect_probability ),
      n_persons_cur( other.n_persons_cur ),
      passengers( std::move( other.passengers ) ),
      next_floor( std::move( other.next_floor ) ),
      p( std::move( other.p ) )
    {
    }

//----------------------------------------------------------------------
// Destructor function
//----------------------------------------------------------------------

elevator::~elevator() {
    passengers.clear();
    next_floor.clear();
    n_persons_cur = 0;
    m_weight_cur = 0.0;
}

//----------------------------------------------------------------------
// Calculate delay for in/out of persons
//----------------------------------------------------------------------

auto elevator::calculate_in_out_delay() -> useconds_t {
    double d = -1.0;
    useconds_t delay = DEFAULT_DELAY;

    d = p(rd) * MICROSECONDS_DELAY;
    if ( d > 0.0 ) {
        delay = static_cast<useconds_t>(d);
    }

    return(delay);
}

//----------------------------------------------------------------------
// Getter functions
//----------------------------------------------------------------------

auto elevator::getCurrentFloor() -> int {
    return( m_floor_cur );
}

auto elevator::getCurrentLoad() -> double {
    return( m_weight_cur );
}

auto elevator::getLoadCapacity() -> double {
    return( m_weight_max - m_weight_cur );
}

auto elevator::getPersons() -> const int {
    return( n_persons_cur );
}

//----------------------------------------------------------------------
// Setter functions
//----------------------------------------------------------------------

auto elevator::enableSleep() -> void {
    do_usleep = true;
}

auto elevator::setDelay( const double in_out_delay ) -> void {
    if ( in_out_delay > 0.0 ) {
        m_in_out_delay = in_out_delay;
    }
    else {
        m_in_out_delay = fabs( in_out_delay );
    }
    std::normal_distribution<double>::param_type newParams( m_in_out_delay, m_in_out_delay * 0.35 );
    p.param( newParams );
}

auto elevator::setPhysics( const double speed, const double accel, const double decel ) -> void {
    if ( speed > 0.0 ) {
        m_speed = speed;
    }
    else {
        m_speed = fabs( speed );
    }
    if ( accel > 0.0 ) {
        m_accel = accel;
    }
    else {
        m_accel = fabs( accel );
    }
    if ( decel > 0.0 ) {
        m_decel = decel;
    }
    else {
        m_decel = fabs( decel );
    }
}

//----------------------------------------------------------------------
// Passenger boarding and deboarding
//----------------------------------------------------------------------

auto elevator::getPassengerArrived( int floor ) -> std::unique_ptr<class person> {
    if ( not passengers.empty() ) {
        for( auto p = passengers.begin(); p != passengers.end(); p++ ) {
            if ( *p != nullptr ) {
                if ( (*p)->getExitFloor() == floor ) {
                    return( std::move( *p ) );
                }
            }
        }
    }
    return( nullptr );
}

auto elevator::passengerBoard( std::unique_ptr<class person> passenger ) -> bool {
    bool status = false;
    double weight = 0.0;

    if ( passenger == nullptr ) return(status);

    weight = passenger->getWeight();
    // Check weight
    if ( ( m_weight_cur + weight ) <= m_weight_max ) {
        m_weight_cur = m_weight_cur + weight;
        // Check floor - here should be the direction check as well
        int floor = passenger->getExitFloor();
        if ( std::find( next_floor.begin(), next_floor.end(), floor ) == next_floor.end() ) {
            next_floor.push_back( floor );
            // FIXME - should we sort the vector here?
        }
        passengers.push_back( std::move(passenger) );
        n_persons_cur++;
        status = true;
    }

    return(status);
}

auto elevator::passengerExit( std::unique_ptr<class person> passenger, std::unique_ptr<class floor> floor ) -> bool {
    bool status = false;
    double weight = passenger->getWeight();

    if ( m_weight_cur > 0.0 ) {
        m_weight_cur = m_weight_cur - weight;
        floor->addPerson( std::move( passenger ) );
        n_persons_cur--;
        status = true;
    }

    return(status);
}

//----------------------------------------------------------------------
// Move cabin to destination floor and other move operations
//----------------------------------------------------------------------

auto elevator::elevator_move() -> int {
    int floor = 0;
    bool found_floor = false;

    if ( next_floor.size() == 1 ) {
        auto f = next_floor.begin();
        floor = *f;
        found_floor = true;
    }
    if ( not next_floor.empty() ) {
        const auto [ f_min, f_max ] = std::minmax_element( next_floor.begin(), next_floor.end() );
        for( auto f = next_floor.begin(); f != next_floor.end(); f++ ) {
            if ( abs( m_floor_cur - *f ) <= 2 ) {
                floor = m_floor_cur;
                found_floor = true;
                break;
            }
            if ( not found_floor ) {
                auto d_min = abs( m_floor_cur - *f_min );
                auto d_max = abs( m_floor_cur - *f_max );
                floor = std::min( d_min, d_max );
                found_floor = true;
            }
        }
    }
    if ( found_floor ) {
        auto delay = move_cabin( floor );
        if ( do_usleep ) {
            usleep( delay );
        }
    }

    return(floor);
}

auto elevator::move_cabin( int dest_floor ) -> useconds_t {
    useconds_t t = 0;
    int delta_floor = 0;

    if ( dest_floor > m_floor_max) dest_floor = m_floor_max;
    if ( dest_floor < m_floor_min) dest_floor = m_floor_min;
    delta_floor = abs( m_floor_cur - dest_floor );
    if ( delta_floor != 0 ) {
        double dt = sqrt( (2 * m_speed) / m_accel ) + delta_floor * m_speed + sqrt( (2 * m_speed) / fabs(m_decel) );
        dt = dt * MICROSECONDS_DELAY;
        t = static_cast<useconds_t>(dt);
    }

    if ( do_usleep ) {
        usleep(t);
    }

    return(t);
}
