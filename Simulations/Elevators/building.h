#ifndef BUILDING_H
#define BUILDING_H
// =====================================================================================
//
//       Filename:  building.h
//
//    Description:  Building class for elevator simulation.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 16.59 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <map>
#include <memory>
#include <vector>
#include "elevator.h"
#include "floor.h"

class building {
    public:
        building() = default;
        building( const int floor_min, const int floor_max, const int ground_floor );
        building( const int floors, const int floor_min, const int floor_max, const int ground_floor );
        building( const int floors, const int floor_min, const int floor_max, const int ground_floor, const int elevators );
        ~building();
        // Disable copy constructor and copy assignment
        building( const building& ) = delete;
        building& operator=( building& other ) = delete;
        // Move constructor
        // Move assignment operator

        bool addElevator();
        bool addElevator( const int floor_min, const int floor_max );
        bool addMaxElevators();
        unsigned int addRandomPersons( unsigned int max_persons );
        bool floorsEmpty();
        int getDefectiveElevators();
        int getElevators();
        int getRandomFloor();
        int getRandomFloor( int not_this_floor );
        bool handleFloor( std::unique_ptr<class floor>& floor );
        bool handleFloors();
        int move_elevators();
        void showElevators();
        void showPersons();
        void showPersonsInElevators();
        //bool simulationRound();

    private:
        // Storage
        int m_floors = 10;
        int m_floor_min = -1;
        int m_floor_max = 10;
        int m_ground_floor = 0;

        std::vector< std::unique_ptr<class elevator> > m_elevators;
        std::map< int, std::unique_ptr<class floor> > m_building_floors;
        int n_elevators = 2;
        int n_elevators_defect = 0;

        // Members
        void populateElevators();
        void populateFloors();
};

#endif
