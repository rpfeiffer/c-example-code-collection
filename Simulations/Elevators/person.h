#ifndef PERSON_H
#define PERSON_H
// =====================================================================================
//
//       Filename:  person.h
//
//    Description:  Person class for elevator simulation.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 21.19 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <random>

class person {
    public:
        person( const int floor_exit );
        person( const int floor_exit, const double weight );
        person( const int floor_exit, const double weight, const double exit_probability );
        ~person() = default;
        // Disable copy constructor and copy assignment
        person( const person& ) = delete;
        person& operator=( person& other ) = delete;
        // Move constructor
        person( person&& other ) noexcept;
        // Move assignment operator (lacks random device move)
        person& operator=( person&& other ) noexcept;

        bool checkExit( const int floor, const int new_floor );
        int getExitFloor();
        double getWeight();
        void setCurrentFloor( int floor_now );

    private:
        // Storage
        double m_weight = 75.0;
        int m_floor_now = 1;
        int m_floor_exit = 0;

        bool m_exit = false;
        double m_exit_probability = 0.85;

        std::random_device rd;
        std::uniform_real_distribution<double> p;

        // Members
        void initialise();
};

#endif
