// =====================================================================================
//
//       Filename:  person.cpp
//
//    Description:  Person class code.
//
//        Version:  1.0
//        Created:  22. okt. 2024 kl. 00.20 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <algorithm>
#include <cmath>
#include "person.h"

//----------------------------------------------------------------------
// Constructor functions
//----------------------------------------------------------------------

person::person( const int floor_exit ) {
    // Verification of valid floor values will be done when entering the
    // elevator and the floor. :)
    m_floor_exit = floor_exit;
    initialise();
}

person::person( const int floor_exit, const double weight ) {
    // Verification of valid floor values will be done when entering the
    // elevator and the floor. :)
    m_floor_exit = floor_exit;
    if ( weight > 0.0 ) {
        m_weight = weight;
    }
    else {
        m_weight = fabs( weight );
    }
    initialise();
}

person::person( const int floor_exit, const double weight, const double exit_probability ) {
    // Verification of valid floor values will be done when entering the
    // elevator and the floor. :)
    m_floor_exit = floor_exit;
    if ( weight > 0.0 ) {
        m_weight = weight;
    }
    else {
        m_weight = fabs( weight );
    }
    m_exit_probability = std::clamp( exit_probability, 0.0, 1.0 );
    initialise();
}

auto person::initialise() -> void {
    std::uniform_real_distribution<double>::param_type newParams( 0.0, 1.0 );
    p.param( newParams );
}

//----------------------------------------------------------------------
// Move constructor and assignment operator
//----------------------------------------------------------------------

person::person( person&& other ) noexcept
    : m_weight( other.m_weight ),
      m_floor_now( other.m_floor_now ),
      m_floor_exit( other.m_floor_exit ),
      m_exit( other.m_exit ),
      m_exit_probability( other.m_exit_probability ),
      p( std::move(other.p) )
    {
    }

class person& person::operator=( person&& other ) noexcept {
    if ( this != &other ) {
        m_weight = other.m_weight;
        m_floor_now = other.m_floor_now;
        m_floor_exit = other.m_floor_exit;
        m_exit = other.m_exit;
        m_exit_probability = other.m_exit_probability;
        p = other.p;
    }
    return( *this );
}

//----------------------------------------------------------------------
// Getter / setter functions
//----------------------------------------------------------------------

auto person::getExitFloor() -> int {
    return( m_floor_exit );
}

auto person::getWeight() -> double {
    return( m_weight );
}

auto person::setCurrentFloor( int floor_now ) -> void {
    if ( floor_now != m_floor_exit ) {
        m_floor_now = floor_now;
    }
}

//----------------------------------------------------------------------
// Check probability of exit
//----------------------------------------------------------------------

auto person::checkExit( const int floor, const int new_floor ) -> bool {
    if ( m_floor_exit == floor ) {
        if ( p(rd) <= m_exit_probability ) {
            m_exit = true;
        }
        else {
            m_exit = false;
            m_floor_now = floor;
            m_floor_exit = new_floor;
        }
    }

    return( m_exit );
}
