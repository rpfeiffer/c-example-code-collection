#ifndef ELEVATOR_H
#define ELEVATOR_H
// =====================================================================================
//
//       Filename:  elevator.h
//
//    Description:  Elevator class for elevator simulation.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 21.25 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <list>
#include <memory>
#include <random>
#include <vector>
#include "person.h"

#include <unistd.h>

enum direction {
    DOWN,
    STOP,
    UP
};

class elevator {
    public:
        // Class functions
        elevator() = default;
        elevator( const int floor_min, const int floor_max );
        elevator( const int floor_min, const int floor_max, const double weight_max );
        ~elevator();
        // Disable copy constructor and copy assignment
        elevator( const elevator& ) = delete;
        elevator& operator=( elevator& other ) = delete;
        // Move constructor
        elevator( elevator&& other ) noexcept;
        // Move assignment operator (lacks random device move)
        elevator& operator=( elevator&& other ) noexcept;

        // Member functions
        int elevator_move();
        void enableSleep();
        double getCurrentLoad();
        int getCurrentFloor();
        double getLoadCapacity();
        const int getPersons();
        void setDelay( const double in_out_delay );
        void setPhysics( const double speed, const double accel, const double decel );

        // Public storage
        std::unique_ptr<class person> getPassengerArrived( int floor );
        bool passengerBoard( std::unique_ptr<class person> passenger );
        bool passengerExit( std::unique_ptr<class person> passenger, std::unique_ptr<class floor> floor );

    private:
        // Constants
        const double MICROSECONDS_DELAY = 1000000.0;
        const useconds_t DEFAULT_DELAY = 5000000;

        // Storage
        int m_floor_cur = 0;
        int m_floor_min = -1;
        int m_floor_max = 10;

        double m_speed = 0.55;         // Measured in floors per second
        double m_accel = 0.25;
        double m_decel = -0.25;
        double m_in_out_delay = 5.0;
        enum direction m_direction = STOP;

        double m_weight_max = 1200.0;
        double m_weight_cur = 0.0;

        bool do_usleep = false;

        bool m_defect = false;
        double m_defect_probability = 0.001;

        int n_persons_cur = 0;
        std::list< std::unique_ptr<class person> > passengers;
        std::vector<int> next_floor;

        std::random_device rd;
        std::normal_distribution<double> p;

        // Member functions
        useconds_t calculate_in_out_delay();
        void initialise();
        useconds_t move_cabin( int dest_floor );
};

#endif
