// =====================================================================================
//
//       Filename:  main_test.cpp
//
//    Description:  Run a building with elevators and people moving in it - test version.
//
//        Version:  1.0
//        Created:  23. okt. 2024 kl. 22.55 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <iostream>
#include <sysexits.h>
#include "building.h"

using namespace std;

//----------------------------------------------------------------------
// Parameters
//----------------------------------------------------------------------

const int test_floor_max = 10;
const int test_floor_min = -3;
const int test_floors = ( test_floor_max - test_floor_min ) + 1;
const int test_ground_floor = 0;
const unsigned int test_max_persons = 125;

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK;
    unsigned int c = 1;
    class building Umpire_State( test_floor_min, test_floor_max, test_ground_floor );

    cout << "Created building with " << test_floors << " floors.\n";
    Umpire_State.addMaxElevators();
    cout << "Build now has " << Umpire_State.getElevators() << " elevators.\n";

    cout << "\n\n";
    Umpire_State.showElevators();
    cout << "\n\n";

    Umpire_State.addRandomPersons( test_max_persons );
    Umpire_State.showPersons();

    do {
        cout << "Calling .handleFloors()." << endl;
        bool status = Umpire_State.handleFloors();
        if ( not status ) {
            clog << "ERROR: Floor handling error.\n";
        }
        else {
            cout << "Showing elevators..." << endl;
        }
        c++;
        Umpire_State.showElevators();
    } while ( c <= 2 );

    return(rc);
}
