#ifndef FLOOR_H
#define FLOOR_H
// =====================================================================================
//
//       Filename:  floor.h
//
//    Description:  Floor class for elevator simulation.
//
//        Version:  1.0
//        Created:  21. okt. 2024 kl. 21.11 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <atomic>
#include <list>
#include <memory>
#include <utility>
#include "person.h"

class floor {
    public:
        floor( int floor_number );
        ~floor();
        // Disable copy constructor and copy assignment
        floor( const floor& ) = delete;
        floor& operator=( floor& other ) = delete;
        // Move constructor
        floor( floor&& other ) noexcept;
        // Move assignment operator
        floor& operator=( floor&& other ) noexcept;
        // swap
        void swap( floor& f ) noexcept;

        void addPerson( std::unique_ptr<class person> person );
        int getFloor();
        int getPersons();
        std::unique_ptr<class person> getPerson( double max_weight );
        bool isEmpty();
        bool isPersonAvailable( double max_weight );

    private:
        int m_floor = 0;
        int m_persons_waiting = 0;
        std::list< std::unique_ptr<class person> > m_persons;
};

#endif
