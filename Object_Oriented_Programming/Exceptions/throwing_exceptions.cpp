// =====================================================================================
//
//       Filename:  throwing_exceptions.cpp
//
//    Description:  How to throw exceptions.
//
//        Version:  1.0
//        Created:  05. mars 2025 kl. 15.33 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o throwing_exceptions throwing_exceptions.cpp
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o throwing_exceptions throwing_exceptions.cpp
//
// When throwing and catching exception the data type of the throw() call must match the 
// declaration of the catch() term. Only matching catch() blocks are executed.

#include <exception>
#include <iostream>

using namespace std;

double divide( double a, double b ) {
    double r = 0.0;
    if ( b != 0.0 ) {
        r = a / b;
    }
    else {
        throw( "Division by zero error!" );
    }
    return(r);
}

double divide2( double a, double b ) {
    double r = 0.0;
    if ( b != 0.0 ) {
        r = a / b;
    }
    else {
        throw( runtime_error("Division by zero error!") );
    }
    return(r);
}

double divide3( double a, double b ) {
    double r = 0.0;
    r = a / b;
    return(r);
}

int main( int argc, char **argv ) {
    int rc = 0;
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

    // Catch string exception
    try {
        z = divide( x, y );
        cout << "Result = " << z << endl;
    }
    catch ( const string eh ) {
        // Should not be triggered
        cerr << eh << endl;
        rc = 1;
    }
    catch ( const char eh[] ) {
        cerr << eh << endl;
        rc = 1;
    }
    catch ( ... ) {
        cerr << "Something went very wrong." << endl;
        rc = 1;
    }

    // Catch string exception
    try {
        z = divide2( x, y );
        cout << "Result = " << z << endl;
    }
    catch ( const runtime_error &eh ) {
        cerr << eh.what() << endl;
        rc = 1;
    }
    catch ( ... ) {
        cerr << "Something went very wrong." << endl;
        rc = 1;
    }

    // Call function that might throw an exception
    try {
        z = divide3( x, y );
        cout << "Result = " << z << endl;
    }
    catch ( ... ) {
        cerr << "Something went very wrong." << endl;
        rc = 1;
    }

    return( rc );
}
