// =====================================================================================
//
//       Filename:  exception.cpp
//
//    Description:  Exception handling and throwing. Code based on examples in the https://en.cppreference.com/
//                  collection.
//
//        Version:  1.0
//        Created:  06. mai 2024 kl. 18.23 +0200
//       Revision:  none
//       Compiler:  clang++/g++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================

#include <fstream>
#include <iostream>
 
int main( int argc, char **argv ) {
    int ivalue;

    try {
        std::ifstream in("in.txt");
        in.exceptions(std::ifstream::failbit); // may throw
        in >> ivalue; // may throw
    }
    catch ( const std::ios_base::failure& fail ) {
        // handle exception here
        std::cout << fail.what() << '\n';
        return(1);
    }
    catch (...) {
        std::cout << "Unspecified error." << std::endl;
        return(1);
    }

    return(0);
}
