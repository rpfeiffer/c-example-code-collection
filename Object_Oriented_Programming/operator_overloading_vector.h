#ifndef H_MYVECTOR_HEADER
#define H_MYVECTOR_HEADER
// =====================================================================================
//
//       Filename:  operator_overloading_vector.h
//
//    Description:  Class that implements a vector with basic arithmetic operations.
//
//        Version:  1.0
//        Created:  06. mai 2024 kl. 15.49 +0200
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// Implements a 3d vector class with some basic operations.

#include <cfloat>
#include <cmath>
#include <fstream>
#include <iostream>
#include <tuple>

// --- Class definition ----------------------------------------------------------------

class myvector {
    public:
        myvector() = default;
        myvector( const double a, const double b, const double c );
        ~myvector() = default;

        double dot_product( const class myvector& other ) const;
        std::tuple<double,double,double> get_coordinates() const;
        double length();
        void scale( const double f );

        const class myvector operator+( const class myvector& other );
        const class myvector operator-( const class myvector& other );
        const class myvector operator*( const double other );
        const class myvector operator*( const class myvector& other );
        const bool operator==( class myvector& other );
        const bool operator!=( class myvector& other );
        const bool operator<( class myvector& other );
        const bool operator>( class myvector& other );

    private:
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
};

// --- Class code ----------------------------------------------------------------------

// Constructor with initial values. The constructor without parameters and deconstructor 
// are default, i.e. they do not contain any code (there are no dynamic resources).
myvector::myvector(const double a, const double b, const double c ) {
    this->x = a;
    this->y = b;
    this->z = c;
}

// The dot product is implemented by a method, because we have already used two
// possible ways of overloading the '*' operator for other multiplications.
double myvector::dot_product( const class myvector& other ) const {
    double result = this->x * other.x + this->y * other.y + this->z * other.z;
    return( result );
}

// Retrieve internal coordinates of vector - data is exported as a tuple container.
std::tuple<double,double,double> myvector::get_coordinates() const {
    std::tuple<double,double,double> coord{ this->x, this->y, this->z };
    return( coord );
}

// Compue the length of the vector. Note: Do not use the pow() function, because it 
// introduces additional errors in terms of accuracy. Just use multiplications.
double myvector::length() {
    double length;

    length = std::sqrt( (this->x * this->x) + (this->y * this->y) + (this->z * this->z) );
    return( length );
}

// Scale the vector by a factor
void myvector::scale( const double f ) {
    this->x *= f;
    this->y *= f;
    this->z *= f;
}

// === Operators =======================================================================

// Add two vectors
const class myvector myvector::operator+( const class myvector& other ) {
    class myvector result = *this;
    result.x = this->x + other.x;
    result.y = this->y + other.y;
    result.z = this->z + other.z;
    return( result );
}

// Subtract one vector from another vector
const class myvector myvector::operator-( const class myvector& other ) {
    class myvector result = *this;
    result.x = this->x - other.x;
    result.y = this->y - other.y;
    result.z = this->z - other.z;
    return( result );
}

// This scales the vector by a factor. The scale() member already does this.
// If you want to multiply two vectors, then the result is a matrix. This is
// not implemented.
const class myvector myvector::operator*( const double f ) {
    class myvector result = *this;
    result.scale( f );
    return( result );
}

// Cross product of two vectors.
//
// cx = ay bz − az by
// cy = az bx − ax bz
// cz = ax by − ay bx
//
// a is "this", b is "other"
const class myvector myvector::operator*( const class myvector& other ) {
    double new_x = this->y * other.z - this->z * other.y;
    double new_y = this->z * other.x - this->x * other.z;
    double new_z = this->x * other.y - this->y * other.x;
    class myvector result( new_x, new_y, new_z );
    return( result );
}

// Comparison operators - small and larger are easy, equal and not equal are
// hard, because floating point arithmetic introduces errors. So we compare
// differences against the DBL_EPSILON constant (defined in the cfloat header).
//
// See https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
// for more details.

const bool myvector::operator==( class myvector& other ) {
    double length_this  = this->length();
    double length_other = other.length();

    // Constant DBL_EPSILON is defined in the cfloat header.
    // fabs() is the absolute value function for floating point numbers (defined in cmath).
    if ( fabs( length_this - length_other ) < DBL_EPSILON ) {
        return( true );
    }
    else {
        return( false );
    }
}

const bool myvector::operator!=( class myvector& other ) {
    double length_this  = this->length();
    double length_other = other.length();

    // Constant DBL_EPSILON is defined in the cfloat header.
    // fabs() is the absolute value function for floating point numbers (defined in cmath).
    if ( fabs( length_this - length_other ) > DBL_EPSILON ) {
        return( true );
    }
    else {
        return( false );
    }
}

const bool myvector::operator<( class myvector& other ) {
    double length_this  = this->length();
    double length_other = other.length();

    return( length_this < length_other );
}

const bool myvector::operator>( class myvector& other ) {
    double length_this  = this->length();
    double length_other = other.length();

    return( length_this > length_other );
}

// Overload output stream operator in order to pretty print vectors.
// Important: This is _not_ a member function! It is a global override,
// because output streams need to use this function. Basically the code
// overloads the global << operator.
//
// The purpose of overloading output stream operators is to format the
// instance of the object in a sensible manner.

std::ostream& operator<<( std::ostream& stream, const class myvector& other ) {
    std::tuple<double,double,double> values = other.get_coordinates();
    stream << "<" << std::get<0>(values) << "," << std::get<1>(values) << "," << std::get<2>(values) << ">";
    return(stream);
}

// Here be the overloaded input stream operator. In order to implement this, you have
// to decide what a vector should look like as text / binary. Then you have to implement
// a suitable parser that extracts the x, y, and z values (including input validation).

// std::istream& operator>>( std::istream& stream, const class myvector& other ) {…}

#endif
