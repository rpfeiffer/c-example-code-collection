# C++ Code Examples

The C++ code examples show useful programming techniques. The examples are no full exercises, just demonstrations. The code features
a Bash script that can build all the examples. There is also a project file for the [Ninja build system](https://ninja-build.org/).

## C++ Standard

The recommendation is to use C++11, or better C++14 and later C++ standards. Clang and GCC support C++20. Once you get
acquainted with the basics, then you should absolutely take a look at the [C++ Core Guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines).
Make sure to look at a few rules at a time, not all of them.

- [C++ compiler support](https://en.cppreference.com/w/cpp/compiler_support)

## Importing Namespaces

It is convenient to load full namespaces. You should think before you do that. Using the full namespace of libraries can lead to collisions
with function declarations. The compiler might pick the wrong function if you have multiple functions with the same parameters in more than one
namespace.

- [C++ Best Practice #1: Don't simply use: using namespace std; 👋](https://fluentprogrammer.com/dont-use-using-namespace-std/)
- [C++ Without Using Namespace Std: Best Practices in Namespace Management](https://www.codewithc.com/c-without-using-namespace-std-best-practices-in-namespace-management/)

## Compiler Flags, Security, and Optimisation

Do not use optimisation early when creating code. Don't use it until you have a solid skeleton and a working prototype. A sensible flags combination
to get started is this one:
```
-Wall -Werror -Wpedantic -I. -O0 -fno-omit-frame-pointer -std=c++20
```
If you want, then you can create code with all the features of your local processor by using the _-march=native_ flag. Once everything works you
can change the flags. A suggestions for a complete set of compiler flags for the Clang++ compiler is this one:
```
cflags = -Wall -Werror -I. -O3 -march=native -fno-omit-frame-pointer
cxxflags = ${cflags} -std=c++20 -flto -DJEMALLOC_ON=1 -fsanitize-minimal-runtime -fsanitize=undefined
secflags = -fPIE -fstack-protector -fstack-clash-protection -mshstk -fstrict-flex-arrays=3 -fcf-protection=full -Wformat -Wformat-security -Werror=format-security -Wnull-pointer-arithmetic -D_FORTIFY_SOURCE=3
```
Sources explaining some of the security compiler flags are:

- [A developer’s guide to secure coding with FORTIFY\_SOURCE](https://developers.redhat.com/articles/2023/07/04/developers-guide-secure-coding-fortifysource)
- [GCC compiler flags](https://gcc.gnu.org/onlinedocs/gcc-9.1.0/gcc/x86-Options.html)

# Unified Modelling Language (UML) Modelling

Designing classes can be done in most integrated development environments. The KDE project has a tool called [Umbrello](https://apps.kde.org/umbrello/) that
can help you with UML modelling. It can read code and display class diagrams. It can also create code from your class designs.
