#!/bin/bash
#
# Set CXX to the C++ compiler you want to use (i.e. use "export CXX=g++" in the shell or
# the environment).

CC_FILES=$(ls -1 *.cpp)

for CODE in $CC_FILES; do
    if [ "$CODE" == "parallel_algorithms.cpp" ]; then
        $CXX -Wall -Werror -Wpedantic -std=c++20 -ltbb -march=native -o ${CODE::-4} $CODE
    else
        $CXX -Wall -Werror -Wpedantic -std=c++20 -march=native -o ${CODE::-4} $CODE
    fi
done
