// =====================================================================================
//
//       Filename:  csv_sample_class.cpp
//
//    Description:  Example class that reads a CSV file and stores its content in a class instance.
//
//        Version:  1.0
//        Created:  08. april 2024 kl. 17.49 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// For the return codes.
#include <sysexits.h>

using namespace std;

// -------------------------------------------------------------------------------------------
// Constants

// Return code. You do not need to copy the values from sysexits.h, it's just an example.
const int RC_ALL_OK = EX_OK;
const int RC_NO_FILE = EX_IOERR;
//const int RC_GENERAL_ERROR = 99;
const int RC_USAGE = EX_USAGE;

// -------------------------------------------------------------------------------------------
// The CSV parser class will use a fixed format. Creating a flexible CSV parser that reads
// the number of columns and even determines the data type requires a lot more tests and
// dynamic memory management.
// 
// CSV format:
// Timestamp,Sensor-ID,Temperature

class csv {
    public:
        csv();
        csv( string &filename );
        void dump_data();
        unsigned int get_parsed_lines_number();
        bool open_file();
        bool open_file( string &filename );
        void set_filename( string &filename );
        ~csv();

    private:
        bool parse_csv_file();
        vector<string> split( const string &str, char separator );

        string csv_filename = "data.csv";
        ifstream csv_file;
        bool has_error = false;
        unsigned int parsed_lines = 0;

        vector<unsigned int> timestamp;
        vector<unsigned short> sensor_id;
        vector<double> temperature;
};

// ----------------------------------------------

// Empty constructor.
csv::csv() {
}

// Constructor with a filename
csv::csv( string &filename ) {
    set_filename( filename );
    return;
}

// Deconstructor
csv::~csv() {
    if ( csv_file.is_open() ) {
        csv_file.close();
    }
}

// Dump internal data vectors
auto csv::dump_data() -> void {
    size_t n_ts = timestamp.size();
    size_t n_sensor = sensor_id.size();
    size_t n_temp = temperature.size();

    if ( ( n_ts == n_sensor) and ( n_sensor == n_temp ) ) {
        for( size_t j=0; j<n_ts; j++ ) {
            cout << temperature.at(j) << "," << sensor_id.at(j) << "," << temperature.at(j) << endl;
        }
    }
    else {
        cerr << "ERROR: Vector mismatch." << endl;
    }
    return;
}

// Retrieve the number of parsed lines
auto csv::get_parsed_lines_number() -> unsigned int {
    return( parsed_lines );
}

// Open file
auto csv::open_file( string &filename ) -> bool {
    set_filename( filename );
    bool rc = open_file();
    return(rc);
}


// Open file for reading
auto csv::open_file( void ) -> bool {
    bool rc = false;

    try {
        csv_file.open( csv_filename, ios_base::in );
        rc = true;
    }
    catch( ifstream::failure &eh ) {
        cerr << "ERROR: Error opening file - " << eh.what() << endl;
        has_error = true;
    }
    catch( ... ) {
        cerr << "ERROR: Unknown error\n";
        has_error = true;
    }

    if ( rc == true ) {
        rc = parse_csv_file();
    }

    return(rc);
}

// Parse CSV
//
auto csv::parse_csv_file() -> bool {
    bool rc = false;
    string line;

    try {
        vector<string> tokens;
        unsigned int line_count = 0;
        unsigned int ts;
        unsigned short sensor;
        double temp;
        while( getline( csv_file, line ) ) {
            line_count++;
            tokens = split( line, ',' );
            if ( tokens.size() == 3 ) {
                // FIXME: No error checks, fixed column positions
                istringstream( tokens.at(1) ) >> ts;
                istringstream( tokens.at(2) ) >> sensor;
                istringstream( tokens.at(3) ) >> temp;
                // Add to vectors
                temperature.push_back( ts );
                sensor_id.push_back( sensor );
                temperature.push_back( temp );
            }
            else {
                cerr << "ERROR: Data format error in line " << line_count << endl;
                has_error = true;
            }
        }
        parsed_lines = line_count;
    }
    catch( ifstream::failure &eh ) {
        cerr << "ERROR: Error reading file - " << eh.what() << endl;
        has_error = true;
    }
    catch( ... ) {
        cerr << "ERROR: Unknown error" << endl;
        has_error = true;
    }

    return(rc);
}

// Setter function (lacking input checks)
auto csv::set_filename( string &filename ) -> void {
    // FIXME: Here should be checks for a valid filename (i.e. no "..", "/", "\", etc.).
    csv_filename = filename;
    return;
}

// Split string into tokens separated by a separator. This only work for non-string
// columns. For mixed content it is advised to check the line character by character and make copies.
auto csv::split( const string &str, char separator ) -> vector<string> {
    vector<string> tokens;

    string part;
    stringstream ss(str);
    while (ss >> part) {
        tokens.push_back( part );
        if ( ss.peek() == separator ) {
            ss.ignore();
        }
    }

    return(tokens);
}

// -------------------------------------------------------------------------------------------
// main()

int main( int argc, char **argv ) {
    int return_code = RC_ALL_OK;
    class csv csv_data;

    // We don't parse the command line. If there is a parameter, then we will interpret it as a file.
    if ( argc < 2 ) {
        cerr << "ERROR: You must provide the name of an input file!\n";
        return(RC_USAGE);
    }
    string filename(argv[1]);
    if ( csv_data.open_file( filename ) ) {
        csv_data.dump_data();
    }
    else {
        return_code = RC_NO_FILE;
    }

    return(return_code);
}
