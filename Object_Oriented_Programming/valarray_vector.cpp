// =====================================================================================
//
//       Filename:  valarray_vector.cpp
//
//    Description:  Comparison of valarray and vector containers.
//
//        Version:  1.0
//        Created:  28. okt. 2024 kl. 20.24 +0100
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <chrono>
#include <iostream>
#include <random>
#include <valarray>
#include <vector>

#ifdef __unix__
#include <sysexits.h>   // Usually not availble on non-UNIX systems
#else
#define EX_OK		0	/* successful termination */
#define EX_DATAERR	65	/* data format error */
#endif

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;
using namespace std;

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

const unsigned int N_ELEMENTS = 1000000;

//----------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    vector<double> vec_values;
    random_device rdev;
    uniform_real_distribution<double> p( -10000.0, 10000.0 );
    int rc = EX_OK;

    // Reserve space
    vec_values.reserve( N_ELEMENTS );

    // Fill data structures
    auto t1 = high_resolution_clock::now();
    for( unsigned int i=0; i<N_ELEMENTS; i++ ) {
        vec_values.push_back( p(rdev) );
    }
    auto t2 = high_resolution_clock::now();
    duration<double, std::milli> ms_double = t2 - t1;
    cout << "Fill vector   : " << ms_double.count() << " ms" << endl;
    t1 = high_resolution_clock::now();
    vector<double> v;
    for( unsigned int i=0; i<N_ELEMENTS; i++ ) {
        v.push_back( p(rdev) );
    }
    valarray<double> val_values( v.data(), v.size() );
    t2 = high_resolution_clock::now();
    ms_double = t2 - t1;
    cout << "Fill valarray : " << ms_double.count() << " ms" << endl << endl;

    // Calculate square of all values
    t1 = high_resolution_clock::now();
    double tval;
    for( unsigned int i=0; i<N_ELEMENTS; i++ ) {
        tval = vec_values.at(i);
        vec_values.at(i) = cos(tval);
    }
    t2 = high_resolution_clock::now();
    ms_double = t2 - t1;
    cout << "cos()  vector  : " << ms_double.count() << " ms" << endl;
    t1 = high_resolution_clock::now();
    auto val_result = cos(val_values);
    t2 = high_resolution_clock::now();
    ms_double = t2 - t1;
    cout << "cos() valarray : " << ms_double.count() << " ms" << endl;
    cout << "# val_values   : " << val_values.size() << endl;
    cout << "# val_result   : " << val_result.size() << endl << endl;

    return(rc);
}
