// =====================================================================================
//
//       Filename:  modifiers.cpp
//
//    Description:  Use of modifiers with data types.
//
//        Version:  1.0
//        Created:  05. mars 2025 kl. 21.23 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o modifiers modifiers.cpp
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o modifiers modifiers.cpp

#include <cstdint>
#include <iostream>
#include <string>

using namespace std;

class book {
    public:
        book() = default;
        book( const string& title );
        book( const string& title, const string& author );
        book( const string& title, const string& author, uint16_t year );
        book( const string& title, const string& author, uint16_t year, uint32_t pages );
        ~book() = default;
        void get_title( string& title_store ) const;
        string get_title() const;
        void get_author( string& author_store ) const;
        string get_author() const;
        uint16_t get_year() const;
        uint32_t get_pages() const;
        void set_title( const string& title );
        void set_author( const string& author );
        void set_year( uint16_t year );
        void set_pages( uint32_t pages );

        const uint16_t default_year = 0;
        const uint32_t default_pages = 0;

    private:
        string title;
        string author;
        uint16_t year = default_year;
        uint32_t pages = default_pages;
};

book::book( const string& title ) {
    set_title( title );
}

book::book( const string& title, const string& author ) {
    set_title( title );
    set_author( author );
}

book::book( const string& title, const string& author, uint16_t year ) {
    set_title( title );
    set_author( author );
    set_year( year );
}

book::book( const string& title, const string& author, uint16_t year, uint32_t pages ) {
    set_title( title );
    set_author( author );
    set_year( year );
    set_pages( pages );
}

auto book::set_title( const string& title ) -> void {
    this->title = title;
}

auto book::set_author( const string& author ) -> void {
    this->author = author;
}

auto book::set_year( uint16_t year ) -> void {
    this->year = year;
}

auto book::set_pages( uint32_t pages ) -> void {
    this->pages = pages;
}

auto book::get_title( string& title_store ) const -> void {
    title_store = this->title;
}

auto book::get_title() const -> string {
    return( this->title );
}

auto book::get_author( string& author_store ) const -> void {
    author_store = this->author;
}

auto book::get_author() const -> string {
    return( this->author );
}

auto book::get_year() const -> uint16_t {
    return( this->year );
}

auto book::get_pages() const -> uint32_t {
    return( this->pages );
}

auto main( int argc, char **argv ) -> int {
    class book mybook( "1984", "George Orwell", 1949, 328 );
    string myauthor;
    string mytitle;

    mybook.get_author( myauthor );
    mybook.get_title( mytitle );

    cout << "Title : " << mytitle << endl
         << "Author: " << myauthor << endl
         << "Year  : " << mybook.get_year() << endl
         << "Pages : " << mybook.get_pages() << endl;

    return(0);
}
