// =====================================================================================
//
//       Filename:  class_temperature_test.cc
//
//    Description:  Test-drives temperature class.
//
//        Version:  1.0
//        Created:  28. feb. 2024 kl. 12.57 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <iostream>
#include "class_temperature.h"

// Do not use macros or #define constants in C++!
const int ALL_OK = 0;

using namespace std;

int main( int argc, char **argv ) {
    class temperature t;

    cout << "Temperature class instance with defaults:" << endl
         << "t_c = " << t.get_tc() << endl
         << "t_k = " << t.get_tk() << endl
         << "t_r = " << t.get_tr() << endl
         << endl;

    if ( not t.set_tc( 3422.0 ) ) {
        cerr << "Error converting temperature!" << endl;
    }
    else {
        cout << "Temperature class set to melting point of wolfram:" << endl;
        t.print();
    }

    return(ALL_OK);
}
