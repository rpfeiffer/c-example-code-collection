// =====================================================================================
//
//       Filename:  operator_overloading_vector_test.cpp
//
//    Description:  Test code für vector class.
//
//        Version:  1.0
//        Created:  06. mai 2024 kl. 18.23 +0200
//       Revision:  none
//       Compiler:  clang++/g++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// This is a demonstration of how to overload operators. It uses mathematical 3d vectors
// and some basic operations (add, subtract, multiply by scalar, determine length, and
// comparison operators). The methods do not contain any error checks. Also, the coordinates
// are represented by double floating point numbers. It is possible to convert the class
// to templates in order to include float or long double.
//
// In order to verify the correct operation of the 3d vector class, you can implement
// unit tests. See the operator_overloading_vector_catch2.cpp how to do this with the Catch2
// framework.

#ifdef UNIT_TEST
#include <cassert>
#endif

#include <iostream>
#include "operator_overloading_vector.h"

// main() function
int main( int argc, char **argv ) {
    class myvector vec1( 1.0, 2.0, 3.5 );
    class myvector vec2( 23.0, 42.3, 38.5 );

    std::cout << "Vector1        : " << vec1 << std::endl;
    std::cout << "Vector1 length : " << vec1.length() << std::endl << std::endl;

    std::cout << "Vector2        : " << vec2 << std::endl;
    std::cout << "Vector2 length : " << vec2.length() << std::endl << std::endl;

    class myvector sum = vec1 + vec2;
    std::cout << "Vector sum     : " << sum << std::endl;
    std::cout << "Vector length  : " << sum.length() << std::endl << std::endl;

    std::cout << "vec1 > sum     : " << ( ( vec1 > sum  ) ? "true" : "false" ) << std::endl;
    std::cout << "sum  > vec1    : " << ( ( sum > vec1  ) ? "true" : "false" ) << std::endl;
    std::cout << "vec1 < vec2    : " << ( ( vec1 > vec2 ) ? "true" : "false" ) << std::endl;

    class myvector vec3 = vec1 * vec2;
    std::cout << "Vector3 = Vector1 × Vector2" << std::endl;
    std::cout << "Vector3        : " << vec3 << std::endl;
    std::cout << "Vector3 length : " << vec3.length() << std::endl << std::endl;

    double dot_product = vec1.dot_product( vec2 );
    std::cout << "vec1 · vec2    : " << dot_product << std::endl << std::endl;

#ifdef UNIT_TEST
    // Sprinkling unit tests into the production code by using the preprocessor is
    // not recommended. It makes the code hard to read and to maintain.
    class myvector nullvektor;
    std::assert( nullvektor.length() == 0.0 );
#endif

    return(0);
}
