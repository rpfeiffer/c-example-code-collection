// =====================================================================================
//
//       Filename:  array_and_vector.cpp
//
//    Description:  Code shows the use of dynamic and fixed-size array containers.
//
//        Version:  1.0
//        Created:  28. feb. 2024 kl. 12.57 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <array>     // available since C++11
#include <iostream>
#include <vector>

// You can import objects and function into the global namespace selectively.
using std::array;
using std::cout;
using std::endl;
using std::vector;

// Do not use macros or #define constants in C++!
const int ALL_OK = 0;

// main(); note the use of the sizeof() function. It doesn not show the actual size of the
// data structures, just the metadata. Use the size() method to see the number of elements.
//
// The capacity() method shows how much space is reserved for elements. Vectors do not grow
// by single elements.

int main( int argc, char **argv ) {
    // Fixed-size arrays
    array<int,5> temperature = { 42, 23, 13, 7, 19 };
    array<double,5> values;

    cout << "Array temperature has " << temperature.size() << " elements. (" << sizeof(temperature) << " bytes used)" << endl;
    cout << "Array values has " << values.size() << " elements. ( " << sizeof(values) << " bytes used)" << endl;
    values =  { 3.14, 2.178, 4.5, 11.11, 13.15 };
    cout << "Array values has " << values.size() << " elements. ( " << sizeof(values) << " bytes used)" << endl << endl;

    // Vectors (dynamically-sized arrays)
    vector<int> temperature_v = { 42, 23, 13, 7, 19 };
    vector<double> values_v;

    cout << "--- Just constructed." << endl;
    cout << "Vector temperature has " << temperature_v.size() << " elements. (" << sizeof(temperature_v) << " bytes used, can hold " << temperature_v.capacity() << " elements)" << endl;
    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    // Add some mathematical constants
    values_v.push_back( 3.14159265358979323846 );
    values_v.push_back( 1.41421356237309504880 );
    values_v.push_back( 1.28242712910062263687 );
    values_v.push_back( 2.66514414269022518865 );
    values_v.push_back( 1.32471795724474602596 );

    temperature_v.pop_back(); // Removes last element

    cout << "Vector temperature has " << temperature_v.size() << " elements. (" << sizeof(temperature_v) << " bytes used, can hold " << temperature_v.capacity() << " elements)" << endl;
    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    cout << "--- .push_back()" << endl;
    double v = 2.0;
    for( unsigned short i=0; i<15; i++ ) {
        values_v.push_back(v);
        v = v * v;
    }

    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    v = 3.0;
    for( unsigned short i=0; i<15; i++ ) {
        values_v.push_back(v);
        v = v * v;
    }

    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    v = 5.0;
    for( unsigned short i=0; i<30; i++ ) {
        values_v.push_back(v);
        v = v * v;
    }

    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    // Clear vectors.
    cout << "--- .clear()" << endl;
    temperature_v.clear();
    values_v.clear();

    cout << "Vector temperature has " << temperature_v.size() << " elements. (" << sizeof(temperature_v) << " bytes used, can hold " << temperature_v.capacity() << " elements)" << endl;
    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    // Free space
    cout << "--- .shrink_to_fit() - usually not necessary" << endl;
    temperature_v.shrink_to_fit();
    values_v.shrink_to_fit();

    cout << "Vector temperature has " << temperature_v.size() << " elements. (" << sizeof(temperature_v) << " bytes used, can hold " << temperature_v.capacity() << " elements)" << endl;
    cout << "Vector values has " << values_v.size() << " elements. (" << sizeof(values_v) << " bytes used, can hold " << values_v.capacity() << " elements)" << endl;

    return(ALL_OK);
}
