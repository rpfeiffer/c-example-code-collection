#include <cmath>
#include <iostream>
#include "vector2d.hpp"

using namespace std;

vector_2d::vector_2d( double x, double y )  {
    this->x = x;
    this->y = y;
    update();
}

class vector_2d vector_2d::add( class vector_2d& a ) {
    class vector_2d result;
    double new_x = this->x + a.get_x();
    double new_y = this->y + a.get_y();
    result.set_xy( new_x, new_y );
    return( result );
}

double vector_2d::cross( class vector_2d& a ) {
    double result = this->x * a.get_y() - this->y * a.get_x();
    return( result );
}

double vector_2d::dot( class vector_2d& a ) {
    double result = this->x * a.get_x() + this->y * a.get_y();
    return( result );
}

void vector_2d::dump() {
    cout << "x = " << x << endl
         << "y = " << y << endl
         << "r = " << r << endl
         << "theta = " << theta << endl;
}

double vector_2d::get_r() const {
    return( r );
}

double vector_2d::get_x() const {
    return( x );
}

double vector_2d::get_y() const {
    return( y );
}

void vector_2d::set_x( double x ) {
    this->x = x;
    update();
}

void vector_2d::scalar_mult( double s ) {
    x = x * s;
    y = y * s;
    update();
}

void vector_2d::set_y( double y ) {
    this->y = y;
    update();
}

void vector_2d::set_xy( double x, double y ) {
    set_x( x );
    set_y( y );
}

void vector_2d::show() {
    cout << "( " << get_x() << "," << get_y() << " ) " << endl;
}

void vector_2d::update() {
    r = sqrt( x*x + y*y );
    theta = 1.0 / tan( y / x );
}
