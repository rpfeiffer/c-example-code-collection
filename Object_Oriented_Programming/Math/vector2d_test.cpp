#include <iostream>
#include "vector2d.hpp"
#include "vector2d.hpp"

using namespace std;

int main() {
    class vector_2d v1( 2.0, 7.0 );
    class vector_2d v2( 5.0, 9.0 );

    cout << "v1 Länge = " << v1.get_r() << endl;
    cout << "v2 Länge = " << v2.get_r() << endl;

    class vector_2d v3 = v1.add( v2 );
    v3.show();
    v1.dump();
    v2.dump();
    v3.dump();
    return(0);
}
