// =====================================================================================
//
//       Filename:  vector_3d.cpp
//
//    Description:  Sample code for using a threedimensional array/vector.
//
//        Version:  1.0
//        Created:  04. april 2024 kl. 22.58 +0200
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
//
// Multidimensional arrays are implemented with the C++ STL by using encapsulated vector
// objects. The class shows how to do this for three dimensions. If you need to do more
// operations with multidimensional arrays, then you might want to take a look at the
// follwing libraries:
//
// - Eigen - http://eigen.tuxfamily.org/index.php?title=Main_Page
// - GNU Scientific Library (GSL) - http://www.gnu.org/software/gsl/
// - Armadillo - http://arma.sourceforge.net/
// - Boost.MultiArray - https://www.boost.org/doc/libs/1_84_0/libs/multi_array/doc/index.html
// - std::mdspan - https://en.cppreference.com/w/cpp/container/mdspan

#include <iostream>
#include <vector>

using namespace std;

// Template class that builds a 3d vector container.
template<typename T>
class Vector3D {
private:
    vector<vector<vector<T>>> data;

public:
    // Constructors
    Vector3D() {}

    Vector3D(size_t x, size_t y, size_t z, const T& value = T()) {
        data.resize(x, vector<vector<T>>(y, vector<T>(z, value)));
    }

    // Deconstructor
    ~Vector3D() {
        cout << "Vector3D object went out of scope." << endl;
    }

    void resize(size_t x, size_t y, size_t z, const T& value = T()) {
        data.resize(x, vector<vector<T>>(y, vector<T>(z, value)));
    }

    void set(size_t x, size_t y, size_t z, const T& value) {
        if (x >= data.size() || y >= data[x].size() || z >= data[x][y].size()) {
            // Out of bounds error condition. This could be handled by an exception.
            return;
        }
        data[x][y][z] = value;
    }

    T get(size_t x, size_t y, size_t z) const {
        if (x >= data.size() || y >= data[x].size() || z >= data[x][y].size()) {
            // Out of bounds, return default value
            return( T() );
        }
        return data[x][y][z];
    }

    void dump3D( void ) {
        for( auto x = data.begin(); x != data.end(); x++ ) {
            for( auto y = x->begin(); y != x->end(); y++ ) {
                for( auto z = y->begin(); z != y->end(); z++ ) {
                    cout << *z << " ";
                }
                cout << endl;
            }
            cout << endl;
        }
    }

    size_t sizeX() const {
        return( data.size() );
    }

    size_t sizeY() const {
        return( data.empty() ? 0 : data[0].size() );
    }

    size_t sizeZ() const {
        return( data.empty() || data[0].empty() ? 0 : data[0][0].size() );
    }
};

int main() {
    Vector3D<int> threeD(3, 3, 3); // Create a 3x3x3 3D vector filled with zeros

    // Set values
    threeD.set(0, 0, 0, 101);
    threeD.set(0, 0, 1, 102);
    threeD.set(0, 0, 2, 103);
    threeD.set(0, 1, 0, 105);
    threeD.set(0, 1, 2, 106);
    threeD.set(0, 2, 0, 107);
    threeD.set(0, 2, 1, 108);
    threeD.set(0, 2, 2, 109);
    threeD.set(1, 0, 0, 110);
    threeD.set(1, 0, 1, 111);
    threeD.set(1, 0, 2, 112);
    threeD.set(1, 1, 0, 113);
    threeD.set(1, 1, 2, 114);
    threeD.set(1, 2, 0, 115);
    threeD.set(1, 2, 1, 116);
    threeD.set(1, 2, 2, 117);
    threeD.set(2, 0, 0, 118);
    threeD.set(2, 0, 1, 119);
    threeD.set(2, 0, 2, 120);
    threeD.set(2, 1, 0, 121);
    threeD.set(2, 1, 1, 123);
    threeD.set(2, 1, 2, 124);
    threeD.set(2, 2, 0, 125);
    threeD.set(2, 2, 1, 126);
    threeD.set(2, 2, 2, 127);

    // Print values
    threeD.dump3D();

    return(0);
}
