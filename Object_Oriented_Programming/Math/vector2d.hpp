#ifndef H_VECTOR2D
#define H_VECTOR2D

class vector_2d {
    public:
        vector_2d() = default;
        vector_2d( double x, double y );
        ~vector_2d() = default;

        class vector_2d add( class vector_2d& a );
        double cross( class vector_2d& a );
        double dot( class vector_2d& a );
        void scalar_mult( double s );

        void dump();
        double get_r() const;
        double get_x() const;
        double get_y() const;
        void set_x( double x );
        void set_y( double y );
        void set_xy( double x, double y );
        void show();

    private:
        void update();
        double x = 0.0;
        double y = 0.0;
        double r = 0.0;
        double theta = 0.0;
};
#endif
