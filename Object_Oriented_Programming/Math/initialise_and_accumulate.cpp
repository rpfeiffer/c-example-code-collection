// =====================================================================================
//
//       Filename:  initialise_and_accumulate.cpp
//
//    Description:  Initialise and accumulate series of numbers.
//
//        Version:  1.0
//        Created:  05. mars 2025 kl. 17.06 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o initialise_and_accumulate initialise_and_accumulate.cpp
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o initialise_and_accumulate initialise_and_accumulate.cpp

#include <algorithm>
#include <iostream>
#include <random>

using namespace std;

const int minimum = -250000;
const int maximum = 250000;

int fill_generator() {
    static random_device r_device;
    static mt19937_64 prng( r_device() );
    static uniform_int_distribution<int> uniform_distribution( minimum, maximum );
    return( uniform_distribution( prng ) );
}

int main( int argc, char **argv ) {
    const unsigned int max_numbers = 5000;
    vector<unsigned int> value_simple;
    vector<unsigned int> value_ascending;
    vector<int> value_random;

    // Initialise vectors
    value_simple.resize( max_numbers );
    fill( value_simple.begin(), value_simple.end(), 42 );

    value_ascending.resize( max_numbers );
    iota( value_ascending.begin(), value_ascending.end(), 1 );

    value_random.resize( max_numbers );
    generate( value_random.begin(), value_random.end(), fill_generator );

    // Calculate sum of vectors
    auto sum_simple    = reduce( value_simple.begin(), value_simple.end() );
    auto sum_ascending = reduce( value_ascending.begin(), value_ascending.end() );
    auto sum_random    = reduce( value_random.begin(), value_random.end() );

    cout << "Sum (simple)    = " << sum_simple << endl
         << "Sum (ascending) = " << sum_ascending << endl
         << "Sum (random)    = " << sum_random << endl;
}
