// =====================================================================================
//
//       Filename:  random_generator.cpp
//
//    Description:  Pseudo-random numer generation
//
//        Version:  1.0
//        Created:  05. mars 2025 kl. 16.28 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o random_generator random_generator.cpp
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o random_generator random_generator.cpp
//
// Do not use C's rand() functions! Always use the distributions of the C++ random library.

#include <iomanip>
#include <iostream>
#include <random>

using namespace std;

int main( int argc, char **argv ) {
    const unsigned int max_numbers = 50;
    const int minimum = -100;
    const int maximum = 100;
    const double minimum_d = -100;
    const double maximum_d = 100;

    // Random device for generating the seed; device can be hardware or software, the C++
    // run-time decides.
    random_device r_device;
    // Initialise PRNG algorithm with seed value.
    mt19937_64 prng( r_device() );
    // Initialise random distributions with limits.
    uniform_int_distribution<int> uniform_distribution( minimum, maximum );
    normal_distribution n_distribution{ minimum_d, maximum_d }; // Normal distribution uses floating points

    // Retrieve numbers
    for( auto i=0; i<max_numbers; i++ ) {
        cout << setw(15) << uniform_distribution( prng ) << " | " << setw(15) << n_distribution( prng ) << endl;
    }
    return(0);
}
