// =====================================================================================
//
//       Filename:  loops.cc
//
//    Description:  Code shows the use of loops.
//
//        Version:  1.0
//        Created:  28. feb. 2024 kl. 12.57 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <iostream>
#include <string>
#include <vector>

using namespace std;

// We need a class definition to play with.

// Do not use macros or #define constants in C++!
const int ALL_OK = 0;

const unsigned short MAX_NUMBERS = 20;

int main( int argc, char **argv ) {
    vector<double> numbers;

    // Initialise vector (which is the first loop type)
    double v = 0.123;
    for( unsigned short i=0; i<MAX_NUMBERS; i++ ) {
        numbers.push_back(v);
        v = v * 2.1;
    }

    // for-each loop (C++11 and later); the auto datatype is determined by the compiler
    cout << "numbers = { ";
    for( auto n : numbers ) {
        cout << n << " ";
    }
    cout << "}" << endl;

    // C++ knows the while() loop, too.
    unsigned int i = 0;
    cout << "numbers = { ";
    while( i < numbers.size() ) {
        cout << numbers.at(i) << " ";
        i++;
    }
    cout << "}" << endl;

    // C++ also knows the do/while() loop. Do not use it, because the condition is hard to see
    // (because it's at the end).
    unsigned j = 0;
    cout << "numbers = { ";
    do {
        cout << numbers.at(j) << " ";
        j++;
    } while( j < numbers.size() );
    cout << "}" << endl;

    // Loop using iterator objects. All C++ containers have iterators that act as "cursor"
    // in order to walk through a container. The iterator syntax is a bit "noisy", because
    // you have to list the datatype explicitly and name the iterator. Iterator work just
    // like counters or pointer. They can be increased or decreased, and the point to the
    // element.
    cout << "numbers = { ";
    for( vector<double>::iterator it = numbers.begin();
         it != numbers.end();
         it++ ) {
        cout << *it << " ";
    }
    cout << "}" << endl;

    return(ALL_OK);
}
