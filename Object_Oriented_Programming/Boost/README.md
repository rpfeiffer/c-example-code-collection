Boost Libraries
---------------

The [Boost libraries](https://www.boost.org/) are free peer-reviewed portable C++ source libraries. They
work well with existing C++ standards and are reference implementations suitable for eventual standardization.

## Usage

Most Boot libraries are header-only and do not require any linking with binary libraries. This facilitates
creating portable or static binaries.
