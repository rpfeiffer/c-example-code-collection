// =====================================================================================
//
//       Filename:  integer_data_types.cpp
//
//    Description:  Use data types that reflect their actual size.
//
//        Version:  1.0
//        Created:  ti. 19. mars 21:26:11 +0100 2024
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

// The standard integer types depend on the processor architecture. int, long, and
// long long integer do not need to have different sizes. The compiler just has to make sure
// that long is bigger than int. It is better to use integer types defined in <cstdint>
// because it mentions the bit size in the data type.
//
// https://en.cppreference.com/w/cpp/types/integer
//
// <cstdint> also defines size_t which can hold all possible object sizes.

// If you need 128 bit integer types, then you will probably have to use a library.
//
// - Boost Multiprecision https://www.boost.org/doc/libs/1_84_0/libs/multiprecision/doc/html/boost_multiprecision/intro.html
// - GCC/Clang/ICC on 64-bit platforms (data type is __uint128_t / __int128)
// - GMP has big number support
// - uint128_t implementation - https://github.com/calccrypto/uint128_t

#include <cstdint>
#include <iostream>

using std::cout;
using std::endl;

int main( int argc, char **argv ) {
    unsigned char b = 0;
    unsigned short int s = 0;
    unsigned int i = 0;
    unsigned long l = 0;
    unsigned long long ll = 0;

    uint8_t b_ = 0;
    uint16_t s_ = 0;
    uint32_t i_ = 0;
    uint64_t l_ = 0;

    cout << "Size of unsigned char      : " << sizeof(b) << endl;
    cout << "Size of unsigned short int : " << sizeof(s) << endl;
    cout << "Size of unsigned int       : " << sizeof(i) << endl;
    cout << "Size of unsigned long      : " << sizeof(l) << endl;
    cout << "Size of unsigned long long : " << sizeof(ll) << endl << endl;

    cout << "Size of uint8_t            : " << sizeof(b_) << endl;
    cout << "Size of uint16_t           : " << sizeof(s_) << endl;
    cout << "Size of uint32_t           : " << sizeof(i_) << endl;
    cout << "Size of uint64_t           : " << sizeof(l_) << endl;

    return(0);
}
