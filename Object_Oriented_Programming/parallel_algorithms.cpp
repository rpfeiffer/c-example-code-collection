// =====================================================================================
//
//       Filename:  parallel_algorithms.cpp
//
//    Description:  Code shows how to use parallel algorithms.
//
//        Version:  1.0
//        Created:  on. 20. mars 12:39:36 +0100 2024
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -ltbb -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -ltbb -o array_and_vector array_and_vector.cc
//
// IMPORTANT: In order to use the parallel algorithms you need to have the oneTBB library
// installed. On Debian/Ubuntu systems you can use:
//
// apt install libtbb-dev
// 
// Note the extra flag "-ltbb" when invoking the compiler.

#include <algorithm>
#include <chrono>           // C++11, C++20
#include <execution>        // C++17
#include <iomanip>
#include <iostream>
#include <random>
#include <tbb/tbb.h>
#include <vector>

#include <sys/time.h>

using namespace std;

// Maximum number of elements
const unsigned int ELEMENTS = 50000000;

// Intialise function - features a random number generator

void init_vector( vector<double>& v, size_t n ) {
    // Random number generation; PRNG is seeded by a local random device
    random_device rd{};
    mt19937_64 PRNG( rd() );
    normal_distribution<double> normal_dist{ -1000.0, 1000.0 };

    // Allocate elements (optional)
    v.reserve(n);
    // Seed vector
    for( size_t i=0; i<n; i++ ) {
        v.push_back( normal_dist(PRNG) );
    }

    return;
}

// main() function

int main( int argc, char **argv ) {
    vector<double> gauss;

    // Initialise array
    auto start = std::chrono::steady_clock::now();
    init_vector( gauss, ELEMENTS );
    auto end = std::chrono::steady_clock::now();
    auto delta = std::chrono::duration_cast<std::chrono::microseconds>( end - start );
    cout << "init_vector() : " << setw(10) << delta.count() << " ms" << endl << endl;

    // Find minimum and maximum
    start = std::chrono::steady_clock::now();
    const auto min = min_element( gauss.begin(), gauss.end() );
    const auto max = max_element( gauss.begin(), gauss.end() );
    end = std::chrono::steady_clock::now();
    delta = std::chrono::duration_cast<std::chrono::microseconds>( end - start );
    cout << "Min/Max : " << setw(10) << delta.count() << " ms" << endl;
    cout << "Min/Max : " << setw(10) << *min << " / " << setw(10) << *max << endl << endl;

    // Find minimum and maximum (parallel)
    start = std::chrono::steady_clock::now();
    const auto min2 = min_element( std::execution::par, gauss.begin(), gauss.end() );
    const auto max2 = max_element( std::execution::par, gauss.begin(), gauss.end() );
    end = std::chrono::steady_clock::now();
    delta = std::chrono::duration_cast<std::chrono::microseconds>( end - start );
    cout << "Min/Max : " << setw(10) << delta.count() << " ms" << endl;
    cout << "Min/Max : " << setw(10) << *min2 << " / " << setw(10) << *max2 << endl << endl;

    // Find minimum and maximum (parallel, not in sequence)
    start = std::chrono::steady_clock::now();
    const auto min3 = min_element( std::execution::par, gauss.begin(), gauss.end() );
    const auto max3 = max_element( std::execution::par, gauss.begin(), gauss.end() );
    end = std::chrono::steady_clock::now();
    delta = std::chrono::duration_cast<std::chrono::microseconds>( end - start );
    cout << "Min/Max : " << setw(10) << delta.count() << " ms" << endl;
    cout << "Min/Max : " << setw(10) << *min3 << " / " << setw(10) << *max3 << endl << endl;

    return(0);
}
