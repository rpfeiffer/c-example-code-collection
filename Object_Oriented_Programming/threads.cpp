// =====================================================================================
//
//       Filename:  threads.cpp
//
//    Description:  Code shows how to use threads and async calls of functions.
//
//        Version:  1.0
//        Created:  on. 20. mars 12:39:36 +0100 2024
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <vector>

#include <sys/time.h>

using namespace std;

// Maximum number of elements
const unsigned int ELEMENTS = 50000000;

// Intialise function - features a random number generator

void init_vector( vector<unsigned int>& v, size_t n ) {
    // Random number generation
    mt19937_64 PRNG;
    uniform_int_distribution<unsigned int> random_dist;

    // Init PRNG with seed
    struct timeval now = { 0, 0 };
    gettimeofday( &now, nullptr );
    uint32_t seed = (uint32_t)now.tv_usec;
    PRNG.seed(seed);

    // Allocate elements (optional)
    v.reserve(n);
    // Seed vector
    for( size_t i=0; i<n; i++ ) {
        v.push_back( random_dist(PRNG) );
    }

    return;
}

// main() function

int main( int argc, char **argv ) {
    size_t max_threads = thread::hardware_concurrency();
    cout << "Hardware concurrency on this machine is: " << max_threads << endl << endl;

    // Create array for threads
    vector<thread> init_calls;
    init_calls.reserve( max_threads );

    // Create arrays
    vector< vector<unsigned int> > arrays( max_threads );

    // Initialise arrays in parallel
    for( auto& array : arrays ) {
        thread t( init_vector, std::ref(array), std::ref(ELEMENTS) );
        init_calls.push_back( std::move(t) );
    }

    // Wait for all threads
    for( auto& t : init_calls ) {
        t.join();
    }

    // Checkpoint
    cout << "Finished initialising all vectors." << endl << endl;

    // Now do something with the data. Summing all the values will produce an overflow (you will
    // need an additional library for this). So we just look for the minimum and maximum.
    //
    // The min_element() and max_element() functions of <algorithm> use a single processor by default.
    // It is possible to parallelise these operations (shown in a different code).
    size_t n=1;
    for( const auto& array : arrays ) {
        const auto min = min_element( array.begin(), array.end() );
        const auto max = max_element( array.begin(), array.end() );
        // min and max are iterators, so access requires '*'.
        cout << "Vector #" << setw(2) << n << ": " << setw(10) << *min << " / " << setw(10) << *max << endl;
        n++;
    }

    return(0);
}
