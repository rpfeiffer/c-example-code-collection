// =====================================================================================
//
//       Filename:  operator_overloading_vector_catch2.cpp
//
//    Description:  Catch2 unit tests for vector class.
//
//        Version:  1.0
//        Created:  09. mai 2024 kl. 18.20 +0200
//       Revision:  none
//       Compiler:  g++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// Catch2 : https://github.com/catchorg/Catch2/tree/devel

#if __cplusplus < 202002L
#error This code requires C++20 or later.
#endif

#include <iostream>
#include <numbers>
#include "Catch2/catch_amalgamated.hpp"
#include "operator_overloading_vector.h"

//using namespace std;

// === Catch2 Unit Tests ===============================================================

auto get_epsilon() -> double {
    return( DBL_EPSILON );
}

// Comparison with square root of 2, used to test exception unit tests in Catch2.
auto check_sqrt2( double value ) -> bool {
    bool equal = false;
    if ( value == std::numbers::sqrt2 ) {
        equal = true;
    }
    else {
        throw("Comparison yielded false.");
    }
    return(equal);
}

TEST_CASE("Null vector","[basic]") {
    class myvector null;
    std::cout << "Vector null " << null << std::endl;
    REQUIRE( null.length() == 0.0 );
    REQUIRE( null.length() < DBL_EPSILON );
    REQUIRE( null.length() <= DBL_EPSILON );
    REQUIRE( null.length() <= ( 2.0 * DBL_EPSILON ) );
    REQUIRE( null.length() <= ( 3.0 * DBL_EPSILON ) );
    REQUIRE( null.length() <= ( 4.0 * DBL_EPSILON ) );
    REQUIRE( null.length() <= ( 5.0 * DBL_EPSILON ) );
}

TEST_CASE("Unit vector","[basic]") {
    class myvector one( 1.0, 1.0, 1.0 );
    std::cout << "Vector one " << one << std::endl;
    REQUIRE( one.length() != 1.0 );
    REQUIRE( one.length() >= 1.0 );
    REQUIRE( one.length() <= 2.0 );
    REQUIRE( one.length() == std::numbers::sqrt3 );
    CHECK_THROWS( check_sqrt2( one.length() ) );
    //REQUIRE( one.length() == 1.73205080756887729352 );
}

TEST_CASE("Vector scaling","[basic]") {
    class myvector one( 1.0, 1.0, 1.0 );
    class myvector one5 = one * 5.0;
    class myvector five( 5.0, 5.0, 5.0 );
    REQUIRE( one5.length() == five.length() );
}

TEST_CASE("Vector comparison","[basic]") {
    class myvector small( 2.5, 3.5, 4.5 );
    class myvector big( 11.3, 12.7, 14.4 );
    std::cout << "Vector small " << small << std::endl;
    std::cout << "Vector big   " << big << std::endl;
    REQUIRE( small < big );
    REQUIRE( big > small );
    REQUIRE( small != big );
}
