// =====================================================================================
//
//       Filename:  namespace_helper_functions.cpp
//
//    Description:  Add helper functions to its own namespace.
//
//        Version:  1.0
//        Created:  18. mars 2024 kl. 00.13 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <ctime>
#include <fstream>
#include <iostream>
#include <memory>
#include <random>
#include <string>

#include <sys/time.h>

// First comes the namespace declaration. This code uses the function definition syntax
// from Modern C++ (as per the C++ Core Guidelines). You can recognise this format by
// the auto keyword and the return value at the end of the function declaration.

namespace helper {
    // -----------------------------------------------------------------------
    // Namespace can have constants
    const unsigned short PRNG_INIT_URANDOM = sizeof(unsigned int);
    const unsigned int PRNG_INIT_DIVISOR   = 2048;
    const unsigned int PRNG_INIT_OFFSET    = 131072;

    // -----------------------------------------------------------------------
    // Creates random string
    // Function requires an initialised random generator object.
    auto create_random_string( unsigned short max_length, std::mt19937_64& PRNG ) -> std::string {
        unsigned short length = 1;
        const std::string letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        size_t n = 0;
        std::string random;
        std::uniform_int_distribution<unsigned int> r;

        length = ( r(PRNG) % max_length ) + 1;
        for ( n=1; n<=length; n++ ) {
            random.append( letters, r(PRNG) % letters.size(), 1 );
        }

        return(random);
    }

    // -----------------------------------------------------------------------
    // Initialises a random generator object
    //
    // This function uses the C++ random generator templates. In order to initialise
    // the PRNG it uses the current time, an optional seed number, and some bytes
    // from the /dev/urandom device. After initialisation it reads a random byte sequence
    // from the generator in order to make the first random output used after the function
    // more unpredictable.
    auto init_prng( std::mt19937_64& PRNG, unsigned int ext_seed ) -> unsigned long {
        struct timeval now = { 0, 0 };
        uint32_t seed = 0;
        unsigned int n = 0;
        unsigned long sum = 0;
        std::unique_ptr<char> rand_bytes ( new char [sizeof(unsigned int)] );
        unsigned int useed = 0;
        std::ifstream urandom;

        gettimeofday( &now, nullptr );
        seed = (uint32_t)now.tv_usec;
        if ( ext_seed != 0 ) {
            seed = seed ^ ext_seed;
        }
        else {
            // Without an initial seed we use a random device as a source for
            // the initial values. The C++ library will select suitable sources.
            std::random_device random;
            std::uniform_int_distribution<unsigned long> int_seed;

            seed = seed ^ (unsigned int)( int_seed(random) & 0xffffffffULL );
        }

        // Also use /dev/urandom for seeding
        urandom.open( "/dev/urandom", std::ifstream::binary );
        if ( urandom.is_open() ) {
            char *rand_bytes_ptr = rand_bytes.get();
            urandom.read( rand_bytes_ptr, PRNG_INIT_URANDOM );
            urandom.close();
            for( n=sizeof(unsigned int)-1; n != 0; n-- ) {
                useed = (useed << std::numeric_limits<unsigned char>::digits ) + rand_bytes_ptr[n];
            }
            seed = seed ^ useed;
        }

        // Seed internal PRNG state
        PRNG.seed(seed);

        // Create a random number of random numbers to move away from the initial values
        std::uniform_int_distribution<unsigned int> i;
        unsigned int how_many = ( i(PRNG) % PRNG_INIT_DIVISOR ) + PRNG_INIT_OFFSET;
        for( n=0, sum=0; n<how_many; n++ ) {
            sum += i(PRNG);
        }

        return(sum);
}
};

auto main( int argc, char **argv ) -> int {
    std::mt19937_64 myPRNG;

    unsigned long s = helper::init_prng( myPRNG, 0 );
    std::cout << "Sum after initialisation is " << s << "." << std::endl;
    std::cout << "Random string: " << helper::create_random_string( 32, myPRNG ) << std::endl << std::endl;

    return(0);
}
