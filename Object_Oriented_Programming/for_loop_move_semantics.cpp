// =====================================================================================
//
//       Filename:  for_loop_move_semantics.cpp
//
//    Description:  Code shows the use of auto type, for loops, and move semantics.
//
//        Version:  1.0
//        Created:  ti. 19. mars 20:31:24 +0100 2024
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <algorithm>
#include <array>            // available since C++11
#include <bits/stdc++.h>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <vector>

const unsigned int ELEMENTS = 75000000;

using std::array;
using std::cout;
using std::endl;
using std::vector;

// Timer code function
// (taken from https://gist.github.com/neilbalch/1deea32c5918549572b7dd10a9c04d1f and modified slightly)

template <typename Clock>
class ScopedTimer {
 public:
    ScopedTimer(const std::string name) : name(name), start(Clock::now()) {}
    ~ScopedTimer() {
        auto time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - start);
        cout << "TIMER: Operation <" << name << "> took " << (time.count() / 1000.0) << " ms.\n";
    }

 private:
    const std::string name;
    const std::chrono::time_point<Clock> start;
};

// main() function

int main( int argc, char **argv ) {
    // Allocate "big" test vector and initialise it
    auto test_vector = vector<int>(ELEMENTS);
    srand(time(0));
    generate( test_vector.begin(), test_vector.end(), rand );

    // for loop with copying and calculating sum (integer overflows are ignored)
    int64_t sum = 0;
    {
        cout << " " << endl;
        ScopedTimer<std::chrono::high_resolution_clock> timer("for() loop with copy");
        for( auto i : test_vector ) {
            sum = sum + i;
        }
        cout << "Sum: " << sum << endl;
    }

    // Same for loop with references
    sum = 0;
    {
        cout << " " << endl;
        ScopedTimer<std::chrono::high_resolution_clock> timer("for() loop without copy");
        for( const auto& i : test_vector ) {
            sum = sum + i;
        }
        cout << "Sum: " << sum << endl;
    }

    // Same with accumulator template from the algorithm library
    sum = 0;
    {
        cout << " " << endl;
        ScopedTimer<std::chrono::high_resolution_clock> timer("Algorithm accumulate function");
        sum = accumulate( test_vector.begin(), test_vector.end(), 0 );
        cout << "Sum: " << sum << endl;
    }

    return(0);
}
