// =====================================================================================
//
//       Filename:  algorith_min_max.cpp
//
//    Description:  Find minimum and maximum element of vector,
//
//        Version:  1.0
//        Created:  01. nov. 2024 kl. 23.23 +0100
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <algorithm>
#include <iostream>
#include <vector>

#include <sysexits.h>

using namespace std;

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK;

    vector<int> v1;
    vector<int> v2 = { 1, 1, 1, 1, 1, 1, 1, 1 };
    vector<int> v3 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    // Vector 1
    const auto [ min1, max1 ] = minmax_element( v1.begin(), v1.end() );
    if ( ( min1 != v1.begin() ) and ( max1 != v1.begin() ) ) {
        cout << "min1 = " << *min1 << ", max1 = " << *max1 << endl;
    }

    // Vector 2
    const auto [ min2, max2 ] = minmax_element( v2.begin(), v2.end() );
    cout << "min2 = " << *min2 << ", max2 = " << *max2 << endl;

    // Vector 3
    const auto [ min3, max3 ] = minmax_element( v3.begin(), v3.end() );
    cout << "min3 = " << *min3 << ", max3 = " << *max3 << endl;

    return(rc);
}
