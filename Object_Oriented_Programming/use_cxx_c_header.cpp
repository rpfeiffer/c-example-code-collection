// =====================================================================================
//
//       Filename:  use_cxx_c_header.cc
//
//    Description:  Code shows the use of C++ headers for C functions.
//
//        Version:  1.0
//        Created:  28. feb. 2024 kl. 12.57 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

// Some C headers have been ported to C++. Using the C++ variants means to use different
// header names. "math.h" turns into "cmath" for example. Have a look at the collection
// titled "C++ headers for C library facilities" in the C++ language reference:
// https://en.cppreference.com/w/cpp/standard_library
//
// Explicitly linking the math library is not necessary in C++.

#include <cmath>
#include <iomanip>    // std::setprecision
#include <iostream>
#include <numbers>    // C++20, provides mathematical constants

using namespace std;

const short MAX_PRECISION = 19;

int main( int argc, char **argv ) {
    double n = sin( std::numbers::sqrt3 );

    cout << "sqrt3       = " << std::setprecision(MAX_PRECISION) << std::numbers::sqrt3 << endl;
    cout << "sqrt3 (comp)= " << std::setprecision(MAX_PRECISION) << sqrt(3.0) << endl;
    cout << "sin( sqrt3) = " << std::setprecision(MAX_PRECISION) << n << endl;
    cout << "pi          = " << std::setprecision(MAX_PRECISION) << std::numbers::pi << endl;
    return(0);
}
