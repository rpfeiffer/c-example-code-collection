#ifndef H_TEMPERATURE_HEADER
#define H_TEMPERATURE_HEADER
// =====================================================================================
//
//       Filename:  loops.cc
//
//    Description:  Examples class for storing temperatures in three different units.
//
//        Version:  1.0
//        Created:  28. feb. 2024 kl. 12.57 +0100
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <cmath>     // If you use C headers, then use the C++ variant with a "c" prefix. See documentation.
#include <iostream>

class temperature {
    public:
        // Avoid using numbers in the source code ("magic numbers"). Always use meaningful
        // constants with names.
        const double ABSOLUTE_ZERO_C = -273.15;
        const double ABSOLUTE_ZERO_K = 0.0;
        const double ABSOLUTE_ZERO_R = -218.52;
        const double CONV_C2R        = 0.8;
        const double CONV_R2C        = 1.25;

        temperature() = default;
        temperature( double t );
        ~temperature() = default;
        double get_tc( void );
        double get_tk( void );
        double get_tr( void );
        void print( void );
        bool set_tc( double t_c );
        bool set_tk( double t_k );
        bool set_tr( double t_t );

    private:
        // Values are private to enforce valid range checks. Values default to absolute zero temperature.
        // The class is missing °Fahrenheit and °Rankine. °Réaumur is an obsolete unit (since 1901).
        double t_celsius = ABSOLUTE_ZERO_C;
        double t_kelvin  = ABSOLUTE_ZERO_K;
        double t_reaumur = ABSOLUTE_ZERO_R;

        // Private functions for conversion. Result is stored in the class attributes.
        bool conv_c2k( double t );
        bool conv_c2r( double t );
        bool conv_k2c( double t );
        bool conv_k2r( double t );
        bool conv_r2c( double t );
        bool conv_r2k( double t );
};

// Empty constructors / destructors do not need a function, the compiler will replace it with an
// empty function automatically. If you want this to happen, then you need to use the default keyword
// (see class definition above).

// Getter functions.

double temperature::get_tc( void ) {
    return( t_celsius );
}

double temperature::get_tk( void ) {
    return( t_kelvin );
}

double temperature::get_tr( void ) {
    return( t_reaumur );
}

// Print content of temeprature attributes

void temperature::print( void ) {
    std::cout << "t_c = " << this->get_tc() << std::endl
              << "t_k = " << this->get_tk() << std::endl
              << "t_r = " << this->get_tr() << std::endl;
    return;
}

// Setter functions

bool temperature::set_tc( double t ) {
    bool ok = conv_c2k(t);
    if ( ok ) {
        t_celsius = t;
        ok = conv_c2r(t);
    }

    return(ok);
}

bool temperature::set_tk( double t ) {
    bool ok = conv_k2c(t);
    if ( ok ) {
        t_kelvin = t;
        ok = conv_k2r(t);
    }

    return(ok);
}

bool temperature::set_tr( double t ) {
    bool ok = conv_r2c(t);
    if ( ok ) {
        t_reaumur = t;
        ok = conv_r2k(t);
    }

    return(ok);
}

// Converter functions

bool temperature::conv_c2k( double t ) {
    bool ok = false;

    if ( t >= ABSOLUTE_ZERO_C ) {
        t_kelvin = std::abs(t) + std::abs(ABSOLUTE_ZERO_C);
        ok = true;
    }

    return(ok);
}

bool temperature::conv_c2r( double t ) {
    bool ok = false;

    if ( t >= ABSOLUTE_ZERO_C ) {
        t_reaumur = t * CONV_C2R;
        ok = true;
    }

    return(ok);
}

bool temperature::conv_k2c( double t ) {
    bool ok = false;

    if ( t >= ABSOLUTE_ZERO_K ) {
        t_celsius = t - ABSOLUTE_ZERO_C;
        ok = true;
    }

    return(ok);
}

bool temperature::conv_k2r( double t ) {
    bool ok = false;

    if ( t >= ABSOLUTE_ZERO_K ) {
        t_reaumur = (t - ABSOLUTE_ZERO_C) * CONV_C2R;
        ok = true;
    }

    return(ok);
}

bool temperature::conv_r2c( double t ) {
    bool ok = false;

    if ( t >= ABSOLUTE_ZERO_R ) {
        t_celsius = t * CONV_R2C;
        ok = true;
    }

    return(ok);
}

bool temperature::conv_r2k( double t ) {
    bool ok = false;

    if ( t >= ABSOLUTE_ZERO_R ) {
        t_kelvin = ( t * 0.8 ) - ABSOLUTE_ZERO_C;
        ok = true;
    }

    return(ok);
}
#endif
