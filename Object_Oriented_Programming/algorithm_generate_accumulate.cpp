// =====================================================================================
//
//       Filename:  algorithm_generate_accumulate.cpp
//
//    Description:  Code shows how to use the algorithms and random library.
//
//        Version:  1.0
//        Created:  10. april 2024 kl. 10.07 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <algorithm>
#include <chrono>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <random>
#include <vector>

using namespace std;

const size_t NUMBER_ELEMENTS = 100000;

// Random object with initialisation function
class myrandom {
    public:
        myrandom();
        ~myrandom();
        double init_value();

    private:
        random_device rd{};
        mt19937_64 generator;
        const double r_min = numeric_limits<double>::min();
        const double r_max = numeric_limits<double>::max();
        uniform_real_distribution<double> *distr;
};

myrandom::myrandom() {
    size_t seed;

    if ( this->rd.entropy() ) {
        seed = this->rd();
    }
    else {
        seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    }
    this->generator.seed( seed );

    this->distr = new uniform_real_distribution<double>( this->r_min, this->r_max );
    cout << "Created random distribution [" << this->r_min << "|" << this->r_max << "]" << endl;

    return;
}

myrandom::~myrandom() {
    delete distr;
    return;
}

double myrandom::init_value() {
    double d = (*this->distr)(generator);
    return( d );
}

// main()
int main( int argc, char **argv ) {
    size_t n = NUMBER_ELEMENTS;
    vector<double> values;
    class myrandom PRNG;

    // If the argument of our program is a number, then we use this value for the
    // maximum number of elements.
    if ( argc > 1 ) {
        string argument( argv[1] );
        size_t n_arg = stoul( argument );
        if ( n_arg > 0 ) {
            n = n_arg;
        }
    }

    // Make room for elements
    values.reserve( n );

    // Fill vector with a loop
    for( size_t j=0; j<n; j++ ) {
        values.push_back( PRNG.init_value() );
    }

    // Fill vector (the generate algorithm does not work well with member functions; it is
    // easier to create a simple function for creating the values)
    //auto f = bind( &myrandom::init_value, PRNG );
    //generate( values.begin(), values.end(), f );

    // Calculate sum (here be overflows)
    double sum = accumulate( values.begin(), values.end(), 0 );
    cout << "We have " << values.size() << " values." << endl;
    cout << "First 10 values are:" << endl;
    for( size_t k=0; k<10; k++ ) {
        cout << values.at(k) << endl;
    }
    cout << "Sum = " << sum << endl;

    // Minimum/maximum
    const auto [min,max] = minmax_element( values.begin(), values.end() );
    cout << "Min = " << *min << endl;
    cout << "Max = " << *max << endl;

    return(0);
}
