// =====================================================================================
//
//       Filename:  test_factorial.cpp
//
//    Description:  Example code from the Catch2 tutorial.
//
//        Version:  1.0
//        Created:  09. mai 2024 kl. 16.07 +0200
//       Revision:  none
//       Compiler:  g++ / clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-License-Identifier: BSL-1.0
//
// Compile with:
// g++ -Wall -std=c++20 -o test_factorial test_factorial.cpp catch_amalgamated.cpp
// clang++ -Wall -std=c++20 -o test_factorial test_factorial.cpp catch_amalgamated.cpp

#include <cstdint>
#include "catch_amalgamated.hpp"

unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

uint64_t fibonacci(uint64_t number) {
    return number < 2 ? number : fibonacci(number - 1) + fibonacci(number - 2);
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}

TEST_CASE("Benchmark Fibonacci", "[!benchmark]") {
    REQUIRE(fibonacci(5) == 5);

    REQUIRE(fibonacci(20) == 6'765);
    BENCHMARK("fibonacci 20") {
        return fibonacci(20);
    };

    REQUIRE(fibonacci(25) == 75'025);
    BENCHMARK("fibonacci 25") {
        return fibonacci(25);
    };
}
