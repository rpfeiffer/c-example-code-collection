Unit Tests with Catch2 Framework
--------------------------------

This directory contains example unit tests implemented by the [Catch2 framework](https://github.com/catchorg/Catch2).
You will need the _catch2_ installed (either by the package manager or directly from source). The alternative method 
is to use these two files and put them into your project.

- catch\_amalgamated.cpp 
- catch\_amalgamated.hpp

You can find the files as downloads in the Catch2 release.
