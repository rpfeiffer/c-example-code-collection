#!/bin/bash

# Direct download version; it might be better to use git.

aria2c --allow-overwrite=true https://github.com/catchorg/Catch2/releases/download/v3.6.0/catch_amalgamated.cpp
aria2c --allow-overwrite=true https://github.com/catchorg/Catch2/releases/download/v3.6.0/catch_amalgamated.hpp
