// =====================================================================================
//
//       Filename:  futures.cpp
//
//    Description:  Parallel function calls by using futures.
//
//        Version:  1.0
//        Created:  04. april 2024 kl. 01.08 +0200
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
// 
// Compiler use:
// g++     -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc
// clang++ -Wall -Werror -Wpedantic -std=c++20 -o array_and_vector array_and_vector.cc

#include <future>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

unsigned int MAX_VALUE = 500000;

//----------------------------------------------------------------------
// Function definition for our sum function
//----------------------------------------------------------------------

unsigned long sum( const unsigned int n ) {
    unsigned long s = 0;

    for( unsigned int i=1; i<n; i++ ) {
        s = s + i;
    }
    return(s);
}

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    // Start the function in parallel.
    future<unsigned long> s1 = async( &sum, MAX_VALUE );
    future<unsigned long> s2 = async( &sum, MAX_VALUE );
    future<unsigned long> s3 = async( &sum, MAX_VALUE );
    future<unsigned long> s4 = async( &sum, MAX_VALUE );
    future<unsigned long> s5 = async( &sum, MAX_VALUE );

    // Wait for all functions.
    s1.wait();
    s2.wait();
    s3.wait();
    s4.wait();
    s5.wait();

    // Use the return values stored in the <future> data structure.
    cout << "s1 = " << s1.get() << endl;
    cout << "s2 = " << s2.get() << endl;
    cout << "s3 = " << s3.get() << endl;
    cout << "s4 = " << s4.get() << endl;
    cout << "s5 = " << s5.get() << endl;

    return(0);
}
