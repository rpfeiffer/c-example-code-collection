/*
 * =====================================================================================
 *
 *       Filename:  mathematical_constants.c
 *
 *    Description:  The math library predefines mathematical constants. You have to
 *                  undefine the __STRICT_ANSI__ symbol.
 *
 *        Version:  1.0
 *        Created:  06. okt. 2024 kl. 01.49 +0200
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

#undef __STRICT_ANSI__
#include <math.h>
#include <stdio.h>
#include <sysexits.h> /* Defines exit codes, inspect /usr/include/sysexits.h for details. */

int main( int argc, char **argv ) {
    int rc = EX_OK;
    double pi = 0.0;
    double sqrt2 = 0.0;

    printf( "Euler's number                = %.20f\n", M_E );
    printf( "Pi                            = %.20f\n", M_PI );
    pi = 4 * atan( 1.0 );
    printf( "Pi calculated                 = %.20f\n", pi );
    printf( "Square root of Pi             = %.20f\n", sqrt(M_PI) );
    printf( "Square root of 2              = %.20f\n", M_SQRT2 );
    sqrt2 = sqrt( 2.0 );
    printf( "Square root of 2 (calculated) = %.20f\n", sqrt2 );

    return(rc);
}
