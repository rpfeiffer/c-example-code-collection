#!/bin/bash
#
# Set CC to the C++ compiler you want to use (i.e. use "export CC=gcc" in the shell or
# the environment).

C_FILES=$(ls -1 *.c)

for CODE in $C_FILES; do
    $CC -Wall -Werror -Wpedantic -std=c23 -march=native -g -O0 -lm -o ${CODE::-2} $CODE
done
