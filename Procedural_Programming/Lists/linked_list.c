/*
 * =====================================================================================
 *
 *       Filename:  linked_list.c
 *
 *    Description:  Linked list (sequential, forward-linked).
 *
 *        Version:  1.0
 *        Created:  17. nov. 2024 kl. 01.18 +0100
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web-luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

#include <stdio.h>
#include <stdlib.h>

/* This is our data structure. */

struct account {
    int id;
    char *login;
};

int main( int argc, char **argv ) {
}
