/*
 * =====================================================================================
 *
 *       Filename:  fibonacci.c
 *
 *    Description:  Computes the Fibonacci numbers iteratively and rescursively.
 *
 *        Version:  1.0
 *        Created:  sø. 06. okt. 01:29:35 +0200 2024
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

/* This code has unchecked integer overflows for Fibonacci numbers bigger than 47. In
 * this case the recursion will not stop or stop later.
 */

#include <stdio.h>
#include <sysexits.h> /* Defines exit codes, inspect /usr/include/sysexits.h for details. */

unsigned int fibonacci_loop( unsigned int n ) {
    unsigned int f = 0;
    unsigned int f0 = 0;
    unsigned int f1 = 1;
    unsigned int f_n1 = 1;
    unsigned int f_n2 = 0;

    for( unsigned int i = 0; i<=n; i++ ) {
        if ( i == 0 ) {
            f = f0;
            continue;
        }
        if ( i == 1 ) {
            f = f1;
            continue;
        }
        /* n ist hier größer 2 */
        f = f_n1 + f_n2;
        /* Vorbereitung für nächste Iteration */
        f_n2 = f_n1;
        f_n1 = f;
    }

    return(f);
}

unsigned int fibonacci( unsigned int n ) {
    unsigned int f = 0;

    if ( n == 0 ) {
        f = 0;
    }
    if ( n == 1 ) {
        f = 1;
    }
    if ( n > 1 ) {
        f = fibonacci(n-1) + fibonacci(n-2);
    }
    /* Uncomment this line, if you want to see how the value of f changes during the recursion. */
    /* printf("%u\n",f); */
    return(f);
}

int main( int argc, char **argv ) {
    unsigned int eingabe = 0;
    unsigned int ergebnis = 0;
    unsigned int ergebnis_iter = 0;

    scanf( " %u", &eingabe );
    ergebnis = fibonacci(eingabe);
    printf( "%u. Fibonnaci-Zahl = %u\n", eingabe, ergebnis );
    ergebnis_iter = fibonacci_loop(eingabe);
    printf( "%u. Fibonacci_iter = %u\n", eingabe, ergebnis_iter );

    return(EX_OK);
}
