/*
 * =====================================================================================
 *
 *       Filename:  main_exit_code.c
 *
 *    Description:  How to use proper exit code for the main() function.
 *
 *        Version:  1.0
 *        Created:  03. april 2024 kl. 23.57 +0200
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

#include <stdio.h>
#include <sysexits.h> /* Defines exit codes, inspect /usr/include/sysexits.h for details. */

int main( int argc, char **argv ) {
    /* Always initialise variables with meaningful values. */
    int a = 0;
    int b = 0;

    printf("First number : ");
    scanf("%d", &a);
    printf("Second number: ");
    scanf("%d", &b);
    if ( b == 0 ) {
        printf("ERROR: Division by 0 not possible.\n");
        return(EX_DATAERR);
    }

    printf( "%d / %d = %d\n", a, b, a/b );
    printf( "%d %% %d = %d\n", a, b, a%b );

    return(EX_OK);
}
