/*
 * =====================================================================================
 *
 *       Filename:  coding_style.c
 *
 *    Description:  This source code explain the coding style. Use a header to explain
 *                  the metadata of the programme.
 *
 *        Version:  1.0
 *        Created:  04. april 2024 kl. 00.37 +0200
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

#include <stdio.h>

/* -------------------------------------------------------------------------------------
   Constants - Use section headers to mark different parts of the code. 
   ------------------------------------------------------------------------------------- */

const unsigned int MAX_NUMBER = 1000;

/* -------------------------------------------------------------------------------------
   Global variables - try to use as less as possible (ideally none)
   ------------------------------------------------------------------------------------- */

unsigned int global_counter = 0;

/* -------------------------------------------------------------------------------------
   main() section 
   ------------------------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    unsigned int sum = 0;

    /* Don't explain trivial things in the comment. Either the code speaks, or the comment
       explains. The following loop computes the summ of the numbers in the loop. */
    for( unsigned int j=1;
         j<=MAX_NUMBER;
         j++ ) {
        sum = sum + j;
    }

    printf( "Sum = %d\n", sum );

    /* Always use a full {} to mark the conditional command(s) more clearly. */
    if ( MAX_NUMBER < 100 ) {
        printf("ERROR: Maximum limit is too low.\n");
    }

    return(0);
}
