/* Modified code to enlarge array and take the time. -- pfeiffer@luchs.at
   Try with the following compiler options:

   -O0
   -O2
   -O3
   -O3 -march=native
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARRAY_SIZE 100000

void printArray(int a[], int n)
{
    for (int i = 0; i < n; ++i)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void fillArray(int a[], int n)
{
    for (int i = 0; i < n; ++i)
    {
        a[i] = rand() % 100;
    }
}

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void bubbleSort(int a[], int n)
{
    for (int i = 0; i < n - 1; ++i)
    {
        for (int j = 0; j < n - 1; ++j)
        {
            if (a[j] > a[j + 1])
            {
                swap(&a[j], &a[j + 1]);
            }
        }
    }
}

void insertionSort(int a[], int n)
{
    for (int i = 1; i < n; ++i)
    {
        int t = a[i]; // Aktuelles Element
        int j;

        // Suche und verschiebe
        for (j = i; j > 0 && a[j - 1] > t; --j)
        {
            a[j] = a[j - 1];
        }

        a[j] = t; // Einfuegen
    }
}

void selectionSort(int a[], int n)
{
    for (int i = 0; i < n - 1; ++i)
    {
        int min = i;
        for (int j = i + 1; j < n; ++j)
        {
            if (a[j] < a[min])
            {
                min = j; // Position Minimum
            }
        }

        if (min != i)
        {
            swap(&a[min], &a[i]);
        }
    }
}

int comparator( const void *p, const void *q ) {
    return ( *(int*)p-*(int*)q );
}

int main()
{
    int array[ARRAY_SIZE];
    clock_t start, difference;
    unsigned int msec;

    srand(time(NULL));

    start = clock();
    fillArray(array, ARRAY_SIZE);
    /* printArray(array, ARRAY_SIZE); */
    bubbleSort(array, ARRAY_SIZE);
    /* printArray(array, ARRAY_SIZE); */
    difference = clock() - start;
    msec = difference * 1000 / CLOCKS_PER_SEC;
    printf( "Bubble sort took %f seconds.\n", (double)msec / 1000.0 );

    start = clock();
    fillArray(array, ARRAY_SIZE);
    /* printArray(array, ARRAY_SIZE); */
    insertionSort(array, ARRAY_SIZE);
    /* printArray(array, ARRAY_SIZE); */
    difference = clock() - start;
    msec = difference * 1000 / CLOCKS_PER_SEC;
    printf( "Insertion sort took %f seconds.\n", (double)msec / 1000.0 );

    start = clock();
    fillArray(array, ARRAY_SIZE);
    /* printArray(array, ARRAY_SIZE); */
    selectionSort(array, ARRAY_SIZE);
    /* printArray(array, ARRAY_SIZE); */
    difference = clock() - start;
    msec = difference * 1000 / CLOCKS_PER_SEC;
    printf( "Selection sort took %f seconds.\n", (double)msec / 1000.0 );

    start = clock();
    fillArray(array, ARRAY_SIZE);
    qsort( (void *)array, ARRAY_SIZE, sizeof(array[0]), comparator );
    difference = clock() - start;
    msec = difference * 1000 / CLOCKS_PER_SEC;
    printf( "qsort took %f seconds.\n", (double)msec / 1000.0 );

    return 0;
}
