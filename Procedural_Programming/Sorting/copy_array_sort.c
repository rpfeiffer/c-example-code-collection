/*
 * =====================================================================================
 *
 *       Filename:  copy_array_sort.c
 *
 *    Description:  Sorts an array by copying elements sorted into a new array.
 *
 *        Version:  1.0
 *        Created:  03. des. 2024 kl. 22.33 +0100
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------
  Rationale
  ----------------------------------------------------------------------

  This code demonstrates how to sort an array by copying the elements
  into a new array. The algorithm uses a frequency table to detect and
  handle duplicates.
 */

/*----------------------------------------------------------------------
  Constants
  ---------------------------------------------------------------------- */

#define N_ELEMENTS 10

const int array_singles[N_ELEMENTS] = { 161, 910, 262, 313, 9, 514, 824, 723, 980 ,114 };
const int array_duplicate[N_ELEMENTS] = { 685, 307, 939, 497, 280, 339, 919, 348, 280, 842 };
const int array_duplicates[N_ELEMENTS] = { 51, 621, 143, 854, 763, 621, 800, 912, 621, 579 };
const int array_duplicates2[N_ELEMENTS] = { 621, 621, 621, 621, 763, 621, 621, 912, 21, 621 };

const int MODULO_DIVISOR = 1000;
const int RANGE = 1000;

/*----------------------------------------------------------------------
  Functions
  ---------------------------------------------------------------------- */

void clearArray( int a[], unsigned int n ) {
    memset( a, 0, n * sizeof(int) );
}

void fillArray( int a[], unsigned int n ) {
    for (int i = 0; i < n; ++i) {
        a[i] = rand() % MODULO_DIVISOR;
    }
}

int findMinimum( const int a[], unsigned int n ) {
    int minimum = a[0];

    for( unsigned int i=1; i<n; i++ ) {
        if ( a[i] < minimum ) {
            minimum = a[i];
        }
    }

    return( minimum );
}

void printArray( const int a[], unsigned int n ) {
    printf("( ");
    for( unsigned int i=0; i<n; i++ ) {
        printf( "%d ", a[i] );
    }
    printf(")\n");
}

void copySort( const int source[], int target[], unsigned int n ) {
    int *frequency;

    /* Allocate frequency array */
    frequency = (int *)calloc( RANGE, sizeof(int) );
    if ( frequency == NULL ) return;

    /* Create frequency table */
    for( unsigned int i=0; i<n; i++ ) {
        frequency[ source[i] ]++;
    }

    /* Create target array in sorted order */
    unsigned int target_pos = 0;
    for( unsigned int i=0; i<RANGE; i++ ) {
        while ( frequency[i]-- ) {
            target[target_pos] = i;
            target_pos++;
        }
    }

    free(frequency);
}

/*----------------------------------------------------------------------
    M A I N
  ---------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    int result1[N_ELEMENTS];
    int result2[N_ELEMENTS];
    int result3[N_ELEMENTS];

    clearArray( result1, N_ELEMENTS );
    clearArray( result2, N_ELEMENTS );
    clearArray( result3, N_ELEMENTS );

    printArray( array_singles, N_ELEMENTS );
    printf(" --> \n");
    copySort( array_singles, result1, N_ELEMENTS );
    printArray( result1, N_ELEMENTS );
    printf("\n");

    printArray( array_duplicate, N_ELEMENTS );
    printf(" --> \n");
    copySort( array_duplicate, result2, N_ELEMENTS );
    printArray( result2, N_ELEMENTS );
    printf("\n");

    printArray( array_duplicates, N_ELEMENTS );
    printf(" --> \n");
    copySort( array_duplicates, result3, N_ELEMENTS );
    printArray( result3, N_ELEMENTS );
    printf("\n");

    printArray( array_duplicates2, N_ELEMENTS );
    printf(" --> \n");
    copySort( array_duplicates2, result3, N_ELEMENTS );
    printArray( result3, N_ELEMENTS );
    printf("\n");

    return(0);
}
