/*
 * =====================================================================================
 *
 *       Filename:  loops.c
 *
 *    Description:  All the loops in C.
 *
 *        Version:  1.0
 *        Created:  04. april 2024 kl. 00.12 +0200
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

#include <stdio.h>

/* Constant. Sets the upper limit of the loops. The recommendation is to write constants
   all upper case and use the const modifier. */

const unsigned int MAX_NUMBER = 10;

int main( int argc, char **argv ) {
    unsigned int i = 0; /* counter variable */

    /* All three loops do the same task. The recommendation is to use the while() or
       for() loop, because one sees the condition immediately. */
    do {
        printf( "Counter = %d\n", i );
        i++;
    } while ( i < MAX_NUMBER );

    i = 0;
    while( i < MAX_NUMBER ) {
        printf( "Counter = %d\n", i );
        i++;
    }

    for( i=0; i<MAX_NUMBER; i++ ) {
        printf( "Counter = %d\n", i );
    }

    return(0);
}
