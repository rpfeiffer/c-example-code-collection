/*
 * =====================================================================================
 *
 *       Filename:  read_csv.c
 *
 *    Description:  Read and display CSV file.
 *
 *        Version:  1.0
 *        Created:  01. des. 2024 kl. 20.44 +0100
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

/* This symbol needs to be defined for strtok_r() to be available. */
#define __USE_POSIX 1
#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------
  Rationale
  ----------------------------------------------------------------------

  This code demonstrates how to read a CSV file.
 */

/*----------------------------------------------------------------------
  Dangers
  ----------------------------------------------------------------------

  strtok() keeps and internal state in a static variable. This makes it
  unsuitable for parallel code. Use strtok_r() instead and save the
  position in the string locally.
 */

/*----------------------------------------------------------------------
  Constants
  ---------------------------------------------------------------------- */

const char DELIMITER[] = ",";
const char EMPTY[] = "(-)";
const size_t LINEBUFFER_SIZE = 2048;

/*----------------------------------------------------------------------
  Data structures
  ---------------------------------------------------------------------- */

struct crew {
    int id;
    char *rank;
    char *firstname;
    char *lastname;
};

/*----------------------------------------------------------------------
  Helper functions
  ---------------------------------------------------------------------- */

bool copy_rank( struct crew *person, const char *new_rank ) {
    bool success = false;
    char *rank_temp = NULL;
    size_t length = 0;

    length = strlen(new_rank)+1;
    if ( person->rank == NULL ) {
        person->rank = (char *)calloc( length, sizeof(char) );
        if ( person->rank != NULL ) {
            strncpy( person->rank, new_rank, length );
            success = true;
        }
    }
    else {
        if ( strlen(person->rank) < length ) {
            rank_temp = (char *)realloc( (void *)person->rank, length );
            if ( rank_temp != NULL ) {
                person->rank = rank_temp;
            }
        }
        strncpy( person->rank, new_rank, length );
        success = true;
    }

    return(success);
}

bool copy_firstname( struct crew *person, const char *new_firstname ) {
    bool success = false;
    char *firstname_temp = NULL;
    size_t length = 0;

    length = strlen(new_firstname)+1;
    if ( person->firstname == NULL ) {
        person->firstname = (char *)calloc( length, sizeof(char) );
        if ( person->firstname != NULL ) {
            strncpy( person->firstname, new_firstname, length );
            success = true;
        }
    }
    else {
        if ( strlen(person->firstname) < length ) {
            firstname_temp = (char *)realloc( (void *)person->firstname, length );
            if ( firstname_temp != NULL ) {
                person->firstname = firstname_temp;
            }
        }
        strncpy( person->firstname, new_firstname, length );
        success = true;
    }

    return(success);
}

bool copy_lastname( struct crew *person, const char *new_lastname ) {
    bool success = false;
    char *lastname_temp = NULL;
    size_t length = 0;

    length = strlen(new_lastname)+1;
    if ( person->lastname == NULL ) {
        person->lastname = (char *)calloc( length, sizeof(char) );
        if ( person->lastname != NULL ) {
            strncpy( person->lastname, new_lastname, length );
            success = true;
        }
    }
    else {
        if ( strlen(person->lastname) < length ) {
            lastname_temp = (char *)realloc( (void *)person->lastname, length );
            if ( lastname_temp != NULL ) {
                person->lastname = lastname_temp;
            }
        }
        strncpy( person->lastname, new_lastname, length );
        success = true;
    }

    return(success);
}

/*----------------------------------------------------------------------
    M A I N
  ---------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    int rc = 0;
    char *line_buffer = NULL;
    char *tok_pos = NULL;
    struct crew person = { -1, NULL, NULL, NULL };
    FILE *datei = NULL;

    /* Clear data structure */

    line_buffer = (char *)calloc( LINEBUFFER_SIZE, sizeof(char) );
    if ( line_buffer == NULL ) {
        rc = 1;
    }

    if ( ( argc < 2 ) && ( rc == 0 ) ) {
        puts("Please indicate a valid filename as first argument.");
        rc = 1;
    }
    else {
        datei = fopen( argv[1], "r" );
        if ( datei != NULL ) {
            char *result;
            char *item;
            do {
                result = fgets( line_buffer, LINEBUFFER_SIZE-1, datei );
                tok_pos = NULL;
                if ( result != NULL ) {
                    item = strtok_r( line_buffer, DELIMITER, &tok_pos );
                    if ( item != NULL ) {
                        person.id = atoi( item );
                    }
                    item = strtok_r( NULL, DELIMITER, &tok_pos );
                    if ( item != NULL ) {
                        if ( ! copy_rank( &person, item ) ) {
                            person.rank = NULL;
                        }
                    }
                    item = strtok_r( NULL, DELIMITER, &tok_pos );
                    if ( item != NULL ) {
                        if ( ! copy_firstname( &person, item ) ) {
                            person.firstname = NULL;
                        }
                    }
                    item = strtok_r( NULL, DELIMITER, &tok_pos );
                    if ( item != NULL ) {
                        if ( ! copy_lastname( &person, item ) ) {
                            person.lastname = NULL;
                        }
                    }
                }
                printf( "%d. %s %s %s\n", person.id, person.rank, person.firstname, person.lastname );
            } while ( result != NULL );
            fclose(datei);
        }
        else {
            rc = 1;
        }
    }

    if ( person.rank != NULL ) free( person.rank );
    if ( person.firstname != NULL ) free( person.firstname );
    if ( person.lastname != NULL ) free( person.lastname );
    if ( line_buffer != NULL ) free( line_buffer );

    return(rc);
}
