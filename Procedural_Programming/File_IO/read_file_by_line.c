/*
 * =====================================================================================
 *
 *       Filename:  read_file_by_byte.c
 *
 *    Description:  Read file into string buffer. Filename is taken from first argument.
 *
 *        Version:  1.0
 *        Created:  29. nov. 2024 kl. 22.33 +0100
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

/* This symbol needs to be defined for getline() to be available. */
#define __USE_POSIX 1
#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

/*----------------------------------------------------------------------
  Rationale
  ----------------------------------------------------------------------

  This code demonstrates how to read a file of arbitrary size. The file
  is read line by line to reduce read operations.
 */

/*----------------------------------------------------------------------
  Dangers
  ----------------------------------------------------------------------

  getline() uses malloc() to increase the buffer size. Reading an unknown
  file with arbitrary length and no newlines can exhaust all memory. Try
  to check the file size first.
 */

/*----------------------------------------------------------------------
  Constants
  ---------------------------------------------------------------------- */

const size_t BUFFER_SIZE = 256*1024;
const size_t BUFFER_EXTEND_SIZE = 64*1024;
const size_t LINEBUFFER_SIZE = 2048;

/*----------------------------------------------------------------------
    M A I N
  ---------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    int rc = 0;
    char *datei_content = NULL;
    size_t datei_content_size = BUFFER_SIZE;
    char *line_buffer = NULL;
    size_t line_buffer_size = LINEBUFFER_SIZE;
    FILE *datei = NULL;

    datei_content = (char *)calloc( BUFFER_SIZE, sizeof(char) );
    if ( datei_content == NULL ) {
        rc = 1;
    }

    line_buffer = (char *)calloc( LINEBUFFER_SIZE, sizeof(char) );
    if ( line_buffer == NULL ) {
        rc = 1;
    }

    if ( ( argc < 2 ) && ( rc == 0 ) ) {
        puts("Please indicate a valid filename as first argument.");
        rc = 1;
    }
    else {
        datei = fopen( argv[1], "r" );
        if ( datei != NULL ) {
            size_t byte_count = 0;
            ssize_t n = 0;
            char *pos = datei_content;
            printf( "Buffer size = %lu\n", datei_content_size );
            printf( "Buffer      = %p\n", (void *)datei_content );

            while ( ! feof(datei) ) {
                n = getline( &line_buffer, &line_buffer_size, datei );
                if ( n > 0 ) {
                    byte_count = byte_count + n;
                }
                if ( byte_count > datei_content_size ) {
                    datei_content_size = datei_content_size + BUFFER_EXTEND_SIZE;
                    datei_content = (char *)realloc( datei_content, datei_content_size );
                    if ( datei_content == NULL ) {
                        rc = 1;
                        break;
                    }
                }
                strcat( pos, line_buffer );
                pos = pos + n;
            }
            if ( byte_count >= datei_content_size ) {
                // Ensure that nullbyte fits into destination buffer
                datei_content = (char *)realloc( datei_content, datei_content_size+1 );
            }
            pos++;
            *pos = 0;
            printf( "--- CONTENT ---\n%s---------------\n", datei_content );
            printf( "Buffer size = %lu\n", datei_content_size );
            printf( "Buffer      = %p\n", (void *)datei_content );
            fclose(datei);
        }
    }

    if ( datei_content != NULL ) free( datei_content );
    if ( line_buffer != NULL ) free( line_buffer );

    return(rc);
}
