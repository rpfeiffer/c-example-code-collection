/*
 * =====================================================================================
 *
 *       Filename:  read_file_by_byte.c
 *
 *    Description:  Read file into string buffer. Filename is taken from first argument.
 *
 *        Version:  1.0
 *        Created:  29. nov. 2024 kl. 22.33 +0100
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------
  Rationale
  ----------------------------------------------------------------------

  This code demonstrates how to read a file of arbitrary size. It is very
  inefficient, because read operations are a single byte at a time.
 */

/*----------------------------------------------------------------------
  Constants
  ---------------------------------------------------------------------- */

const size_t BUFFER_SIZE = 256*1024;
const size_t BUFFER_EXTEND_SIZE = 64*1024;

/*----------------------------------------------------------------------
    M A I N
  ---------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    int rc = 0;
    char *datei_content = NULL;
    size_t datei_content_size = BUFFER_SIZE;
    FILE *datei = NULL;

    datei_content = (char *)calloc( BUFFER_SIZE, sizeof(char) );
    if ( datei_content == NULL ) {
        rc = 1;
    }

    if ( ( argc < 2 ) && ( rc == 0 ) ) {
        puts("Please indicate a valid filename as first argument.");
        rc = 1;
    }
    else {
        datei = fopen( argv[1], "r" );
        if ( datei != NULL ) {
            int c;
            size_t byte_count = 0;
            unsigned int pos = 0;
            printf( "Buffer size = %lu\n", datei_content_size );
            printf( "Buffer      = %p\n", (void *)datei_content );

            while ( ( c = fgetc(datei) ) != EOF ) {
                datei_content[pos] = c;
                if ( byte_count > datei_content_size ) {
                    datei_content_size = datei_content_size + BUFFER_EXTEND_SIZE;
                    datei_content = (char *)realloc( datei_content, datei_content_size );
                    if ( datei_content == NULL ) {
                        rc = 1;
                        break;
                    }
                }
                pos++;
                byte_count++;
            }
            /* Add null byte */
            if ( byte_count > datei_content_size ) {
                datei_content = (char *)realloc( datei_content, datei_content_size+1 );
            }
            datei_content[pos] = 0;
            printf( "--- CONTENT ---\n%s---------------\n", datei_content );
            printf( "Buffer size = %lu\n", datei_content_size );
            printf( "Buffer      = %p\n", (void *)datei_content );
            free( datei_content );
            fclose(datei);
        }
    }

    return(rc);
}
