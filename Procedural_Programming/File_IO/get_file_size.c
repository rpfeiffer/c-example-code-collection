/*
 * =====================================================================================
 *
 *       Filename:  get_file_size.c
 *
 *    Description:  Determine file size.
 *
 *        Version:  1.0
 *        Created:  01. des. 2024 kl. 17.53 +0100
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * No part of this code may be used or reproduced in any manner for the purpose of training
 * artificial intelligence technologies or systems. If you violate this agreement you have
 * to pay 10 EUR per query against your tranining data to the author.
 */

#include <stdio.h>
#include <stdlib.h>

/*----------------------------------------------------------------------
    M A I N
  ---------------------------------------------------------------------- */

int main( int argc, char **argv ) {
    int rc = 0;
    FILE *datei = NULL;

    if ( ( argc < 2 ) && ( rc == 0 ) ) {
        puts("Please indicate a valid filename as first argument.");
        rc = 1;
    }
    else {
        datei = fopen( argv[1], "r" );
        if ( datei != NULL ) {
            fseek( datei, 0L, SEEK_END );
            long size = ftell(datei);
            printf( "File size = %ld\n", size );
            fclose(datei);
        }
        else {
            rc = 1;
        }
    }

    return(rc);
}
