/*
 * =====================================================================================
 *
 *       Filename:  basic_data_types.c
 *
 *    Description:  Overview of basic data types. 
 *
 *        Version:  1.0
 *        Created:  03. april 2024 kl. 23.21 +0200
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

#include <limits.h> /* contains all the minimum and maximum values of the data types */
#include <float.h>  /* contains all the minimum and maximum values of the floating point data types */
#include <stdio.h>

/* You can look up all the basic C data types here: https://en.wikipedia.org/wiki/C_data_types 

   The sizeof() operator determines the number of bytes used for the data type on the target system.
   The actual size is determined by the processor and memory hardware.
 */

int main( int argc, char **argv ) {
    /* Look up the format specifiers and add the unsigned integer data types. Also try to run this code
       on a different processor architecture (32-bit, Raspberry Pi) and compare the results. */
    printf( "char (%ld bytes), minimum is %d, maximum is %d\n", sizeof(char), CHAR_MIN, CHAR_MAX );
    printf( "unsigned char (%ld bytes), minimum is %d, maximum is %u\n", sizeof(unsigned char), 0, UCHAR_MAX );
    printf( "short (%ld bytes), minimum is %d, maximum is %d\n", sizeof(short), SHRT_MIN, SHRT_MAX );
    printf( "unsigned short (%ld bytes), minimum is %d, maximum is %u\n", sizeof(unsigned short), 0, USHRT_MAX );
    printf( "int (%ld bytes), minimum is %d, maximum is %d\n", sizeof(int), INT_MIN, INT_MAX );
    printf( "unsigned int (%ld bytes), minimum is %d, maximum is %u\n", sizeof(unsigned int), 0, UINT_MAX );
    printf( "long (%ld bytes), minimum is %ld, maximum is %ld\n", sizeof(long), LONG_MIN, LONG_MAX );
    printf( "unsigned long (%ld bytes), minimum is %ld, maximum is %lu\n", sizeof(unsigned long), 0l, ULONG_MAX );
    printf( "long long (%ld bytes), minimum is %lld, maximum is %lld\n", sizeof(long long), LLONG_MIN, LLONG_MAX );
    printf( "unsigned long long (%ld bytes), minimum is %lld, maximum is %llu\n", sizeof(unsigned long long), 0ll, ULLONG_MAX );
    printf( "float (%ld bytes), minimum is %e, maximum is %e\n", sizeof(float), FLT_MIN, FLT_MAX );
    printf( "double (%ld bytes), minimum is %e, maximum is %e\n", sizeof(double), DBL_MIN, DBL_MAX );
    printf( "long double (%ld bytes), minimum is %Le, maximum is %Le\n", sizeof(long double), LDBL_MIN, LDBL_MAX );
    return(0);
}
