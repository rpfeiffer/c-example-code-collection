/*
 * =====================================================================================
 *
 *       Filename:  hello_world.c
 *
 *    Description:  Classic "Hello, world!" code.
 *
 *        Version:  1.0
 *        Created:  03. april 2024 kl. 23.16 +0200
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  René Pfeiffer (), pfeiffer@luchs.at
 *        Company:  https://web.luchs.at/
 *
 * =====================================================================================
 */

/* SPDX-License-Identifier: GPL-2.0-or-later */

/* Include section - here you tell the compiler what parts of the C libraries you want to use. */

#include <stdio.h>

/* The main() function is called whenever the program is executed. main() has function parameters,
   but we will ignore these. */

int main( int argc, char **argv ) {
    /* Output "Hello, world!" with a line break. */
    printf("Hello World\n");

    /* main() has a return value. 0 means no errors. */
    return(0);
}
