# C Code Examples

The C code examples are targetted for beginners in C programming and related procedural programming languages.

## C Standard

The recommendation is to use C code conforming to the C11 or C17 standard. C17 support is available in most C compilers.
Support for C23 ist also part of some compilers. Enabling the newer standards allows the compiler to generate warnings and allow modern C code constructs.

- [Clang C standard support](https://clang.llvm.org/c_status.html)
- [GCC C standard support](https://gcc.gnu.org/onlinedocs/gcc/language-standards-supported-by-gcc/c-language.html)

## Compiler Flags, Security, and Optimisation

Do not use optimisation early when creating code. Don't use it until you have a solid skeleton and a working prototype. A sensible flags combination
to get started is this one:
```
-Wall -Werror -Wpedantic -I. -O0 -fno-omit-frame-pointer -std=c17
```
If you want, then you can create code with all the features of your local processor by using the _-march=native_ flag. Once everything works you
can change the flags. A suggestions for a complete set of compiler flags for the Clang++ compiler is this one:
```
cflags = -Wall -Werror -I. -O3 -march=native -fno-omit-frame-pointer -std=c17
secflags = -fPIE -fstack-protector -fstack-clash-protection -mshstk -fstrict-flex-arrays=3 -fcf-protection=full -Wformat -Wformat-security -Werror=format-security -Wnull-pointer-arithmetic -D_FORTIFY_SOURCE=3
```
Sources explaining some of the security compiler flags are:

- [A developer’s guide to secure coding with FORTIFY\_SOURCE](https://developers.redhat.com/articles/2023/07/04/developers-guide-secure-coding-fortifysource)
- [GCC compiler flags](https://gcc.gnu.org/onlinedocs/gcc-9.1.0/gcc/x86-Options.html)

GCC and Clang use slightly different flags. Be careful with the optimisation flags! Unless you need the performace,
do _not_ use any of them. Optimisation can change the order of code line executions, comparisons, and read/write to
memory locations.
