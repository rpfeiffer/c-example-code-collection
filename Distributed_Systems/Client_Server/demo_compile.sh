#!/bin/bash
# 
# This is not a Makefile, but shell scripts can be used to compile small
# projects.
#
# CXX should contain the name of the local C++ compiler. You can set CXX
# by using the shell environment (issueing "export CXX=g++" manually works, too).
# This code and script works with both gcc and clang.
#
# The compile flags have the following functions:
#
# -Wall -Werror 
# -g adds debug information
# -O0 disables all optimisations (usefull and recommended for debugging)
# -march=native compiles for all local CPU features (optional)
# -std=c++20 enables C++20 language features and checks (safe to use with gcc and clang)

CXXFLAGS="-Wall -Werror -g -O0 -march=native -std=c++20"

$CXX $CXXFLAGS -o bin/demo_client demo_client.cpp
$CXX $CXXFLAGS -o bin/demo_server demo_server.cpp
