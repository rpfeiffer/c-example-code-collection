// =====================================================================================
//
//       Filename:  demo_server.cpp
//
//    Description:  Demonstration server code with basic sockets. C++ version of the C
//                  code in this directory.
//
//        Version:  1.0
//        Created:  25. sep. 2024 kl. 00.07 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <iostream>
#include <string>

// C
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

const unsigned int BUFFER_SIZE = 65536;
const in_port_t SERVER_PORT = 5427;

const std::string server_welcome = "Welcome to myserver!\r\nPlease enter your commands...\r\n";
const std::string server_ok = "OK\r\n";

//----------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------

bool abort_requested = false;
int server_socket = -1;
int new_connection_socket = -1;

//----------------------------------------------------------------------
// Function prototypes
//----------------------------------------------------------------------

extern "C" void signalHandler(int signal);
extern "C" int clientCommunication( void *socket );

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK; // EX_OK is defined in sysexits.h
    socklen_t addrlen;
    struct sockaddr_in address, client_address;
    int reuseValue = 1;
    int status = -1;

    // -----------------------------------------------------------------
    // Set signal handler
    if ( signal( SIGINT, signalHandler ) == SIG_ERR ) {
        std::cerr << "Error setting signal handler: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_UNAVAILABLE );
    }

    // -----------------------------------------------------------------
    // Create socket descriptor (i.e. request descriptor from operating
    // system)
    server_socket = socket( AF_INET, SOCK_STREAM, 0 );
    if ( server_socket == -1 ) {
        std::cerr << "Error getting socket descriptor: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_CANTCREAT );
    }

    // -----------------------------------------------------------------
    // Set socket options
    status = setsockopt( server_socket, SOL_SOCKET, SO_REUSEADDR, &reuseValue, sizeof(reuseValue) );
    if ( status == -1 ) {
        std::cerr << "Error setting socket options: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_OSERR );
    }

    // -----------------------------------------------------------------
    // Initialise address structure with listening address and port
    memset( &address, 0, sizeof(address) );
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( SERVER_PORT );

    // -----------------------------------------------------------------
    // Bind socket to address
    status = bind( server_socket, (struct sockaddr *)&address, sizeof(address) );
    if ( status == -1 ) {
        std::cerr << "Error binding socket: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_UNAVAILABLE );
    }

    // -----------------------------------------------------------------
    // Allow connections (and up to 5 waiting connections)
    status = listen( server_socket, 5 );
    if ( status == -1 ) {
        std::cerr << "Error enabling listening mode: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_UNAVAILABLE );
    }

    // =================================================================
    // Main server loop - here clients are accepted and directed to
    // work functions

    while ( not abort_requested ) {
        std::cout << "Waiting for connections..." << std::endl;

        // Accept client connection - accept() will wait for a client; if a signal is received, then
        // the code exits after the next client connection
        addrlen = sizeof(struct sockaddr_in);
        new_connection_socket = accept( server_socket, (struct sockaddr *)&client_address,  &addrlen );

        // Check if an error occurred
        if ( new_connection_socket == -1 ) {
            if ( abort_requested ) {
                std::cerr << "Error in accept() call after abort request: " << strerror(errno) << " (" << errno << ")\n";
            }
            else {
                std::cerr << "Error during accept() call: " << strerror(errno) << " (" << errno << ")\n";
            }
            break;
        }

        // Start client handling
        std::cout << "Client connected from " << inet_ntoa(client_address.sin_addr) << ":" << ntohs(client_address.sin_port) << " ...\n";
        rc = clientCommunication( &new_connection_socket );
    }

    return(rc);
}

//----------------------------------------------------------------------
// Functions
//----------------------------------------------------------------------

// The clientCommunication() function requires a pointer to the original socket, because
// the signal handler might close the connection while we work with it. So you must not
// copy the socket, otherwise both this function and the signal handler can close the
// client socket.

extern "C" auto clientCommunication( void *socket ) -> int {
    char buffer[BUFFER_SIZE];
    std::string client_message;
    bool quit = false;
    int size;
    int status;
    int rc = EX_OK;

    int *current_socket = (int *)socket;

    // Send welcome message
    status = send( *current_socket, server_welcome.c_str(), server_welcome.length(), 0 );
    if ( status == -1 ) {
        std::cerr << "Error while sending data to client: " << strerror(errno) << " (" << errno << ")\n";
        rc = EX_OSERR;
    }
    else {
        // Receive data from client
        do {
            size = recv( *current_socket, buffer, BUFFER_SIZE-1, 0 );
            // Error condition
            if ( size == -1 ) {
                if ( abort_requested ) {
                    std::cerr << "Error in recv() call after abort request: " << strerror(errno) << " (" << errno << ")\n";
                }
                else {
                    std::cerr << "Error during recv() call: " << strerror(errno) << " (" << errno << ")\n";
                }
            }
            // Client went away
            if ( size == 0 ) {
                std::cout << "Client closed remote socket\n";
                break;
            }
            // Get message; but first fix newline codes possibly sent by client
            if ( ( buffer[size - 2] == '\r' ) && ( buffer[size - 1] == '\n' ) ) {
                size = size - 2;
            }
            else if ( buffer[size - 1] == '\n' ) {
                size = size - 1;
            }
            buffer[size] = '\0';
            client_message.assign( buffer, buffer+size );
            std::cout << "Message received: " << client_message << std::endl;

            quit = ( client_message.compare( 0, 4, "quit" ) == 0 );
            client_message.clear();

            // Send confirmation message
            status = send( *current_socket, server_ok.c_str(), server_ok.length(), 0 );
            if ( status == -1 ) {
                std::cerr << "Error sending OK message to client: " << strerror(errno) << " (" << errno << ")\n";
                rc = EX_OSERR;
                break;
            }
        } while ( not quit );

        // Done with conversation, check if socket is still open in order not to close iut twice
        if ( *current_socket != -1 ) {
            status = shutdown( *current_socket, SHUT_RDWR );
            if ( status == -1 ) {
                std::cerr << "Error shutting down socket: " << strerror(errno) << " (" << errno << ")\n";
                rc = EX_OSERR;
            }
            status = close( *current_socket );
            if ( status == -1 ) {
                std::cerr << "Error closing socket: " << strerror(errno) << " (" << errno << ")\n";
                rc = EX_OSERR;
            }
            *current_socket = -1;
        }
    }

    return(rc);
}

extern "C" auto signalHandler(int signal) -> void {
    switch( signal ) {
        case SIGINT:
        case SIGTERM:
            // Notify for Ctrl-c
            std::cout << "SIGINT received - abort requested...\n";
            abort_requested = true;
            // Shutdown and close client socket
            if ( new_connection_socket != -1 ) {
                int status = shutdown( new_connection_socket, SHUT_RDWR );
                if ( status == -1 ) {
                    std::cerr << "Error shutting down socket: " << strerror(errno) << " (" << errno << ")\n";
                }
                status = close( new_connection_socket );
                if ( status == -1 ) {
                    std::cerr << "Error closing socket: " << strerror(errno) << " (" << errno << ")\n";
                }
                new_connection_socket = -1;
            }
            // Shutdown and close server socket
            if ( server_socket == -1 ) {
                int status = shutdown( server_socket, SHUT_RDWR );
                if ( status == -1 ) {
                    std::cerr << "Error shutting down socket: " << strerror(errno) << " (" << errno << ")\n";
                }
                status = close( server_socket );
                if ( status == -1 ) {
                    std::cerr << "Error closing socket: " << strerror(errno) << " (" << errno << ")\n";
                }
                server_socket = -1;
            }
            exit( signal );
            break;
        case SIGUSR1:
            std::cout << "SIGUSR1 received.\n";
            break;
        case SIGUSR2:
            std::cout << "SIGUSR2 received.\n";
            break;
        default:
            std::cout << "Signal " << signal << " received.\n";
    }
    // Modern C++ recommends not to use the return statement in void function.
}
