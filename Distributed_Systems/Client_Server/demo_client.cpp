// =====================================================================================
//
//       Filename:  demo_client.cpp
//
//    Description:  Demonstration client code with basic sockets. C++ version of the C
//                  code in this directory.
//
//        Version:  1.0
//        Created:  25. sep. 2024 kl. 01.42 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <iostream>
#include <string>

// C
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

const unsigned int BUFFER_SIZE = 65536;
const in_port_t SERVER_PORT = 5427;

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK; // EX_OK is defined in sysexits.h
    char buffer[BUFFER_SIZE];
    std::string message;
    struct sockaddr_in address;
    int client_socket;
    bool quit = false;
    int size;
    int status = -1;

    // -----------------------------------------------------------------
    // Create socket descriptor (i.e. request descriptor from operating
    // system)
    client_socket = socket( AF_INET, SOCK_STREAM, 0 );
    if ( client_socket == -1 ) {
        std::cerr << "Error getting socket descriptor: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_CANTCREAT );
    }

    // -----------------------------------------------------------------
    // Initialise address structure with listening address and port
    memset( &address, 0, sizeof(address) );
    address.sin_family = AF_INET;
    address.sin_port = htons( SERVER_PORT );
    if (argc < 2) {
        inet_aton( "127.0.0.1", &address.sin_addr );
    }
    else {
        inet_aton( argv[1], &address.sin_addr );
    }

    // -----------------------------------------------------------------
    // Create a connection to the server
    status = connect( client_socket, (struct sockaddr *)&address, sizeof(address) );
    if ( status == -1 ) {
        std::cerr << "Error connecting to server: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_UNAVAILABLE );
    }
    std::cout << "Connected to server " << inet_ntoa( address.sin_addr ) << ".\n";

    // -----------------------------------------------------------------
    // Receive data
    size = recv( client_socket, buffer, BUFFER_SIZE-1, 0 );
    if ( size == -1 ) {
        std::cerr << "Error receiving data from server: " << strerror(errno) << " (" << errno << ")\n";
    }
    else if ( size == 0 ) {
        std::cout << "Server closed remote socket.\n";
    }
    else {
        // Make sure buffer is null-terminated
        buffer[size] = 0x0;
        message.assign( buffer, size );
        std::cout << message;
    }

    // -----------------------------------------------------------------
    // Main communication loop
    do {
        std::cout << ">> ";
        getline( std::cin, message );
        if ( message.length() > 0 ) {
            // Check for 'quit' command
            quit = ( message.compare( 0, 4, "quit" ) == 0 );
            // Send input data to server
            status = send( client_socket, message.c_str(), message.length(), 0 );
            if ( status == -1 ) {
                std::cerr << "Error sending data to server: " << strerror(errno) << " (" << errno << ")\n";
                break;
            }
            // Receive feedback from server
            size = recv( client_socket, buffer, BUFFER_SIZE-1, 0 );
            if ( size == -1 ) {
                std::cerr << "Error receiving data from server: " << strerror(errno) << " (" << errno << ")\n";
            }
            else if ( size == 0 ) {
                std::cout << "Server closed remote socket.\n";
            }
            else {
                // Make sure buffer is null-terminated
                buffer[size] = 0x0;
                message.assign( buffer, size );
                std::cout << "<< " << message << "\n";
                if ( message.compare( 0, 2, "OK" ) != 0 ) {
                    std::cerr << "Server did not send 'OK' string: " << strerror(errno) << " (" << errno << ")\n";
                    break;
                }
            }
        }
    } while( not quit );

    // -----------------------------------------------------------------
    // Shutdown and close socket descriptor
    if ( client_socket != -1 ) {
        status = shutdown( client_socket, SHUT_RDWR );
        if ( status == -1 ) {
            std::cerr << "Error shutting down socket: " << strerror(errno) << " (" << errno << ")\n";
            rc = EX_OSERR;
        }
        status = close( client_socket );
        if ( status == -1 ) {
            std::cerr << "Error closing socket: " << strerror(errno) << " (" << errno << ")\n";
            rc = EX_OSERR;
        }
    }

    return(rc);
}
