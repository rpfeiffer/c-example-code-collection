	.text
	.file	"signal_handler_threads.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90                         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	leaq	_ZStL8__ioinit(%rip), %rdi
	callq	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	_ZStL8__ioinit(%rip), %rsi
	leaq	__dso_handle(%rip), %rdx
	callq	__cxa_atexit@PLT
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_Z15handler_sigtermi            # -- Begin function _Z15handler_sigtermi
	.p2align	4, 0x90
	.type	_Z15handler_sigtermi,@function
_Z15handler_sigtermi:                   # @_Z15handler_sigtermi
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	%edi, -4(%rbp)
	movq	_ZSt4cout@GOTPCREL(%rip), %rdi
	leaq	.L.str(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	callq	getpid@PLT
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.1(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdi
	movl	-4(%rbp), %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.2(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -16(%rbp)                 # 8-byte Spill
	callq	gettid@PLT
	movq	-16(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.3(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	_Z15handler_sigtermi, .Lfunc_end1-_Z15handler_sigtermi
	.cfi_endproc
                                        # -- End function
	.globl	_Z14handler_siginti             # -- Begin function _Z14handler_siginti
	.p2align	4, 0x90
	.type	_Z14handler_siginti,@function
_Z14handler_siginti:                    # @_Z14handler_siginti
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	%edi, -4(%rbp)
	movq	_ZSt4cout@GOTPCREL(%rip), %rdi
	leaq	.L.str(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	callq	getpid@PLT
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.4(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdi
	movl	-4(%rbp), %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.2(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -16(%rbp)                 # 8-byte Spill
	callq	gettid@PLT
	movq	-16(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.3(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_Z14handler_siginti, .Lfunc_end2-_Z14handler_siginti
	.cfi_endproc
                                        # -- End function
	.globl	_Z7thread1j                     # -- Begin function _Z7thread1j
	.p2align	4, 0x90
	.type	_Z7thread1j,@function
_Z7thread1j:                            # @_Z7thread1j
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	%edi, -4(%rbp)
	movq	_ZSt4cout@GOTPCREL(%rip), %rdi
	leaq	.L.str.5(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdi
	movl	-4(%rbp), %esi
	callq	_ZNSolsEj@PLT
	movq	%rax, %rdi
	leaq	.L.str.6(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	callq	getpid@PLT
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.7(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -16(%rbp)                 # 8-byte Spill
	callq	gettid@PLT
	movq	-16(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.8(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	-4(%rbp), %edi
	callq	sleep@PLT
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end3:
	.size	_Z7thread1j, .Lfunc_end3-_Z7thread1j
	.cfi_endproc
                                        # -- End function
	.globl	_Z7thread2j                     # -- Begin function _Z7thread2j
	.p2align	4, 0x90
	.type	_Z7thread2j,@function
_Z7thread2j:                            # @_Z7thread2j
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	%edi, -4(%rbp)
	movq	_ZSt4cout@GOTPCREL(%rip), %rdi
	leaq	.L.str.9(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdi
	movl	-4(%rbp), %esi
	callq	_ZNSolsEj@PLT
	movq	%rax, %rdi
	leaq	.L.str.6(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	callq	getpid@PLT
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.7(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -16(%rbp)                 # 8-byte Spill
	callq	gettid@PLT
	movq	-16(%rbp), %rdi                 # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.8(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	-4(%rbp), %edi
	callq	sleep@PLT
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end4:
	.size	_Z7thread2j, .Lfunc_end4-_Z7thread2j
	.cfi_endproc
                                        # -- End function
	.globl	main                            # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, DW.ref.__gxx_personality_v0
	.cfi_lsda 27, .Lexception0
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movl	$0, -4(%rbp)
	movl	%edi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	$0, -20(%rbp)
	movl	$15, %edi
	leaq	_Z15handler_sigtermi(%rip), %rsi
	callq	signal@PLT
	movq	$-1, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_2
# %bb.1:
	movq	_ZSt4cerr@GOTPCREL(%rip), %rdi
	leaq	.L.str.10(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -88(%rbp)                 # 8-byte Spill
	callq	__errno_location@PLT
	movl	(%rax), %edi
	callq	strerror@PLT
	movq	-88(%rbp), %rdi                 # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdi
	leaq	.L.str.11(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -80(%rbp)                 # 8-byte Spill
	callq	__errno_location@PLT
	movq	-80(%rbp), %rdi                 # 8-byte Reload
	movl	(%rax), %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.8(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$69, -20(%rbp)
.LBB5_2:
	movl	$2, %edi
	leaq	_Z14handler_siginti(%rip), %rsi
	callq	signal@PLT
	movq	$-1, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_4
# %bb.3:
	movq	_ZSt4cerr@GOTPCREL(%rip), %rdi
	leaq	.L.str.12(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -104(%rbp)                # 8-byte Spill
	callq	__errno_location@PLT
	movl	(%rax), %edi
	callq	strerror@PLT
	movq	-104(%rbp), %rdi                # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, %rdi
	leaq	.L.str.11(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -96(%rbp)                 # 8-byte Spill
	callq	__errno_location@PLT
	movq	-96(%rbp), %rdi                 # 8-byte Reload
	movl	(%rax), %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.8(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$69, -20(%rbp)
.LBB5_4:
	movq	_ZSt4cout@GOTPCREL(%rip), %rdi
	leaq	.L.str.13(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%rax, -112(%rbp)                # 8-byte Spill
	callq	getpid@PLT
	movq	-112(%rbp), %rdi                # 8-byte Reload
	movl	%eax, %esi
	callq	_ZNSolsEi@PLT
	movq	%rax, %rdi
	leaq	.L.str.14(%rip), %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$35, -36(%rbp)
	leaq	_Z7thread1j(%rip), %rsi
	leaq	-32(%rbp), %rdi
	leaq	-36(%rbp), %rdx
	callq	_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_
.Ltmp0:
	movl	$1, %edi
	callq	sleep@PLT
.Ltmp1:
	jmp	.LBB5_5
.LBB5_5:
	movl	$55, -68(%rbp)
.Ltmp2:
	leaq	_Z7thread2j(%rip), %rsi
	leaq	-64(%rbp), %rdi
	leaq	-68(%rbp), %rdx
	callq	_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_
.Ltmp3:
	jmp	.LBB5_6
.LBB5_6:
.Ltmp5:
	movl	$60, %edi
	callq	sleep@PLT
.Ltmp6:
	jmp	.LBB5_7
.LBB5_7:
.Ltmp7:
	leaq	-32(%rbp), %rdi
	callq	_ZNSt6thread4joinEv@PLT
.Ltmp8:
	jmp	.LBB5_8
.LBB5_8:
.Ltmp9:
	leaq	-64(%rbp), %rdi
	callq	_ZNSt6thread4joinEv@PLT
.Ltmp10:
	jmp	.LBB5_9
.LBB5_9:
	movl	-20(%rbp), %eax
	movl	%eax, -4(%rbp)
	leaq	-64(%rbp), %rdi
	callq	_ZNSt6threadD2Ev
	leaq	-32(%rbp), %rdi
	callq	_ZNSt6threadD2Ev
	movl	-4(%rbp), %eax
	addq	$112, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB5_10:
	.cfi_def_cfa %rbp, 16
.Ltmp4:
	movq	%rax, %rcx
	movl	%edx, %eax
	movq	%rcx, -48(%rbp)
	movl	%eax, -52(%rbp)
	jmp	.LBB5_12
.LBB5_11:
.Ltmp11:
	movq	%rax, %rcx
	movl	%edx, %eax
	movq	%rcx, -48(%rbp)
	movl	%eax, -52(%rbp)
	leaq	-64(%rbp), %rdi
	callq	_ZNSt6threadD2Ev
.LBB5_12:
	leaq	-32(%rbp), %rdi
	callq	_ZNSt6threadD2Ev
# %bb.13:
	movq	-48(%rbp), %rdi
	callq	_Unwind_Resume@PLT
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2, 0x0
GCC_except_table5:
.Lexception0:
	.byte	255                             # @LPStart Encoding = omit
	.byte	255                             # @TType Encoding = omit
	.byte	1                               # Call site Encoding = uleb128
	.uleb128 .Lcst_end0-.Lcst_begin0
.Lcst_begin0:
	.uleb128 .Lfunc_begin0-.Lfunc_begin0    # >> Call Site 1 <<
	.uleb128 .Ltmp0-.Lfunc_begin0           #   Call between .Lfunc_begin0 and .Ltmp0
	.byte	0                               #     has no landing pad
	.byte	0                               #   On action: cleanup
	.uleb128 .Ltmp0-.Lfunc_begin0           # >> Call Site 2 <<
	.uleb128 .Ltmp3-.Ltmp0                  #   Call between .Ltmp0 and .Ltmp3
	.uleb128 .Ltmp4-.Lfunc_begin0           #     jumps to .Ltmp4
	.byte	0                               #   On action: cleanup
	.uleb128 .Ltmp5-.Lfunc_begin0           # >> Call Site 3 <<
	.uleb128 .Ltmp10-.Ltmp5                 #   Call between .Ltmp5 and .Ltmp10
	.uleb128 .Ltmp11-.Lfunc_begin0          #     jumps to .Ltmp11
	.byte	0                               #   On action: cleanup
	.uleb128 .Ltmp10-.Lfunc_begin0          # >> Call Site 4 <<
	.uleb128 .Lfunc_end5-.Ltmp10            #   Call between .Ltmp10 and .Lfunc_end5
	.byte	0                               #     has no landing pad
	.byte	0                               #   On action: cleanup
.Lcst_end0:
	.p2align	2, 0x0
                                        # -- End function
	.section	.text._ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_,"axG",@progbits,_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_,comdat
	.weak	_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_ # -- Begin function _ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_
	.p2align	4, 0x90
	.type	_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_,@function
_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_:    # @_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, DW.ref.__gxx_personality_v0
	.cfi_lsda 27, .Lexception1
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -72(%rbp)                 # 8-byte Spill
	callq	_ZNSt6thread2idC2Ev
	movq	$0, -32(%rbp)
	movl	$24, %edi
	callq	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rdi, %rax
	movq	%rax, -64(%rbp)                 # 8-byte Spill
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
.Ltmp12:
	callq	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_
.Ltmp13:
	jmp	.LBB6_1
.LBB6_1:
	movq	-64(%rbp), %rsi                 # 8-byte Reload
	leaq	-40(%rbp), %rdi
	movq	%rdi, -80(%rbp)                 # 8-byte Spill
	callq	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_
	movq	-72(%rbp), %rdi                 # 8-byte Reload
	movq	-80(%rbp), %rsi                 # 8-byte Reload
.Ltmp15:
	xorl	%eax, %eax
	movl	%eax, %edx
	callq	_ZNSt6thread15_M_start_threadESt10unique_ptrINS_6_StateESt14default_deleteIS1_EEPFvvE@PLT
.Ltmp16:
	jmp	.LBB6_2
.LBB6_2:
	leaq	-40(%rbp), %rdi
	callq	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev
	addq	$80, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB6_3:
	.cfi_def_cfa %rbp, 16
.Ltmp14:
	movq	-64(%rbp), %rdi                 # 8-byte Reload
	movq	%rax, %rcx
	movl	%edx, %eax
	movq	%rcx, -48(%rbp)
	movl	%eax, -52(%rbp)
	movl	$24, %esi
	callq	_ZdlPvm@PLT
	jmp	.LBB6_5
.LBB6_4:
.Ltmp17:
	movq	%rax, %rcx
	movl	%edx, %eax
	movq	%rcx, -48(%rbp)
	movl	%eax, -52(%rbp)
	leaq	-40(%rbp), %rdi
	callq	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev
.LBB6_5:
	movq	-48(%rbp), %rdi
	callq	_Unwind_Resume@PLT
.Lfunc_end6:
	.size	_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_, .Lfunc_end6-_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_
	.cfi_endproc
	.section	.gcc_except_table._ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_,"aG",@progbits,_ZNSt6threadC2IRFvjEJiEvEEOT_DpOT0_,comdat
	.p2align	2, 0x0
GCC_except_table6:
.Lexception1:
	.byte	255                             # @LPStart Encoding = omit
	.byte	255                             # @TType Encoding = omit
	.byte	1                               # Call site Encoding = uleb128
	.uleb128 .Lcst_end1-.Lcst_begin1
.Lcst_begin1:
	.uleb128 .Lfunc_begin1-.Lfunc_begin1    # >> Call Site 1 <<
	.uleb128 .Ltmp12-.Lfunc_begin1          #   Call between .Lfunc_begin1 and .Ltmp12
	.byte	0                               #     has no landing pad
	.byte	0                               #   On action: cleanup
	.uleb128 .Ltmp12-.Lfunc_begin1          # >> Call Site 2 <<
	.uleb128 .Ltmp13-.Ltmp12                #   Call between .Ltmp12 and .Ltmp13
	.uleb128 .Ltmp14-.Lfunc_begin1          #     jumps to .Ltmp14
	.byte	0                               #   On action: cleanup
	.uleb128 .Ltmp15-.Lfunc_begin1          # >> Call Site 3 <<
	.uleb128 .Ltmp16-.Ltmp15                #   Call between .Ltmp15 and .Ltmp16
	.uleb128 .Ltmp17-.Lfunc_begin1          #     jumps to .Ltmp17
	.byte	0                               #   On action: cleanup
	.uleb128 .Ltmp16-.Lfunc_begin1          # >> Call Site 4 <<
	.uleb128 .Lfunc_end6-.Ltmp16            #   Call between .Ltmp16 and .Lfunc_end6
	.byte	0                               #     has no landing pad
	.byte	0                               #   On action: cleanup
.Lcst_end1:
	.p2align	2, 0x0
                                        # -- End function
	.section	.text._ZNSt6threadD2Ev,"axG",@progbits,_ZNSt6threadD2Ev,comdat
	.weak	_ZNSt6threadD2Ev                # -- Begin function _ZNSt6threadD2Ev
	.p2align	4, 0x90
	.type	_ZNSt6threadD2Ev,@function
_ZNSt6threadD2Ev:                       # @_ZNSt6threadD2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNKSt6thread8joinableEv
	testb	$1, %al
	jne	.LBB7_1
	jmp	.LBB7_2
.LBB7_1:
	callq	_ZSt9terminatev@PLT
.LBB7_2:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end7:
	.size	_ZNSt6threadD2Ev, .Lfunc_end7-_ZNSt6threadD2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNKSt6thread8joinableEv,"axG",@progbits,_ZNKSt6thread8joinableEv,comdat
	.weak	_ZNKSt6thread8joinableEv        # -- Begin function _ZNKSt6thread8joinableEv
	.p2align	4, 0x90
	.type	_ZNKSt6thread8joinableEv,@function
_ZNKSt6thread8joinableEv:               # @_ZNKSt6thread8joinableEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-24(%rbp), %rdi
	callq	_ZNSt6thread2idC2Ev
	movq	-16(%rbp), %rdi
	movq	-24(%rbp), %rsi
	callq	_ZSteqNSt6thread2idES0_
	xorb	$-1, %al
	andb	$1, %al
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end8:
	.size	_ZNKSt6thread8joinableEv, .Lfunc_end8-_ZNKSt6thread8joinableEv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSteqNSt6thread2idES0_,"axG",@progbits,_ZSteqNSt6thread2idES0_,comdat
	.weak	_ZSteqNSt6thread2idES0_         # -- Begin function _ZSteqNSt6thread2idES0_
	.p2align	4, 0x90
	.type	_ZSteqNSt6thread2idES0_,@function
_ZSteqNSt6thread2idES0_:                # @_ZSteqNSt6thread2idES0_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	cmpq	-16(%rbp), %rax
	sete	%al
	andb	$1, %al
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end9:
	.size	_ZSteqNSt6thread2idES0_, .Lfunc_end9-_ZSteqNSt6thread2idES0_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt6thread2idC2Ev,"axG",@progbits,_ZNSt6thread2idC2Ev,comdat
	.weak	_ZNSt6thread2idC2Ev             # -- Begin function _ZNSt6thread2idC2Ev
	.p2align	4, 0x90
	.type	_ZNSt6thread2idC2Ev,@function
_ZNSt6thread2idC2Ev:                    # @_ZNSt6thread2idC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end10:
	.size	_ZNSt6thread2idC2Ev, .Lfunc_end10-_ZNSt6thread2idC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_,"axG",@progbits,_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_,comdat
	.weak	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_ # -- Begin function _ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_
	.p2align	4, 0x90
	.type	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_,@function
_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_: # @_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -32(%rbp)                 # 8-byte Spill
	callq	_ZNSt6thread6_StateC2Ev
	movq	-32(%rbp), %rdi                 # 8-byte Reload
	leaq	_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rdi)
	addq	$8, %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	callq	_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end11:
	.size	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_, .Lfunc_end11-_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEC2IJRS3_iEEEDpOT_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_,"axG",@progbits,_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_,comdat
	.weak	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_ # -- Begin function _ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_
	.p2align	4, 0x90
	.type	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_,@function
_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_: # @_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, DW.ref.__gxx_personality_v0
	.cfi_lsda 27, .Lexception2
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
.Ltmp18:
	callq	_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_
.Ltmp19:
	jmp	.LBB12_1
.LBB12_1:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB12_2:
	.cfi_def_cfa %rbp, 16
.Ltmp20:
	movq	%rax, %rdi
                                        # kill: def $eax killed $edx killed $rdx
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_, .Lfunc_end12-_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_
	.cfi_endproc
	.section	.gcc_except_table._ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_,"aG",@progbits,_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EEC2IS3_vEEPS1_,comdat
	.p2align	2, 0x0
GCC_except_table12:
.Lexception2:
	.byte	255                             # @LPStart Encoding = omit
	.byte	155                             # @TType Encoding = indirect pcrel sdata4
	.uleb128 .Lttbase0-.Lttbaseref0
.Lttbaseref0:
	.byte	1                               # Call site Encoding = uleb128
	.uleb128 .Lcst_end2-.Lcst_begin2
.Lcst_begin2:
	.uleb128 .Ltmp18-.Lfunc_begin2          # >> Call Site 1 <<
	.uleb128 .Ltmp19-.Ltmp18                #   Call between .Ltmp18 and .Ltmp19
	.uleb128 .Ltmp20-.Lfunc_begin2          #     jumps to .Ltmp20
	.byte	1                               #   On action: 1
.Lcst_end2:
	.byte	1                               # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                               #   No further actions
	.p2align	2, 0x0
                                        # >> Catch TypeInfos <<
	.long	0                               # TypeInfo 1
.Lttbase0:
	.p2align	2, 0x0
                                        # -- End function
	.section	.text._ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev,comdat
	.weak	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev # -- Begin function _ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev
	.p2align	4, 0x90
	.type	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev,@function
_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev: # @_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, DW.ref.__gxx_personality_v0
	.cfi_lsda 27, .Lexception3
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -24(%rbp)                 # 8-byte Spill
	callq	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	cmpq	$0, (%rax)
	je	.LBB13_3
# %bb.1:
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	callq	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv
	movq	%rax, %rdi
	movq	-16(%rbp), %rax
	movq	(%rax), %rsi
.Ltmp21:
	callq	_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_
.Ltmp22:
	jmp	.LBB13_2
.LBB13_2:
	jmp	.LBB13_3
.LBB13_3:
	movq	-16(%rbp), %rax
	movq	$0, (%rax)
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB13_4:
	.cfi_def_cfa %rbp, 16
.Ltmp23:
	movq	%rax, %rdi
                                        # kill: def $eax killed $edx killed $rdx
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev, .Lfunc_end13-_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev
	.cfi_endproc
	.section	.gcc_except_table._ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev,"aG",@progbits,_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EED2Ev,comdat
	.p2align	2, 0x0
GCC_except_table13:
.Lexception3:
	.byte	255                             # @LPStart Encoding = omit
	.byte	155                             # @TType Encoding = indirect pcrel sdata4
	.uleb128 .Lttbase1-.Lttbaseref1
.Lttbaseref1:
	.byte	1                               # Call site Encoding = uleb128
	.uleb128 .Lcst_end3-.Lcst_begin3
.Lcst_begin3:
	.uleb128 .Ltmp21-.Lfunc_begin3          # >> Call Site 1 <<
	.uleb128 .Ltmp22-.Ltmp21                #   Call between .Ltmp21 and .Ltmp22
	.uleb128 .Ltmp23-.Lfunc_begin3          #     jumps to .Ltmp23
	.byte	1                               #   On action: 1
.Lcst_end3:
	.byte	1                               # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                               #   No further actions
	.p2align	2, 0x0
                                        # >> Catch TypeInfos <<
	.long	0                               # TypeInfo 1
.Lttbase1:
	.p2align	2, 0x0
                                        # -- End function
	.section	.text._ZNSt6thread6_StateC2Ev,"axG",@progbits,_ZNSt6thread6_StateC2Ev,comdat
	.weak	_ZNSt6thread6_StateC2Ev         # -- Begin function _ZNSt6thread6_StateC2Ev
	.p2align	4, 0x90
	.type	_ZNSt6thread6_StateC2Ev,@function
_ZNSt6thread6_StateC2Ev:                # @_ZNSt6thread6_StateC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	_ZTVNSt6thread6_StateE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end14:
	.size	_ZNSt6thread6_StateC2Ev, .Lfunc_end14-_ZNSt6thread6_StateC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_,"axG",@progbits,_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_,comdat
	.weak	_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_ # -- Begin function _ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_
	.p2align	4, 0x90
	.type	_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_,@function
_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_: # @_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, DW.ref.__gxx_personality_v0
	.cfi_lsda 27, .Lexception4
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
.Ltmp24:
	callq	_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_
.Ltmp25:
	jmp	.LBB15_1
.LBB15_1:
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB15_2:
	.cfi_def_cfa %rbp, 16
.Ltmp26:
	movq	%rax, %rdi
                                        # kill: def $eax killed $edx killed $rdx
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_, .Lfunc_end15-_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_
	.cfi_endproc
	.section	.gcc_except_table._ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_,"aG",@progbits,_ZNSt5tupleIJPFvjEiEEC2IRS0_iTnNSt9enable_ifIXclsr4_TCCIXntcl14__is_alloc_argIT_EEEEE29__is_implicitly_constructibleIS6_T0_EEEbE4typeELb1EEEOS6_OS7_,comdat
	.p2align	2, 0x0
GCC_except_table15:
.Lexception4:
	.byte	255                             # @LPStart Encoding = omit
	.byte	155                             # @TType Encoding = indirect pcrel sdata4
	.uleb128 .Lttbase2-.Lttbaseref2
.Lttbaseref2:
	.byte	1                               # Call site Encoding = uleb128
	.uleb128 .Lcst_end4-.Lcst_begin4
.Lcst_begin4:
	.uleb128 .Ltmp24-.Lfunc_begin4          # >> Call Site 1 <<
	.uleb128 .Ltmp25-.Ltmp24                #   Call between .Ltmp24 and .Ltmp25
	.uleb128 .Ltmp26-.Lfunc_begin4          #     jumps to .Ltmp26
	.byte	1                               #   On action: 1
.Lcst_end4:
	.byte	1                               # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                               #   No further actions
	.p2align	2, 0x0
                                        # >> Catch TypeInfos <<
	.long	0                               # TypeInfo 1
.Lttbase2:
	.p2align	2, 0x0
                                        # -- End function
	.section	.text._ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev,"axG",@progbits,_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev,comdat
	.weak	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev # -- Begin function _ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev
	.p2align	4, 0x90
	.type	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev,@function
_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev: # @_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt6thread6_StateD2Ev@PLT
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end16:
	.size	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev, .Lfunc_end16-_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev,"axG",@progbits,_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev,comdat
	.weak	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev # -- Begin function _ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev
	.p2align	4, 0x90
	.type	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev,@function
_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev: # @_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)                 # 8-byte Spill
	callq	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev
	movq	-16(%rbp), %rdi                 # 8-byte Reload
	movl	$24, %esi
	callq	_ZdlPvm@PLT
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end17:
	.size	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev, .Lfunc_end17-_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv,"axG",@progbits,_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv,comdat
	.weak	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv # -- Begin function _ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv
	.p2align	4, 0x90
	.type	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv,@function
_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv: # @_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	addq	$8, %rdi
	callq	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end18:
	.size	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv, .Lfunc_end18-_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_,"axG",@progbits,_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_,comdat
	.weak	_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_ # -- Begin function _ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_,@function
_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_: # @_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -40(%rbp)                 # 8-byte Spill
	movq	-24(%rbp), %rsi
	callq	_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_
	movq	-40(%rbp), %rdi                 # 8-byte Reload
	addq	$8, %rdi
	movq	-16(%rbp), %rax
	movq	%rax, -32(%rbp)
	leaq	-32(%rbp), %rsi
	callq	_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_
	addq	$48, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end19:
	.size	_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_, .Lfunc_end19-_ZNSt11_Tuple_implILm0EJPFvjEiEEC2IRS0_JiEvEEOT_DpOT0_
	.cfi_endproc
                                        # -- End function
	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate          # -- Begin function __clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxa_begin_catch@PLT
	callq	_ZSt9terminatev@PLT
.Lfunc_end20:
	.size	__clang_call_terminate, .Lfunc_end20-__clang_call_terminate
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_,"axG",@progbits,_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_,comdat
	.weak	_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_ # -- Begin function _ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_,@function
_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_:   # @_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end21:
	.size	_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_, .Lfunc_end21-_ZNSt11_Tuple_implILm1EJiEEC2IiEEOT_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_,"axG",@progbits,_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_,comdat
	.weak	_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_ # -- Begin function _ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_,@function
_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_: # @_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end22:
	.size	_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_, .Lfunc_end22-_ZNSt10_Head_baseILm0EPFvjELb0EEC2ERKS1_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_,"axG",@progbits,_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_,comdat
	.weak	_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_ # -- Begin function _ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_,@function
_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_:  # @_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rax)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end23:
	.size	_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_, .Lfunc_end23-_ZNSt10_Head_baseILm1EiLb0EEC2IiEEOT_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv,"axG",@progbits,_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv,comdat
	.weak	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv # -- Begin function _ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv
	.p2align	4, 0x90
	.type	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv,@function
_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv: # @_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end24:
	.size	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv, .Lfunc_end24-_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE,"axG",@progbits,_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE,comdat
	.weak	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE # -- Begin function _ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE
	.p2align	4, 0x90
	.type	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE,@function
_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE: # @_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -32(%rbp)                 # 8-byte Spill
	callq	_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	movq	-32(%rbp), %rdi                 # 8-byte Reload
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	callq	_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end25:
	.size	_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE, .Lfunc_end25-_ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_,"axG",@progbits,_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_,comdat
	.weak	_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_ # -- Begin function _ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_
	.p2align	4, 0x90
	.type	_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_,@function
_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_: # @_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end26:
	.size	_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_, .Lfunc_end26-_ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_,"axG",@progbits,_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_,comdat
	.weak	_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_ # -- Begin function _ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.p2align	4, 0x90
	.type	_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_,@function
_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_: # @_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end27:
	.size	_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_, .Lfunc_end27-_ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_,"axG",@progbits,_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_,comdat
	.weak	_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_ # -- Begin function _ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.p2align	4, 0x90
	.type	_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_,@function
_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_: # @_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end28:
	.size	_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_, .Lfunc_end28-_ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_,"axG",@progbits,_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_,comdat
	.weak	_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_ # -- Begin function _ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_
	.p2align	4, 0x90
	.type	_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_,@function
_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_: # @_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	movl	(%rcx), %edi
	callq	*%rax
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end29:
	.size	_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_, .Lfunc_end29-_ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE,"axG",@progbits,_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE,comdat
	.weak	_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE # -- Begin function _ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE
	.p2align	4, 0x90
	.type	_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE,@function
_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE: # @_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end30:
	.size	_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE, .Lfunc_end30-_ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_,"axG",@progbits,_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_,comdat
	.weak	_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_ # -- Begin function _ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_,@function
_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_: # @_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	addq	$8, %rdi
	callq	_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end31:
	.size	_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_, .Lfunc_end31-_ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_,"axG",@progbits,_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_,comdat
	.weak	_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_ # -- Begin function _ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_,@function
_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_: # @_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end32:
	.size	_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_, .Lfunc_end32-_ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE,"axG",@progbits,_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE,comdat
	.weak	_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE # -- Begin function _ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE
	.p2align	4, 0x90
	.type	_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE,@function
_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE: # @_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end33:
	.size	_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE, .Lfunc_end33-_ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_,"axG",@progbits,_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_,comdat
	.weak	_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_ # -- Begin function _ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_,@function
_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_: # @_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end34:
	.size	_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_, .Lfunc_end34-_ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_,"axG",@progbits,_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_,comdat
	.weak	_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_ # -- Begin function _ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_,@function
_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_: # @_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end35:
	.size	_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_, .Lfunc_end35-_ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_,"axG",@progbits,_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_,comdat
	.weak	_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_ # -- Begin function _ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_
	.p2align	4, 0x90
	.type	_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_,@function
_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_: # @_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end36:
	.size	_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_, .Lfunc_end36-_ZNSt15__uniq_ptr_dataINSt6thread6_StateESt14default_deleteIS1_ELb1ELb1EECI2St15__uniq_ptr_implIS1_S3_EEPS1_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_,"axG",@progbits,_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_,comdat
	.weak	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_ # -- Begin function _ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_
	.p2align	4, 0x90
	.type	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_,@function
_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_: # @_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -32(%rbp)                 # 8-byte Spill
	callq	_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv
	movq	-32(%rbp), %rdi                 # 8-byte Reload
	movq	-16(%rbp), %rax
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	callq	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv
	movq	-24(%rbp), %rcx                 # 8-byte Reload
	movq	%rcx, (%rax)
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end37:
	.size	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_, .Lfunc_end37-_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EEC2EPS1_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv,"axG",@progbits,_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv,comdat
	.weak	_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv # -- Begin function _ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv
	.p2align	4, 0x90
	.type	_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv,@function
_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv: # @_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, DW.ref.__gxx_personality_v0
	.cfi_lsda 27, .Lexception5
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
.Ltmp27:
	callq	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev
.Ltmp28:
	jmp	.LBB38_1
.LBB38_1:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB38_2:
	.cfi_def_cfa %rbp, 16
.Ltmp29:
	movq	%rax, %rdi
                                        # kill: def $eax killed $edx killed $rdx
	callq	__clang_call_terminate
.Lfunc_end38:
	.size	_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv, .Lfunc_end38-_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv
	.cfi_endproc
	.section	.gcc_except_table._ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv,"aG",@progbits,_ZNSt5tupleIJPNSt6thread6_StateESt14default_deleteIS1_EEEC2ILb1ETnNSt9enable_ifIXclsr17_TupleConstraintsIXT_ES2_S4_EE37__is_implicitly_default_constructibleEEbE4typeELb1EEEv,comdat
	.p2align	2, 0x0
GCC_except_table38:
.Lexception5:
	.byte	255                             # @LPStart Encoding = omit
	.byte	155                             # @TType Encoding = indirect pcrel sdata4
	.uleb128 .Lttbase3-.Lttbaseref3
.Lttbaseref3:
	.byte	1                               # Call site Encoding = uleb128
	.uleb128 .Lcst_end5-.Lcst_begin5
.Lcst_begin5:
	.uleb128 .Ltmp27-.Lfunc_begin5          # >> Call Site 1 <<
	.uleb128 .Ltmp28-.Ltmp27                #   Call between .Ltmp27 and .Ltmp28
	.uleb128 .Ltmp29-.Lfunc_begin5          #     jumps to .Ltmp29
	.byte	1                               #   On action: 1
.Lcst_end5:
	.byte	1                               # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                               #   No further actions
	.p2align	2, 0x0
                                        # >> Catch TypeInfos <<
	.long	0                               # TypeInfo 1
.Lttbase3:
	.p2align	2, 0x0
                                        # -- End function
	.section	.text._ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv,"axG",@progbits,_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv,comdat
	.weak	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv # -- Begin function _ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv
	.p2align	4, 0x90
	.type	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv,@function
_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv: # @_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end39:
	.size	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv, .Lfunc_end39-_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev,"axG",@progbits,_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev,comdat
	.weak	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev # -- Begin function _ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev,@function
_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev: # @_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)                 # 8-byte Spill
	callq	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev
	movq	-16(%rbp), %rdi                 # 8-byte Reload
	callq	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end40:
	.size	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev, .Lfunc_end40-_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEEC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev,"axG",@progbits,_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev,comdat
	.weak	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev # -- Begin function _ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev,@function
_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev: # @_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end41:
	.size	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev, .Lfunc_end41-_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEEC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev,"axG",@progbits,_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev,comdat
	.weak	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev # -- Begin function _ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev,@function
_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev: # @_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end42:
	.size	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev, .Lfunc_end42-_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EEC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev,"axG",@progbits,_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev,comdat
	.weak	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev # -- Begin function _ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev,@function
_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev: # @_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end43:
	.size	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev, .Lfunc_end43-_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EEC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_,"axG",@progbits,_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_,comdat
	.weak	_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_ # -- Begin function _ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.p2align	4, 0x90
	.type	_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_,@function
_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_: # @_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end44:
	.size	_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_, .Lfunc_end44-_ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE,"axG",@progbits,_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE,comdat
	.weak	_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE # -- Begin function _ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE
	.p2align	4, 0x90
	.type	_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE,@function
_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE: # @_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end45:
	.size	_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE, .Lfunc_end45-_ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_,"axG",@progbits,_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_,comdat
	.weak	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_ # -- Begin function _ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_,@function
_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_: # @_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end46:
	.size	_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_, .Lfunc_end46-_ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_,"axG",@progbits,_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_,comdat
	.weak	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_ # -- Begin function _ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_,@function
_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_: # @_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end47:
	.size	_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_, .Lfunc_end47-_ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv,"axG",@progbits,_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv,comdat
	.weak	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv # -- Begin function _ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv
	.p2align	4, 0x90
	.type	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv,@function
_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv: # @_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end48:
	.size	_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv, .Lfunc_end48-_ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_,"axG",@progbits,_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_,comdat
	.weak	_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_ # -- Begin function _ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_
	.p2align	4, 0x90
	.type	_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_,@function
_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_: # @_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, -24(%rbp)                 # 8-byte Spill
	cmpq	$0, %rax
	je	.LBB49_2
# %bb.1:
	movq	-24(%rbp), %rdi                 # 8-byte Reload
	movq	(%rdi), %rax
	callq	*8(%rax)
.LBB49_2:
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end49:
	.size	_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_, .Lfunc_end49-_ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv,"axG",@progbits,_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv,comdat
	.weak	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv # -- Begin function _ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv
	.p2align	4, 0x90
	.type	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv,@function
_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv: # @_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end50:
	.size	_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv, .Lfunc_end50-_ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_,"axG",@progbits,_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_,comdat
	.weak	_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_ # -- Begin function _ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.p2align	4, 0x90
	.type	_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_,@function
_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_: # @_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end51:
	.size	_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_, .Lfunc_end51-_ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE,"axG",@progbits,_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE,comdat
	.weak	_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE # -- Begin function _ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE
	.p2align	4, 0x90
	.type	_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE,@function
_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE: # @_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end52:
	.size	_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE, .Lfunc_end52-_ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_,"axG",@progbits,_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_,comdat
	.weak	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_ # -- Begin function _ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_
	.p2align	4, 0x90
	.type	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_,@function
_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_: # @_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end53:
	.size	_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_, .Lfunc_end53-_ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_,"axG",@progbits,_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_,comdat
	.weak	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_ # -- Begin function _ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_
	.p2align	4, 0x90
	.type	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_,@function
_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_: # @_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end54:
	.size	_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_, .Lfunc_end54-_ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90                         # -- Begin function _GLOBAL__sub_I_signal_handler_threads.cpp
	.type	_GLOBAL__sub_I_signal_handler_threads.cpp,@function
_GLOBAL__sub_I_signal_handler_threads.cpp: # @_GLOBAL__sub_I_signal_handler_threads.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end55:
	.size	_GLOBAL__sub_I_signal_handler_threads.cpp, .Lfunc_end55-_GLOBAL__sub_I_signal_handler_threads.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object          # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object                  # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Process "
	.size	.L.str, 9

	.type	.L.str.1,@object                # @.str.1
.L.str.1:
	.asciz	" received signal SIGTERM ("
	.size	.L.str.1, 27

	.type	.L.str.2,@object                # @.str.2
.L.str.2:
	.asciz	"). TID = "
	.size	.L.str.2, 10

	.type	.L.str.3,@object                # @.str.3
.L.str.3:
	.asciz	"\n"
	.size	.L.str.3, 2

	.type	.L.str.4,@object                # @.str.4
.L.str.4:
	.asciz	" received signal SIGINT ("
	.size	.L.str.4, 26

	.type	.L.str.5,@object                # @.str.5
.L.str.5:
	.asciz	"Thread 1 running with argument "
	.size	.L.str.5, 32

	.type	.L.str.6,@object                # @.str.6
.L.str.6:
	.asciz	" (PID = "
	.size	.L.str.6, 9

	.type	.L.str.7,@object                # @.str.7
.L.str.7:
	.asciz	", TID = "
	.size	.L.str.7, 9

	.type	.L.str.8,@object                # @.str.8
.L.str.8:
	.asciz	")\n"
	.size	.L.str.8, 3

	.type	.L.str.9,@object                # @.str.9
.L.str.9:
	.asciz	"Thread 2 running with argument "
	.size	.L.str.9, 32

	.type	.L.str.10,@object               # @.str.10
.L.str.10:
	.asciz	"ERROR: Could not set SIGTERM handler - reason: "
	.size	.L.str.10, 48

	.type	.L.str.11,@object               # @.str.11
.L.str.11:
	.asciz	" ("
	.size	.L.str.11, 3

	.type	.L.str.12,@object               # @.str.12
.L.str.12:
	.asciz	"ERROR: Could not set SIGINT handler - reason: "
	.size	.L.str.12, 47

	.type	.L.str.13,@object               # @.str.13
.L.str.13:
	.asciz	"Parent PID is "
	.size	.L.str.13, 15

	.type	.L.str.14,@object               # @.str.14
.L.str.14:
	.asciz	". Sleeping now.\n"
	.size	.L.str.14, 17

	.type	_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,@object # @_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.section	.data.rel.ro._ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,"awG",@progbits,_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,comdat
	.weak	_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.p2align	3, 0x0
_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE:
	.quad	0
	.quad	_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.quad	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED2Ev
	.quad	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEED0Ev
	.quad	_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEE6_M_runEv
	.size	_ZTVNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE, 40

	.type	_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,@object # @_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.section	.rodata._ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,"aG",@progbits,_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,comdat
	.weak	_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE:
	.asciz	"NSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE"
	.size	_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE, 60

	.type	_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,@object # @_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.section	.data.rel.ro._ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,"awG",@progbits,_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE,comdat
	.weak	_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.p2align	3, 0x0
_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.quad	_ZTINSt6thread6_StateE
	.size	_ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3, 0x0
	.quad	_GLOBAL__sub_I_signal_handler_threads.cpp
	.section	".linker-options","e",@llvm_linker_options
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.p2align	3, 0x0
	.type	DW.ref.__gxx_personality_v0,@object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.ident	"Debian clang version 19.1.0 (++20240917125639+a4bf6cd7cfb1-1~exp1~20240917125815.40)"
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _Z15handler_sigtermi
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSolsEi
	.addrsig_sym getpid
	.addrsig_sym gettid
	.addrsig_sym _Z14handler_siginti
	.addrsig_sym _Z7thread1j
	.addrsig_sym _ZNSolsEj
	.addrsig_sym sleep
	.addrsig_sym _Z7thread2j
	.addrsig_sym signal
	.addrsig_sym strerror
	.addrsig_sym __errno_location
	.addrsig_sym __gxx_personality_v0
	.addrsig_sym _ZNSt6thread4joinEv
	.addrsig_sym _ZNKSt6thread8joinableEv
	.addrsig_sym _ZSteqNSt6thread2idES0_
	.addrsig_sym _ZSt9terminatev
	.addrsig_sym _ZNSt6thread15_M_start_threadESt10unique_ptrINS_6_StateESt14default_deleteIS1_EEPFvvE
	.addrsig_sym _Znwm
	.addrsig_sym _ZdlPvm
	.addrsig_sym __clang_call_terminate
	.addrsig_sym __cxa_begin_catch
	.addrsig_sym _ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEEclEv
	.addrsig_sym _ZNSt6thread8_InvokerISt5tupleIJPFvjEiEEE9_M_invokeIJLm0ELm1EEEEvSt12_Index_tupleIJXspT_EEE
	.addrsig_sym _ZSt8__invokeIPFvjEJiEENSt15__invoke_resultIT_JDpT0_EE4typeEOS3_DpOS4_
	.addrsig_sym _ZSt3getILm0EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.addrsig_sym _ZSt3getILm1EJPFvjEiEEONSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEOS6_
	.addrsig_sym _ZSt13__invoke_implIvPFvjEJiEET_St14__invoke_otherOT0_DpOT1_
	.addrsig_sym _ZSt12__get_helperILm0EPFvjEJiEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE
	.addrsig_sym _ZNSt11_Tuple_implILm0EJPFvjEiEE7_M_headERS2_
	.addrsig_sym _ZNSt10_Head_baseILm0EPFvjELb0EE7_M_headERS2_
	.addrsig_sym _ZSt12__get_helperILm1EiJEERT0_RSt11_Tuple_implIXT_EJS0_DpT1_EE
	.addrsig_sym _ZNSt11_Tuple_implILm1EJiEE7_M_headERS0_
	.addrsig_sym _ZNSt10_Head_baseILm1EiLb0EE7_M_headERS0_
	.addrsig_sym _ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE6_M_ptrEv
	.addrsig_sym _ZSt3getILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.addrsig_sym _ZSt12__get_helperILm0EPNSt6thread6_StateEJSt14default_deleteIS1_EEERT0_RSt11_Tuple_implIXT_EJS5_DpT1_EE
	.addrsig_sym _ZNSt11_Tuple_implILm0EJPNSt6thread6_StateESt14default_deleteIS1_EEE7_M_headERS5_
	.addrsig_sym _ZNSt10_Head_baseILm0EPNSt6thread6_StateELb0EE7_M_headERS3_
	.addrsig_sym _ZNSt10unique_ptrINSt6thread6_StateESt14default_deleteIS1_EE11get_deleterEv
	.addrsig_sym _ZNKSt14default_deleteINSt6thread6_StateEEclEPS1_
	.addrsig_sym _ZNSt15__uniq_ptr_implINSt6thread6_StateESt14default_deleteIS1_EE10_M_deleterEv
	.addrsig_sym _ZSt3getILm1EJPNSt6thread6_StateESt14default_deleteIS1_EEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS9_
	.addrsig_sym _ZSt12__get_helperILm1ESt14default_deleteINSt6thread6_StateEEJEERT0_RSt11_Tuple_implIXT_EJS4_DpT1_EE
	.addrsig_sym _ZNSt11_Tuple_implILm1EJSt14default_deleteINSt6thread6_StateEEEE7_M_headERS4_
	.addrsig_sym _ZNSt10_Head_baseILm1ESt14default_deleteINSt6thread6_StateEELb1EE7_M_headERS4_
	.addrsig_sym _GLOBAL__sub_I_signal_handler_threads.cpp
	.addrsig_sym _Unwind_Resume
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
	.addrsig_sym _ZSt4cerr
	.addrsig_sym _ZTVN10__cxxabiv120__si_class_type_infoE
	.addrsig_sym _ZTSNSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
	.addrsig_sym _ZTINSt6thread6_StateE
	.addrsig_sym _ZTINSt6thread11_State_implINS_8_InvokerISt5tupleIJPFvjEiEEEEEE
