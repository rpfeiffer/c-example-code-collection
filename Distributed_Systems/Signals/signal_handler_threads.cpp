// =====================================================================================
//
//       Filename:  signal_handler_threads.cpp
//
//    Description:  Demonstration of signal handlers and signal reception
//
//        Version:  1.0
//        Created:  fr. 20. sep. 23:28:19 +0200 2024
//       Revision:  none
//       Compiler:  gcc/clang
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <iostream>
#include <thread>

// C
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

//----------------------------------------------------------------------
// Signal handler
//----------------------------------------------------------------------

void handler_sigterm( int signal ) {
    std::cout << "Process " << getpid() << " received signal SIGTERM (" << signal << "). TID = " << gettid() << "\n";
    return;
}

void handler_sigint( int signal ) {
    std::cout << "Process " << getpid() << " received signal SIGINT (" << signal << "). TID = " << gettid() << "\n";
    return;
}

//----------------------------------------------------------------------
// Thread code
//----------------------------------------------------------------------

void thread1( unsigned int n ) {
    std::cout << "Thread 1 running with argument " << n << " (PID = " << getpid() << ", TID = " << gettid() << ")\n";
    sleep(n);
    return;
}

void thread2( unsigned int n ) {
    std::cout << "Thread 2 running with argument " << n << " (PID = " << getpid() << ", TID = " << gettid() << ")\n";
    sleep(n);
    return;
}

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK; // EX_OK is defined in sysexits.h

    if ( signal( SIGTERM, handler_sigterm ) == SIG_ERR ) {
        std::cerr << "ERROR: Could not set SIGTERM handler - reason: " << strerror(errno) << " (" << errno << ")\n";
        rc = EX_UNAVAILABLE;
    }
    if ( signal( SIGINT, handler_sigint ) == SIG_ERR ) {
        std::cerr << "ERROR: Could not set SIGINT handler - reason: " << strerror(errno) << " (" << errno << ")\n";
        rc = EX_UNAVAILABLE;
    }

    std::cout << "Parent PID is " << getpid() << ". Sleeping now.\n";
    std::thread t1( thread1, 35 );
    sleep(1);
    std::thread t2( thread2, 55 );
    sleep(60);
    t1.join();
    t2.join();

    return(rc);
}
