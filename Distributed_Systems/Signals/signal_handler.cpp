// =====================================================================================
//
//       Filename:  signal_handler.cpp
//
//    Description:  Demonstration of signal handlers and signal reception
//
//        Version:  1.0
//        Created:  20. sep. 2024 kl. 22.40 +0200
//       Revision:  none
//       Compiler:  gcc/clang
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <iostream>

// C
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

//----------------------------------------------------------------------
// Signal handler
//----------------------------------------------------------------------

void handler_sigterm( int signal ) {
    std::cout << "Process " << getpid() << " received signal SIGTERM (" << signal << ").\n";
    return;
}

void handler_sigint( int signal ) {
    std::cout << "Process " << getpid() << " received signal SIGINT (" << signal << ").\n";
    return;
}

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK; // EX_OK is defined in sysexits.h

    if ( signal( SIGTERM, handler_sigterm ) == SIG_ERR ) {
        std::cerr << "ERROR: Could not set SIGTERM handler - reason: " << strerror(errno) << " (" << errno << ")\n";
        rc = EX_UNAVAILABLE;
    }
    if ( signal( SIGINT, handler_sigint ) == SIG_ERR ) {
        std::cerr << "ERROR: Could not set SIGINT handler - reason: " << strerror(errno) << " (" << errno << ")\n";
        rc = EX_UNAVAILABLE;
    }

    std::cout << "My PID is " << getpid() << ". Sleeping now.\n";
    sleep(60);

    return(rc);
}
