// =====================================================================================
//
//       Filename:  buffered_recv_test.cpp
//
//    Description:  Test reading newline strings from network connection.
//
//        Version:  1.0
//        Created:  22. okt. 2024 kl. 20.07 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.
//
// Compile with:
// g++ -Wall -Werror -std=c++20 -g -O0 -march=native -o buffered_recv_test buffered_recv.cpp buffered_recv_test.cpp
// or
// clang++ -Wall -Werror -std=c++20 -g -O0 -march=native -o buffered_recv_test buffered_recv.cpp buffered_recv_test.cpp
//
// To simulate the server part, just install netcat and run "nc -l -p 6668". "nc"
// then sends all terminal stdin inputs to the client.

#include <iostream>
#include "buffered_recv.h"

// C
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

const unsigned int BUFFER_SIZE = 65536;
const in_port_t SERVER_PORT = 6668;

//----------------------------------------------------------------------
//  M A I N
//----------------------------------------------------------------------

auto main( int argc, char **argv ) -> int {
    int rc = EX_OK; // EX_OK is defined in sysexits.h
    std::string message;
    struct sockaddr_in address;
    int client_socket;
    int status = -1;

    // -----------------------------------------------------------------
    // Create socket descriptor (i.e. request descriptor from operating
    // system)
    client_socket = socket( AF_INET, SOCK_STREAM, 0 );
    if ( client_socket == -1 ) {
        std::cerr << "Error getting socket descriptor: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_CANTCREAT );
    }

    // -----------------------------------------------------------------
    // Initialise address structure with listening address and port
    memset( &address, 0, sizeof(address) );
    address.sin_family = AF_INET;
    address.sin_port = htons( SERVER_PORT );
    if (argc < 2) {
        inet_aton( "127.0.0.1", &address.sin_addr );
    }
    else {
        inet_aton( argv[1], &address.sin_addr );
    }

    // -----------------------------------------------------------------
    // Create a connection to the server
    status = connect( client_socket, (struct sockaddr *)&address, sizeof(address) );
    if ( status == -1 ) {
        std::cerr << "Error connecting to server: " << strerror(errno) << " (" << errno << ")\n";
        return( EX_UNAVAILABLE );
    }
    std::cout << "Connected to server " << inet_ntoa( address.sin_addr ) << ".\n";

    // -----------------------------------------------------------------
    // Read lines from server
    std::string line;
    class buf_recv recv_line;
    recv_line.setBufferSize( BUFFER_SIZE );
    do {
        if ( recv_line.readLine( client_socket, line ) ) {
            std::cout << "Received: |" << line << "|\n";
        }
    } while ( line.find("stop") == std::string::npos );

    // -----------------------------------------------------------------
    // Shutdown and close socket descriptor
    if ( client_socket != -1 ) {
        status = shutdown( client_socket, SHUT_RDWR );
        if ( status == -1 ) {
            std::cerr << "Error shutting down socket: " << strerror(errno) << " (" << errno << ")\n";
            rc = EX_OSERR;
        }
        status = close( client_socket );
        if ( status == -1 ) {
            std::cerr << "Error closing socket: " << strerror(errno) << " (" << errno << ")\n";
            rc = EX_OSERR;
        }
    }

    return(rc);
}
