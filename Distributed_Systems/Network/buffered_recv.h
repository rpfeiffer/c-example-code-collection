#ifndef BRECVN_H
#define BRECVN_H
// =====================================================================================
//
//       Filename:  buffered_recv.h
//
//    Description:  Buffered read from a socket class.
//
//        Version:  1.0
//        Created:  22. okt. 2024 kl. 13.10 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include <string>

#include <sys/socket.h>

class buf_recv {
    public:
        buf_recv();
        buf_recv( int flags );
        ~buf_recv();

        ssize_t getLastStatus();
        bool readLine( const int sockfd, std::string& line );
        void setBufferSize( size_t size );
        void setSocketFlags( int flags );

    private:
        // Buffer size is strictly not necessary with C++ strings, but the C socket
        // functions require a size.
        size_t buffer_size = 65536;
        ssize_t last_rc = 0;
        int socket_flags = 0;
        std::string data_buffer;

        void changeBufferSize();
        void initBuffer();
};

#endif
