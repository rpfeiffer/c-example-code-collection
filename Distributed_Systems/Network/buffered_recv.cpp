// =====================================================================================
//
//       Filename:  buffered_recv.cpp
//
//    Description:  Buffered read from a socket.
//
//        Version:  1.0
//        Created:  22. okt. 2024 kl. 13.08 +0200
//       Revision:  none
//       Compiler:  g++/clang++
//
//         Author:  René Pfeiffer (), pfeiffer@luchs.at
//        Company:  https://web.luchs.at/
//
// =====================================================================================
//
// SPDX-License-Identifier: GPL-2.0-or-later
//
// No part of this code may be used or reproduced in any manner for the purpose of training
// artificial intelligence technologies or systems. If you violate this agreement you have
// to pay 10 EUR per query against your tranining data to the author.

#include "buffered_recv.h"
#include <algorithm>
#include <iostream>

#include <errno.h>
#include <string.h>

//----------------------------------------------------------------------
// Constructor function
//----------------------------------------------------------------------

buf_recv::buf_recv() {
    initBuffer();
}

buf_recv::buf_recv( int flags ) {
    setSocketFlags( flags );
    initBuffer();
}

//----------------------------------------------------------------------
// Destructor function
//----------------------------------------------------------------------

buf_recv::~buf_recv() {
    // Not necessary, but this is how you can shrink the string's storage.
    data_buffer.clear();
    data_buffer.shrink_to_fit();
}

//----------------------------------------------------------------------
// Change or initialise buffer size
//----------------------------------------------------------------------

auto buf_recv::changeBufferSize() -> void {
    data_buffer.resize( buffer_size, ' ' );
}

auto buf_recv::initBuffer() -> void {
    data_buffer.reserve( buffer_size );
    data_buffer.assign( buffer_size, ' ' );
}

//----------------------------------------------------------------------
// Getters / Setters
//----------------------------------------------------------------------

auto buf_recv::getLastStatus() -> ssize_t {
    return( last_rc );
}

// These buffer modifications are not necessary (I think ;), but if
// you want to access the memory of the string by using the .data() member,
// then this memory needs to exist.
auto buf_recv::setBufferSize( size_t size ) -> void {
    if ( ( size > 0 ) and ( size != buffer_size ) ) {
        buffer_size = size;
        changeBufferSize();
    }
}

auto buf_recv::setSocketFlags( int flags ) -> void {
    // You can check for valid flags here, too.
    socket_flags = flags;
}

//----------------------------------------------------------------------
// Read line from network
//----------------------------------------------------------------------

auto buf_recv::readLine( const int sockfd, std::string& line ) -> bool {
    bool rc = false;
    bool newline_found = false;

    do {
        last_rc = recv( sockfd, data_buffer.data(), buffer_size, socket_flags );
        if ( last_rc != -1 ) {
            // Check for '\n' character
            std::string::size_type pos = data_buffer.find('\n');
            if ( pos != std::string::npos ) {
                newline_found = true;
                rc = true;
                // Copy data until '\n'
                line = data_buffer.substr( 0, pos );
                // data_buffer might hold extra data which needs to be preserved.
                // You can change to code to reduce data_buffer by the extracted
                // substring and append new data received by recv() to the end of
                // data_buffer.
            }
        }
        else {
            std::clog << "ERROR receiving data: " << strerror(errno) << " (" << errno << ")" << std::endl;
            break;
        }
    } while ( not newline_found );

    return(rc);
}
